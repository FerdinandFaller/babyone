[CustomEditor(typeof(RageConstraint))]
public class RageConstraintEditor (RageToolsEdit): 
	
	private _RageConstraint as RageConstraint
	private _Edgetune as RageEdgetune

	public def OnDrawInspectorHeaderLine():
		_RageConstraint = target if _RageConstraint == null
		
		LookLikeControls(20f, 1f)
		EasyToggle "Live", _RageConstraint.Live, MaxWidth(45f)
		LookLikeControls(60f, 1f)		
		EasyObjectField	"Follower:", _RageConstraint.Follower, typeof(GameObject)		

	public def OnDrawInspectorGUI():
		_RageConstraint = target if _RageConstraint == null
		
		LookLikeControls(60f)
		EasyRow:		
			EasyToggle "Position", _RageConstraint.FollowPosition
			EasyToggle "Rotation", _RageConstraint.FollowRotation		
			EasyToggle "Scale", _RageConstraint.FollowScale
			EasyToggle "Local", _RageConstraint.Local

		if not _RageConstraint.FollowerGroup == null:
			EasyRow:
				EasyToggle "Visible", _RageConstraint.GroupVisible, MaxWidth(80f)
				if _RageConstraint.GroupVisible:
					if _RageConstraint.FollowerGroup.Proportional:	
						LookLikeControls(110f,30f)
						EasyPercent "Opacity x", _RageConstraint.FollowerGroup.OpacityMult, 1
					else:
						LookLikeControls(110f,30f)
						EasyPercent "Opacity", _RageConstraint.FollowerGroup.Opacity, 1

		EditorUtility.SetDirty (_RageConstraint)
		if not _RageConstraint.FollowerGroup == null:
			EditorUtility.SetDirty (_RageConstraint.FollowerGroup)

