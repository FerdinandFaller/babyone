using UnityEditor;
using UnityEngine;
using System.Collections;

public class MacrosRageConstraint : MonoBehaviour {
	/// <summary> Quick creation and setup of a new external controller, tied to the current selection
	/// </summary>
	[MenuItem("Component/RageTools/Macros/RageConstraint - Create Controller")]
	public static void RageConstraintQuickCreate() {
		#region Error Detection
		if (Selection.activeTransform == null) {
			Debug.Log("Macro Error: First select the Root Game Object of an imported Font.");
			return;
		}

		if (Selection.activeTransform.childCount == 0) {
			Debug.Log("Macro Error: Game Object has no children. Please select the RageFont root.");
			return;
		}
		#endregion Error Detection

		var activeGameObject = Selection.activeGameObject;
		var controller = new GameObject("Controller"+activeGameObject.name);
		controller.transform.parent = activeGameObject.transform;
		controller.transform.localPosition = Vector3.zero;
		controller.transform.localRotation = Quaternion.identity;
		controller.transform.localScale = activeGameObject.transform.lossyScale;
		controller.transform.parent = activeGameObject.transform.root;
		var rageConstraint = controller.AddComponent<RageConstraint>();
		rageConstraint.Follower = activeGameObject;
		rageConstraint.FollowPosition = true;
		Selection.activeGameObject = controller;
		var rageHandle = controller.AddComponent<RageHandle>();
		rageHandle.GizmoFile = "pole";
		rageHandle.Live = true;
	}
}
