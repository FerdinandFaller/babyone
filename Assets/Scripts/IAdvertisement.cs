using System;
using UnityEngine;
using System.Collections;


public enum AdFormat
{
    Mma320X50,
}

public interface IAdvertisement
{
    string PublisherId { get; set; }
    AdFormat Format { get; set; }

    // TODO: Make Event usefull!
    event Action RequestFailedEvent;

    void Request();

    /// <summary>
    /// Shows the Ad at the given relative Position.
    /// </summary>
    /// <param name="relativePosition">[0..1] 0 = Top, 1 = Bottom</param>
    void Show(float relativePosition);

    void Hide();
    
    void Refresh();
}
