using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;


public class AdManager : MonoBehaviour
{
    private static SortedList<float, IAdvertisement> _advertisements;
    private static IAdvertisement _currentlyUsedAd;

	void Awake () 
    {
	    DontDestroyOnLoad(gameObject);

        _advertisements = new SortedList<float, IAdvertisement>();
	}

    /// <summary>
    /// Adds the given Advertisement to the List of possibly used Ads and requests the first Banner.
    /// </summary>
    /// <param name="ad"></param>
    /// <param name="priority">[0..1] The smaller the priority, the earlier the ad is used.</param>
    public static void AddAdvertisement(IAdvertisement ad, float priority)
    {
        if(ad == null) throw new ArgumentNullException("ad");
        
        _advertisements.Add(priority, ad);

        ad.Request();

        if(_currentlyUsedAd == null)
        {
            _currentlyUsedAd = ad;
        }
    }
}