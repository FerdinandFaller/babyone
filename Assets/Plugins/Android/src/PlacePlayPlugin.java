package com.paradigmcreatives.placeplayplugin;

import android.graphics.PixelFormat;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.WindowManager;

import com.placeplay.ads.PAAdListener;
import com.placeplay.ads.PALayout;
import com.placeplay.ads.PAAdListener;
import com.unity3d.player.UnityPlayerActivity;
import com.unity3d.player.UnityPlayer;
import com.placeplay.ads.PACrossPromotion;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import java.util.StringTokenizer;

public class PlacePlayPlugin extends UnityPlayerActivity implements
		PAAdListener {
    
    private static final String UNITY_SCRIPT_TARGET_OBJECT_NAME = "Main Camera";
    private static final String UNITY_SCRIPT_TARGET_METHOD_NAME = "dispatchNativeMessage";
    
    private static final String UNITY_MESSAGE_AD_RECEIVED = "com.placeplay.adreceived";
    private static final String UNITY_MESSAGE_AD_FAILED = "com.placeplay.adfailed";
    private static final String UNITY_MESSAGE_AD_WILL_PRESENT_FULLSCREEN = "com.placeplay.willpresentfullscreen";
    private static final String UNITY_MESSAGE_AD_WILL_DISMISS_FULLSCREEN  = "com.placeplay.diddismissfullscreen";
    
    private UnityScriptMessenger scriptMessanger;
    
	static PALayout paLayout = null;
	static WindowManager manager = null;
	public static final String TAG = "PlacePlayActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        scriptMessanger = new UnityScriptMessenger(UNITY_SCRIPT_TARGET_OBJECT_NAME, UNITY_SCRIPT_TARGET_METHOD_NAME);
    }
	
	/**
	 * Method to display the ad with the given width,height and
	 * position(anchorTag) ...
	 * 
	 * @param gameId
	 * @param anchorTag
	 * @param isRepeating
	 * @param adWidth
	 * @param adHeight
	 * @param isInstallTrackEnabled
	 */
	public static void displayAdLayouts(final String gameId,
			final String anchorTag, final boolean isRepeating,
			final int adWidth, final int adHeight,
			final boolean isInstallTrackEnabled) {

		final Activity activity = UnityPlayer.currentActivity;
		manager = (WindowManager) activity.getSystemService(WINDOW_SERVICE);
		// here we are providing the x and y values of the windowmanager
		// layoutparams as (0,0) because we don't want to depend on the
		// coordinates of the screen. we are positioning the ad based on
		// gravity attribute of the layoutparams
		final float density = activity.getResources().getDisplayMetrics().density;
		final int width = (int) (adWidth * density);
		final int height = (int) (adHeight * density);
		final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
				width, height, 0, 0,
				WindowManager.LayoutParams.TYPE_APPLICATION,
				WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
				PixelFormat.TRANSPARENT);
		if (anchorTag != null && anchorTag.length() > 0) {
			StringTokenizer st = new StringTokenizer(anchorTag, "| ");
			while (st.hasMoreElements()) {
				final String adPosition = st.nextElement().toString();
				if (adPosition.equals("Top") || adPosition.equals("PAAdViewAnchorTop")) {
					params.gravity = params.gravity | Gravity.TOP;
				} else if (adPosition.equals("Left") || adPosition.equals("PAAdViewAnchorLeft")) {
					params.gravity = params.gravity | Gravity.LEFT;
				} else if (adPosition.equals("Right") || adPosition.equals("PAAdViewAnchorRight")) {
					params.gravity = params.gravity | Gravity.RIGHT;
				} else if (adPosition.equals("Bottom") || adPosition.equals("PAAdViewAnchorBottom")) {
					params.gravity = params.gravity | Gravity.BOTTOM;
				} else if (adPosition.equals("HorCenter") || adPosition.equals("PAAdViewAnchorHorCenter")) {
					params.gravity = params.gravity | Gravity.CENTER_HORIZONTAL;
				} else if (adPosition.equals("VerCenter") || adPosition.equals("PAAdViewAnchorVerCenter")) {
					params.gravity = params.gravity | Gravity.CENTER_VERTICAL;
				}
			}
		}

		activity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// Calling the tracking service if enabled
				if (isInstallTrackEnabled) {
					PACrossPromotion.trackInstallation(activity, gameId);
				}
				// Init the PALayout
				paLayout = new PALayout(activity, gameId);
				paLayout.setMaxWidth(width);
				paLayout.setMaxHeight(height);
				paLayout.setPAAdListener((com.placeplay.ads.PAAdListener) activity);
				paLayout.setPackageId("plugin-package-unity-android");
				manager.addView(paLayout, params);
				paLayout.requestAd(isRepeating);
			}
		});
	}

    /**
     * This method causes the ad service to be paused
     */
    public static void pauseAdService() {
        UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
            
            @Override
            public void run() {
                if (paLayout != null) {
                    paLayout.pauseAdService();
                }
            }
        });
    }
    
    /**
     * This method causes the ad service to be resumed where it had passed
     */
    public static void resumeAdService() {
        UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
            
            @Override
            public void run() {
                if (paLayout != null) {
                    paLayout.resumeAdService();
                }
            }
        });
    }

    /* PlacePlay delegate's methods */
            
    @Override
    public void pA_RequestAdSuccess() {
        scriptMessanger.sendMessage(UNITY_MESSAGE_AD_RECEIVED);
	}
    
    @Override
	public void pA_RequestAdFail() {
		// TODO: add error handler
        scriptMessanger.sendMessage(UNITY_MESSAGE_AD_FAILED);
	}

	@Override
	public void pA_WillDismissAdBrowser() {
		scriptMessanger.sendMessage(UNITY_MESSAGE_AD_WILL_DISMISS_FULLSCREEN);
	}
	
	@Override
	public void pA_WillLeaveActivity(boolean toAdBrowser) {
        scriptMessanger.sendMessage(UNITY_MESSAGE_AD_WILL_PRESENT_FULLSCREEN);
	}
}