package com.paradigmcreatives.placeplayplugin;

import com.unity3d.player.UnityPlayer;

public class UnityScriptMessenger
{
	private String objectName;
	private String methodName;

	public UnityScriptMessenger(String objectName, String methodName)
	{
		this.objectName = objectName;
		this.methodName = methodName;
	}
	
	public void sendMessage(String message)
	{
		UnityPlayer.UnitySendMessage(objectName, methodName, message);
	}
}