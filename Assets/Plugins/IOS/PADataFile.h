//
//  PATextFile.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 2/8/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PADataFile : NSObject
{
    NSString *path;
    NSString *name;
}

- (id)initWithName:(NSString *)name;

- (BOOL)writeData:(NSData *)data outError:(NSError**)error;
- (NSData *)readData:(NSError**)error;

- (BOOL)exists;
- (BOOL)deleteFile;

@end
