//
//  PAAdLog.h
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 26.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PAAvailability.h"

typedef enum {
    PALogLevelNone  = 0,
    PALogLevelCrit  = 1,
    PALogLevelError = 2,
    PALogLevelWarn  = 3,
    PALogLevelInfo  = 4,
    PALogLevelDebug = 5
} PALogLevel;


void PALogSetLogLevel(PALogLevel level);

void _PALogCrit(NSString *format, ...);
void _PALogError(NSString *format, ...);
void _PALogWarn(NSString *format, ...);
void _PALogInfo(NSString *format, ...);
void _PALogDebug(NSString *format, ...);

void _PAAssert(const char* expression, const char* file, int line, const char* function);
void _PAAssertMsg(const char* expression, const char* file, int line, const char* function, NSString *message);
void _PAAssertMsgv(const char* expression, const char* file, int line, const char* function, NSString *format, ...);

#ifndef PALogCrit
#define PALogCrit(...) _PALogCrit(__VA_ARGS__)
#endif

#ifndef PALogError
#define PALogError(...) _PALogError(__VA_ARGS__)
#endif

#ifndef PALogWarn
#define PALogWarn(...) _PALogWarn(__VA_ARGS__)
#endif

#ifndef PALogInfo
#define PALogInfo(...) _PALogInfo(__VA_ARGS__)
#endif

#ifndef PALogDebug
#define PALogDebug(...) _PALogDebug(__VA_ARGS__)
#endif


#define PAAssert(expression) if (!(expression)) _PAAssert(#expression, __FILE__, __LINE__, __FUNCTION__)    
#define PAAssertMsg(expression, msg) if (!(expression)) _PAAssertMsg(#expression, __FILE__, __LINE__, __FUNCTION__, (msg))
#define PAAssertMsgv(expression, msg, ...) if (!(expression)) _PAAssertMsgv(#expression, __FILE__, __LINE__, __FUNCTION__, (msg), __VA_ARGS__)