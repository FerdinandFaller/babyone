//
//  Foo.m
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 21.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import "PAAdStatistics.h"

static PAAdStatistics* instance = nil;

@implementation PAAdStatistics

@synthesize impressions;
@synthesize clicks;
@synthesize impressionsFailed;
@synthesize clicksFailed;

- (id)init
{
    self = [super init];
    if (self) {
        requests = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [requests release];
    [super dealloc];
}

- (void)addRequestInfoWithDuration:(NSTimeInterval)duration
{
    BannerRequestInfo *info = [[BannerRequestInfo alloc] init];
    info.responseDuration = duration;
    [requests insertObject:info atIndex:0];
    [info release];
}

- (BannerRequestInfo *)currentRequestInfo
{
    return requests.count > 0 ? [requests objectAtIndex:0] : nil;
}

- (NSArray *)requests
{
    return requests;
}

- (void)addClick
{
    clicks++;
}

- (void)addImpression
{
    impressions++;
}

- (void)addClickFailed
{
    clicksFailed++;
}

- (void)addImpressionFailed
{
    impressionsFailed++;
}

+ (PAAdStatistics *)sharedInstance
{
    if (instance == nil)
        instance = [[PAAdStatistics alloc] init];
    return instance;
}

@end
