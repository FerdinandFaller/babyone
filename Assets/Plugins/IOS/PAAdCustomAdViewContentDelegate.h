//
//  PAAdCustomAdViewContentDelegate.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 2/7/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PAAdCustomAdViewContentDelegate <NSObject>

@optional
- (void)adContentDidLoad:(PAAdCustomAdView *)adView;
- (void)adContentDidFail:(PAAdCustomAdView *)adView withError:(NSError *)error;
- (void)adContentDidCancel:(PAAdCustomAdView *)adView;

@end
