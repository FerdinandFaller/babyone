//
//  PAHttpImageRequest.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 1/14/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PAHttpImageRequest.h"
#import "PAHttpRequest_Inheritance.h"
#import "PAAvailability.h"

@implementation PAHttpImageRequest

@synthesize responseImage;
@synthesize useRetinaScale;

- (void)dealloc
{
    self.responseImage = nil;
    [super dealloc];
}

- (void)initResponseImage
{
    CGFloat scale = 1.0;
    
    if (useRetinaScale && PA_SELECTOR_AVAILABLE([UIScreen mainScreen], scale))
	{
#if PA_IOS_SDK_AVAILABLE(__IPHONE_4_0)
        scale = [UIScreen mainScreen].scale;    
#endif
	}
    
    UIImage *image = [[UIImage alloc] initWithData:self.responseData];
    
    if (scale > 1.0)
    {
        UIImage *scaledImage = [[UIImage alloc] initWithCGImage:image.CGImage scale:scale orientation:UIImageOrientationUp];        
        self.responseImage = scaledImage;
        [scaledImage release];
    }
    else
    {
        self.responseImage = image;
    }    
    [image release];
}

- (void)finish
{
    [self initResponseImage];
    [super finish];
}

- (void)releaseConnection
{
    [super releaseConnection];
    self.responseImage = nil;
}

@end
