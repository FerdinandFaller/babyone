//
//  PAAdLog.m
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 26.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import "PADebug.h"

#define kButtonTitleContinue    @"Continue"
#define kButtonTitleAbort       @"Abort"

#ifdef PA_ASSERTS

BOOL blockingAlertActive;

@interface AlertViewDelegate : NSObject<UIActionSheetDelegate>
{
	NSString* alertMessage;
}

- (id)initWithExpression:(const char*)expression andMessage:(NSString*)message inFile:(const char*) file andLine:(int)line inFunction:(const char*)function;
- (void)show;

@end

@implementation AlertViewDelegate

- (void)actionSheet:(UIActionSheet*)view didDismissWithButtonIndex:(NSInteger) buttonIndex
{
    NSString *buttonTitle = [view buttonTitleAtIndex:buttonIndex];
    NSLog(@"Assertion notification: '%@'", buttonTitle);
    if ([buttonTitle isEqualToString:kButtonTitleAbort])
    {     
        abort();
    }
    
    blockingAlertActive = NO;
}

- (void) actionSheetCancel:(UIActionSheet*)view
{
    blockingAlertActive = NO;
}

- (id)initWithExpression:(const char *)expression andMessage:(NSString *)message inFile:(const char *)file andLine:(int)line inFunction:(const char *)function
{
    self = [super init];
    if (self)
    {     
        alertMessage = [[NSString alloc] initWithFormat:@"Assertion: (%s)\n with message: %@\n fails in:%s\nin file: %s:%d", expression, message, function, file, line];
    }
	return self;
}

- (void)dealloc
{
    [alertMessage release];
    [super dealloc];
}

- (void) show
{
	UIActionSheet* view = [[UIActionSheet alloc] initWithTitle:alertMessage
                                                      delegate:self 
											 cancelButtonTitle:nil 
                                        destructiveButtonTitle:kButtonTitleAbort 
                                             otherButtonTitles:nil];
    [view addButtonWithTitle:kButtonTitleContinue];
	UIView* parentView = [UIApplication sharedApplication].keyWindow;
	[view showInView:parentView];
	[view release];
}

@end

#endif // #ifdef PA_ASSERTS

static PALogLevel g_PALogLevel = PALogLevelWarn;

void PALogSetLogLevel(PALogLevel level) {
    g_PALogLevel = level;
}

void _PALogCrit(NSString *format, ...) {
    if (g_PALogLevel < PALogLevelCrit) return;
    va_list ap;
    va_start(ap, format);
    NSString *fmt = [[NSString alloc] initWithFormat:@"PlacePlayAds/C: %@", format];
    NSLogv(fmt, ap);
    [fmt release];
    va_end(ap);
}

void _PALogError(NSString *format, ...) {
    if (g_PALogLevel < PALogLevelError) return;
    va_list ap;
    va_start(ap, format);
    NSString *fmt = [[NSString alloc] initWithFormat:@"PlacePlayAds/E: %@", format];
    NSLogv(fmt, ap);
    [fmt release];
    va_end(ap);
}

void _PALogWarn(NSString *format, ...) {
    if (g_PALogLevel < PALogLevelWarn) return;
    va_list ap;
    va_start(ap, format);
    NSString *fmt = [[NSString alloc] initWithFormat:@"PlacePlayAds/W: %@", format];
    NSLogv(fmt, ap);
    [fmt release];
    va_end(ap);
}

void _PALogInfo(NSString *format, ...) {
    if (g_PALogLevel < PALogLevelInfo) return;
    va_list ap;
    va_start(ap, format);
    NSString *fmt = [[NSString alloc] initWithFormat:@"PlacePlayAds/I: %@", format];
    NSLogv(fmt, ap);
    [fmt release];
    va_end(ap);
}

void _PALogDebug(NSString *format, ...) {
    if (g_PALogLevel < PALogLevelDebug) return;
    va_list ap;
    va_start(ap, format);
    NSString *fmt = [[NSString alloc] initWithFormat:@"PlacePlayAds/D: %@", format];
    NSLogv(fmt, ap);
    [fmt release];
    va_end(ap);
}

const char * __shorten_path(const char * path);

void _PAAssert(const char* expression, const char* file, int line, const char* function)
{
    _PAAssertMsg(expression, file, line, function, @"");
}

void _PAAssertMsg(const char* expression, const char* file, int line, const char* function, NSString *message)
{
    _PAAssertMsgv(expression, file, line, function, message);
}

void _PAAssertMsgv(const char* expression, const char* file, int line, const char* function, NSString *format, ...)
{
    file = __shorten_path(file);    
    
    va_list ap;
    va_start(ap, format);    
    NSString *message = [[NSString alloc] initWithFormat:format arguments:ap];    
    NSString *consoleMessage = [[NSString alloc] initWithFormat:@"PlacePlayAds/ASSERT: (%s) in %s:%d %s message:'%@'", expression, file, line, function, message];
    NSLog(@"%@", consoleMessage);
    [consoleMessage release];
    [message release];
    va_end(ap);
    
#ifdef PA_ASSERTS
    AlertViewDelegate* alertDelegate = [[AlertViewDelegate alloc] initWithExpression:expression andMessage:message inFile:file andLine:line inFunction:function];
    [alertDelegate show];
    [message release];
    
    blockingAlertActive = TRUE;    
	while (blockingAlertActive)
	{
		while(CFRunLoopRunInMode(kCFRunLoopDefaultMode, 0, TRUE) == kCFRunLoopRunHandledSource);
	}
#endif
}

const char * __shorten_path(const char * path)
{
	const char *to_return = path;
	const char *p = path; while (*p) ++p;
	while (p >= path)
	{
		--p;
		if (*p == '/')
        {
			to_return = p+1;
            break;
        }
	}
	
	return to_return;
}
