//
//  PALandingCacheData.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 2/4/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PALandingInfo.h"

#import "PADebug.h"

@implementation PALandingInfo

@synthesize type, data, networkId, networkType, responseData, version;

- (id)initWithNetworkId:(NSString *)aNetworkId andNetworkType:(NSString *)aNetworkType
{
    self = [super init];
    if (self)
    {
        state = PALandingDataStateUndefined;
        networkId = [aNetworkId copy];
        networkType = [aNetworkType copy];
    }
    return self;
}

- (void)dealloc
{
    self.type = nil;
    self.data = nil;
    self.responseData = nil;
    
    [networkId release];
    [networkType release];
    [super dealloc];
}

- (void)setReceivedWithType:(NSString *)aType andData:(NSString *)aData
{
    PAAssertMsgv([self isUndefined], @"Unexpected landing data state (%@): %d", networkId, state);
    PALogDebug(@"'%@' landing info received", networkId);
    
    self.type = aType;
    self.data = aData;    
    state = PALandingDataStateReceived;
}

- (void)invalidate
{
    PALogWarn(@"'%@' landing info invalidated", networkId);
    state = PALandingDataStateInvalid;
    self.type = nil;
    self.data = nil;
    self.responseData = nil;
}

- (void)clear
{
    PALogWarn(@"'%@' landing info clear", networkId);
    state = PALandingDataStateUndefined;
}

- (BOOL)isValid
{
    return state == PALandingDataStateReceived;
}

- (BOOL)isInvalid
{
    return state == PALandingDataStateInvalid;
}

- (BOOL)isUndefined
{
    return state == PALandingDataStateUndefined;
}

@end
