//
//  PAHttpRequestError.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 1/24/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PAHttpRequestError.h"

#import "PAHttpRequest.h"

#define kPAHttpRequestErrorDomain @"PAHttpRequestErrorDomain"

@implementation PAHttpRequestError

@synthesize request;

- (id)initWithRequest:(PAHttpRequest *)aRequest errorCode:(NSInteger)code andMessage:(NSString *)message
{
    NSDictionary *info = [[NSDictionary alloc] initWithObjectsAndKeys:message, @"NSLocalizedDescriptionKey", nil];
    self = [super initWithDomain:kPAHttpRequestErrorDomain code:code userInfo:info];
    [info release];
    
    if (self)
    {
        request = [aRequest retain];
    }
    
    return self;
}

- (void)dealloc
{
    [request release];
    [super dealloc];
}

- (NSInteger)responseCode
{
    return request.responseCode;
}

@end
