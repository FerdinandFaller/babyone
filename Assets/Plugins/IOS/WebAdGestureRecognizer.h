//
//  WebAdGestureRecognizer.h
//
//  Created by Alex Koloskov on 9/30/11.
//  Copyright 2011 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "PAAvailability.h"

#if PA_IOS_SDK_AVAILABLE(__IPHONE_3_2)
	#define kWebAdGestureRecognizerBase UIGestureRecognizer <UIGestureRecognizerDelegate>
#else
	#define kWebAdGestureRecognizerBase NSObject
#endif

@interface WebAdGestureRecognizer : kWebAdGestureRecognizerBase
{
    id target;
    SEL action;
}

- (id)initWithTarget:(id)target action:(SEL)action;

@end

