//
//  PAAdStatisticsTableViewCell.m
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 22.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import "PAAdStatisticsTableViewCell.h"

@implementation PAAdStatisticsTableViewCell

@synthesize nameLabel;
@synthesize bannerLoadLabel;
@synthesize pageLoadLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIView* xibView = [[[NSBundle mainBundle] loadNibNamed:@"PAAdStatisticsTableViewCell" owner:self options:nil] objectAtIndex:0];
        [xibView setFrame:[self bounds]];
        [self addSubview:xibView];
    }
    return self;
}

- (void)dealloc
{
    self.nameLabel = nil;
    self.bannerLoadLabel = nil;
    self.pageLoadLabel = nil;
    [super dealloc];
}

@end
