//
//  StopWatch.m
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 21.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import "StopWatch.h"

@implementation StopWatch

@synthesize startDate;
@synthesize duration;

- (id)init
{
    self = [super init];
    if (self)
    {
        duration = -1;
    }
    return self;
}

- (void) dealloc
{
    self.startDate = nil;
    [super dealloc];
}

- (void)start
{
    NSDate *now = [[NSDate alloc] init];
    self.startDate = now;
    [now release];
}

- (void)stop
{
    self.duration = -[self.startDate timeIntervalSinceNow];
}

@end
