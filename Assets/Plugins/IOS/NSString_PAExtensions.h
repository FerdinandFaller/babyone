//
//  NSString+PAExtensions.h
//  PlacePlayAds
//
//  Created by Alex Koloskov on 9/21/11.
//  Copyright 2011 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (PAExtensions)
    - (NSString*) md5;
@end
