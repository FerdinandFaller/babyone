//
//  PAAdCustomAdViewDelegate.h
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 26.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PAAdCustomAdView;

@protocol PAAdCustomAdViewDelegate<NSObject>

- (void)adTapped:(PAAdCustomAdView *)adView;
- (void)adTappedSecondaryClick:(PAAdCustomAdView *)adView;
- (void)adClosed:(PAAdCustomAdView *)adView;

@optional
- (void)adDidStart:(PAAdCustomAdView *)adView;
- (void)adDidFinish:(PAAdCustomAdView *)adView;
- (void)adDidFinish:(PAAdCustomAdView *)adView withError:(NSError *)error;

@end
