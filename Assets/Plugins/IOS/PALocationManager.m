//
//  PALocationManager.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 5/22/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PALocationManager.h"

#import "patimer.h"
#import "PADebug.h"
#import "PAAdDateFormatter.h"
#import "PAUIStatistics.h"

@interface PALocationManager (Private)

- (void)addNotificationCenterObservers;
- (void)removeNotificationCenterObservers;

- (void)resignActive:(NSNotification *)notification;
- (void)becomeActive:(NSNotification *)notification;

- (void)startLocationUpdateTimer;
- (void)stopLocationUpdateTimer;
- (void)onLocationUpdateTimer:(NSTimer *)timer;

@end

@implementation PALocationManager

@synthesize locationManager = locationManager_;
@synthesize lastKnownLocation = lastKnownLocation_;
@synthesize locationUpdateTimer = locationUpdateTimer_;

- (id)initWithUpdateInterval:(NSTimeInterval)interval
{
    self = [super init];
    if (self)
    {
        locationManager_ = [[CLLocationManager alloc] init];
        locationManager_.delegate = self;
        locationManager_.distanceFilter = 0;
        locationManager_.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
        
        updateInterval = interval;
        
        [self addNotificationCenterObservers];
    }
    return self;
}

- (void)dealloc
{
    [self removeNotificationCenterObservers];
    [self stopLocationUpdateTimer];
    
    self.locationManager.delegate = nil;
    self.locationManager = nil;
    self.lastKnownLocation = nil;
    
    [super dealloc];
}

#pragma mark -
#pragma mark Location

- (void)setExpiryInterval:(NSTimeInterval)interval
{
    updateInterval = interval;
}

- (void)startUpdatingLocation
{
    PALogDebug(@"PALocationManager started updating location");
    
    [self stopLocationUpdateTimer];
    [self.locationManager startUpdatingLocation];
}

- (void)stopUpdatingLocation
{
    PALogDebug(@"PALocationManager stopped updating location");
    
    [self stopLocationUpdateTimer];
    [self.locationManager stopUpdatingLocation];
}

- (CLLocation *)location
{
    return self.lastKnownLocation;
}

#pragma mark -
#pragma mark CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    PALogDebug(@"PALocationManager updated location: lat=%f lon=%f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    
    self.lastKnownLocation = newLocation;
    PAUISetLocationStatus([PAAdDateFormatter getTimestampInRFC3339FormatForDate:self.lastKnownLocation.timestamp]);
    
    [self stopUpdatingLocation];
    [self startLocationUpdateTimer];
}

#pragma mark -
#pragma mark NotificationCenter

- (void)addNotificationCenterObservers
{
    NSNotificationCenter *notifCenter = [NSNotificationCenter defaultCenter];
    [notifCenter addObserver:self
                    selector:@selector(resignActive:)
                        name:UIApplicationWillResignActiveNotification
                      object:nil];
    [notifCenter addObserver:self
                    selector:@selector(becomeActive:)
                        name:UIApplicationDidBecomeActiveNotification
                      object:nil];
}

- (void)removeNotificationCenterObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)resignActive:(NSNotification *)notification
{
    [self stopLocationUpdateTimer];
}

- (void)becomeActive:(NSNotification *)notification
{
    [self startUpdatingLocation];
}

#pragma mark -
#pragma mark Location update timer

- (void)startLocationUpdateTimer
{
    if (![locationUpdateTimer_ isValid])
    {
        PALogDebug(@"PALocationManager's timer started");
    }
    scheduleTimer(&locationUpdateTimer_, updateInterval, self, @selector(onLocationUpdateTimer:), nil, NO);
}

- (void)stopLocationUpdateTimer
{
    if ([locationUpdateTimer_ isValid])
    {
        PALogDebug(@"PALocationManager's timer stopped");    
    }
    invalidateTimer(&locationUpdateTimer_);
}

- (void)onLocationUpdateTimer:(NSTimer *)timer
{
    PALogDebug(@"PALocationManager's timer fired");    
    [self stopLocationUpdateTimer];
    [self startUpdatingLocation];
}

@end
