//
//  PAHttpJSonRequest.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 1/14/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PAHttpRequest.h"

@interface PAHttpJSonRequest : PAHttpRequest
{
@private
    id responseJson;
}

@property (nonatomic, retain) id responseJson;

@end
