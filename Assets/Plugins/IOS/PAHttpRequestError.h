//
//  PAHttpRequestError.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 1/24/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PAHttpRequest;

typedef enum 
{
    PAHttpRequestErrorCodeUnexpectedHttpCode = -1000,    
    PAHttpRequestErrorCodeJSonParserFailed = -2000,
    PAHttpRequestErrorCodeConnectionError = -3000,
} PAHttpRequestErrorCode;

@interface PAHttpRequestError : NSError
{
    PAHttpRequest *request;
}

@property (nonatomic, readonly) NSInteger responseCode;
@property (nonatomic, readonly) PAHttpRequest *request;

- (id)initWithRequest:(PAHttpRequest *)request errorCode:(NSInteger)code andMessage:(NSString *)message;

@end
