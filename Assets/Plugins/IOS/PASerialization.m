//
//  PASerialization.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 5/22/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PASerialization.h"

@implementation PASerialization

+ (void)saveObject:(id)object forKey:(id)key
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:object];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:key];
}

+ (id)loadObjectForKey:(id)key
{
    NSData* data = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    if (!data)
    {        
        return nil;
    }
    
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

@end
