//
//  PABannerCategoryExtractor.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 3/25/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PABannerCategoryExtractor.h"
#import "PADebug.h"

@implementation PABannerCategoryExtractor

- (id)initWithJson:(PAAdJsonHelper *)json
{
    self = [super init];
    if (self)
    {        
    }
    return self;
}

- (NSString *)tryExtractCategoryFromJSon:(PAAdJsonHelper *)json
{
    PAAssertMsg(false, @"PABannerCategoryExtractor::tryExtractCategoryFromResponseString should be overriden in subclass");
    return nil;
}

@end
