//
//  PAUIStatistics.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 2/10/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#if PA_AD_STATISTICS

#import <Foundation/Foundation.h>

#define PAUISetCacheSize(x) [[PAUIStatistics sharedInstance] setCacheSize:(x)]
#define PAUISetCacheDisabled(x) [[PAUIStatistics sharedInstance] setCacheDisabled]
#define PAUISetCachePopulating(x) [[PAUIStatistics sharedInstance] setCachePopulating]
#define PAUISetCacheFull(x) [[PAUIStatistics sharedInstance] setCacheFull]

#define PAUISetLandingPageNotLoaded(x) [[PAUIStatistics sharedInstance] setLandingPageNotLoaded]
#define PAUISetLandingPageLoading(x) [[PAUIStatistics sharedInstance] setLandingPageLoading]
#define PAUISetLandingPageLoaded(x) [[PAUIStatistics sharedInstance] setLandingPageLoaded]

#define PAUISetPageNotLoaded(x) [[PAUIStatistics sharedInstance] setPageNotLoaded]
#define PAUISetPageLoading(x) [[PAUIStatistics sharedInstance] setPageLoading]
#define PAUISetPageLoaded(x) [[PAUIStatistics sharedInstance] setPageLoaded]

#define PAUISetStatus(x) [[PAUIStatistics sharedInstance] setStatus:(x)]
#define PAUISetLocationStatus(x) [[PAUIStatistics sharedInstance] setLocationStatus:(x)]

@interface PAUIStatistics : NSObject
{
    UILabel* cacheLabel;
    UILabel* landingLabel;
    UILabel* pageLabel;
    UILabel* statusLabel;
    UILabel* locationLabel;
}

@property (nonatomic, retain) IBOutlet UILabel* cacheLabel;
@property (nonatomic, retain) IBOutlet UILabel* landingLabel;
@property (nonatomic, retain) IBOutlet UILabel* pageLabel;
@property (nonatomic, retain) IBOutlet UILabel* statusLabel;
@property (nonatomic, retain) IBOutlet UILabel* locationLabel;

+ (PAUIStatistics *)sharedInstance;

- (void)setCacheSize:(NSUInteger)size;
- (void)setCacheDisabled;
- (void)setCachePopulating;
- (void)setCacheFull;

- (void)setLandingPageNotLoaded;
- (void)setLandingPageLoading;
- (void)setLandingPageLoaded;

- (void)setPageNotLoaded;
- (void)setPageLoading;
- (void)setPageLoaded;

- (void)setStatus:(NSString *)text;

- (void)setLocationStatus:(NSString *)text;

@end

#else  // #if PA_AD_STATISTICS

#define PAUISetCacheSize(x)
#define PAUISetCacheDisabled(x)
#define PAUISetCachePopulating(x)
#define PAUISetCacheFull(x)

#define PAUISetLandingPageNotLoaded(x)
#define PAUISetLandingPageLoading(x)
#define PAUISetLandingPageLoaded(x)

#define PAUISetPageNotLoaded(x)
#define PAUISetPageLoading(x)
#define PAUISetPageLoaded(x)

#define PAUISetStatus(x)

#define PAUISetLocationStatus(x)

#endif // #if PA_AD_STATISTICS