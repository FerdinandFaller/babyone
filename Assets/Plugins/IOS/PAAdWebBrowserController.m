//
//  PAAdWebBrowserController.m
//  PlacePlayAds Sample
//
//  Created by Alex Lementuev on 16.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import "PAAdWebBrowserController.h"

#import "PADebug.h"
#import "WebAdGestureRecognizer.h"
#import "PAAppStoreHandler.h"

#import "PAUIStatistics.h"

#import "patimer.h"

@interface PAAdWebBrowserController (Private)

- (void)updateBrowserControls;
- (void)showLoadingAnimation;
- (void)hideLoadingAnimation;
- (void)onPageLoadingStart;
- (void)onPageLoadingFinish;

- (void)startBackgroundLoadingWithDelegate:(id<PAAdWebBrowserBackgroundLoadingDelegate>)aDelegate;
- (void)stopBackgroundLoading;
- (void)stopBackgroundLoadingWithError:(NSError *)error;

- (BOOL)needCannotCloseTimer;
- (void)startCannotCloseTimer;
- (void)cancelCannotCloseTimer;
- (void)onCannotCloseTimer:(NSTimer *)timer;

- (void)notifyAdCycleStatisticsPageLoaded:(BOOL)succeed;
- (void)sendAdCycleStatisticsIfDone;

- (void)notifyDelegate:(id)aDelegate selector:(SEL)selector;
- (void)notifyDelegate:(id)aDelegate selector:(SEL)selector withError:(NSError *)error;

- (void)webViewTapped;
- (void)setWebViewClickTimeStamp;
- (void)clearWebViewClickTimeStamp;
- (BOOL)webViewClickTimeStampValid;

- (BOOL)trySpecialHandlerForURL:(NSURL *)url;

@end

@implementation PAAdWebBrowserController

@synthesize delegate;
@synthesize backgroundDelegate;

@synthesize viewControllerForPresenting;
@synthesize lastWebViewTapTimeStamp;
@synthesize closeButtonTimeout;
@synthesize adCycleStatistics;
@synthesize landingPageURL;

@synthesize webView;

@synthesize toolBar;
@synthesize closeButton;
@synthesize backButton;
@synthesize forwardButton;
@synthesize reloadButton;

- (id)init
{
    self = [super initWithNibName:@"PAAdWebBrowserController" bundle:nil];
    if (self)
    {        
    }
    return self;
}

- (void)dealloc
{
    self.adCycleStatistics = nil;
    self.landingPageURL = nil;
    [super dealloc];
}

#pragma mark - 
#pragma mark View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.    
    self.backButton.enabled = NO;
    self.forwardButton.enabled = NO;
    
    WebAdGestureRecognizer *gestureRecognizer = [[WebAdGestureRecognizer alloc] initWithTarget:self action:@selector(webViewTapped)];
    [self.webView addGestureRecognizer:gestureRecognizer];
    [gestureRecognizer release];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.webView.delegate = nil;
    self.webView = nil;
    self.toolBar = nil;
    self.closeButton = nil;
    self.backButton = nil;
    self.forwardButton = nil;
    self.reloadButton = nil;
    self.lastWebViewTapTimeStamp = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark - 
#pragma mark Loading

- (void)presentWithController:(UIViewController *)viewController andRequest:(NSURLRequest *)request
{
    if (backgroundLoading)
    {
        PAUISetLandingPageNotLoaded();
    }
    
    [self stopBackgroundLoading];
    
    self.viewControllerForPresenting = viewController;
    self.landingPageURL = request.URL;
    
    [viewController presentModalViewController:self animated:YES];
    initialLoadFlag = YES;
    initialLoadFinishedFlag = NO;

    [self updateBrowserControls];
    
    self.view.hidden = NO;
    self.webView.alpha = 0;
    
    pageFullyLoadedAtLeastOnce = NO;
    [self.webView loadRequest:request];
    [self.adCycleStatistics notifyPageStartedLoading];
    
    PAUISetPageLoading();
}

- (void)presentWithController:(UIViewController *)viewController
{
    self.viewControllerForPresenting = viewController;
    [viewController presentModalViewController:self animated:YES];    
    self.view.hidden = NO;
}


#pragma mark -
#pragma mark Background loading

- (void)loadHtmlInBackground:(NSString *)htmlString withDelegate:(id<PAAdWebBrowserBackgroundLoadingDelegate>)aDelegate
{
    @synchronized (self)
    {                
        [self startBackgroundLoadingWithDelegate:aDelegate];
        
        PALogDebug(@"Browser is loading html in backgroud: %@", htmlString);
        
        pageFullyLoadedAtLeastOnce = NO;
        [self.webView loadHTMLString:htmlString baseURL:nil];
    }
}

- (void)loadURLInBackground:(NSString *)urlString withDelegate:(id<PAAdWebBrowserBackgroundLoadingDelegate>)aDelegate
{
    @synchronized (self)
    {
        [self startBackgroundLoadingWithDelegate:aDelegate];
        
        PALogDebug(@"Browser is loading url in backgroud: %@", urlString);
        NSURL *url = [[NSURL alloc] initWithString:urlString];
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:60.0];
        [self.webView loadRequest:request];
        [request release];
        [url release];
    }
}

- (void)startBackgroundLoadingWithDelegate:(id<PAAdWebBrowserBackgroundLoadingDelegate>)aDelegate
{
    @synchronized (self)
    {
        if (self.view.superview == nil)
        {
            PALogWarn(@"Browser wasn't shown yet. Put it into the root view controller.");
            
            UIView *parentView = [UIApplication sharedApplication].keyWindow;
            self.view.hidden = YES;
            [parentView addSubview:self.view];
        }
        
        PAUISetLandingPageLoading();
        
        backgroundDelegate = aDelegate;
        backgroundLoading = YES;
    }
}

- (void)stopBackgroundLoading
{
    @synchronized (self)
    {
        [self notifyDelegate:backgroundDelegate selector:@selector(adWebBrowserBackgroundLoadingFinished:)];
        backgroundDelegate = nil;
        backgroundLoading = NO;
    }
}

- (void)stopBackgroundLoadingWithError:(NSError *)error
{
    @synchronized (self)
    {
        [self notifyDelegate:backgroundDelegate selector:@selector(adWebBrowserBackgroundLoadingFailed:withError:) withError:error];
        backgroundDelegate = nil;
        backgroundLoading = NO;
        
        PAUISetLandingPageNotLoaded();
    }
}

#pragma mark -
#pragma mark Background controls

- (void)updateBrowserControls
{
    if (initialLoadFinishedFlag)
    {
        self.backButton.enabled = self.webView.canGoBack && ![self.landingPageURL isEqual:self.webView.request.URL];
        self.forwardButton.enabled = self.webView.canGoForward;
    }
    else 
    {
        self.backButton.enabled = NO;
        self.forwardButton.enabled = NO;
    }
}

- (void)showLoadingAnimation
{
    NSArray *barItems = self.toolBar.items;
    NSMutableArray *newBarItems = [[NSMutableArray alloc] initWithCapacity:barItems.count];
    for (id barItem in barItems)
    {
        if (barItem == self.reloadButton)
        {
            UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];            
            activityView.frame = CGRectMake(0, 0, 18, 18);
            UIBarButtonItem *loadingButton = [[UIBarButtonItem alloc] initWithCustomView:activityView];
            [activityView startAnimating];
            [newBarItems addObject:loadingButton];
            [loadingButton release];
            [activityView release];
        }
        else
        {
            [newBarItems addObject:barItem];
        }        
    }
    
    self.toolBar.items = newBarItems;
    [newBarItems release];
}

- (void)hideLoadingAnimation
{    
    NSArray *barItems = self.toolBar.items;
    NSMutableArray *newBarItems = [[NSMutableArray alloc] initWithCapacity:barItems.count];
    for (id barItem in barItems)
    {
        if ([barItem isKindOfClass:[UIBarButtonItem class]] && [[barItem customView] isKindOfClass:[UIActivityIndicatorView class]])
        {
            [newBarItems addObject:self.reloadButton];
        }
        else
        {
            [newBarItems addObject:barItem];
        }        
    }
    
    self.toolBar.items = newBarItems;
    [newBarItems release];
}

- (void)onPageLoadingStart
{
    if ([self needCannotCloseTimer])
    {    
        // don't let browser to be closed while initial load timer fires or web page fails
        closeButton.enabled = NO;
        [self startCannotCloseTimer];
    }
}

- (void)onPageLoadingFinish
{
    self.reloadButton.enabled = YES;
    self.closeButton.enabled = YES;
    
    [self cancelCannotCloseTimer];
    
    [self hideLoadingAnimation];
}

#pragma mark -
#pragma mark Cannot close timer

- (BOOL)needCannotCloseTimer
{
    return closeButtonTimeout > 0;
}

- (void)startCannotCloseTimer
{
    @synchronized (self)
    {
        PALogDebug(@"html based start first load timer: %f", closeButtonTimeout);
        scheduleTimer(&cannotCloseTimer, closeButtonTimeout, self, @selector(onCannotCloseTimer:), nil, NO);
    }
}

- (void)cancelCannotCloseTimer
{
    @synchronized (self)
    {
        PALogDebug(@"html based cancel first load timer");
        invalidateTimer(&cannotCloseTimer);
    }    
}

- (void)onCannotCloseTimer:(NSTimer *)timer
{
    @synchronized (self)
    {
        PALogDebug(@"html based first load timer fired");
        cannotCloseTimer = nil;
        closeButton.enabled = YES;
    }
}

#pragma mark -
#pragma mark Delegate notification

- (void)notifyDelegate:(id)aDelegate selector:(SEL)selector
{
    if ([aDelegate respondsToSelector:selector])
    {
        [aDelegate performSelector:selector withObject:self];
    }
}

- (void)notifyDelegate:(id)aDelegate selector:(SEL)selector withError:(NSError *)error
{
    if ([aDelegate respondsToSelector:selector])
    {
        [aDelegate performSelector:selector withObject:self withObject:error];
    }
}

#pragma mark -
#pragma mark Delayed notifications

- (void)startDelayedPageLoadNotification
{
    @synchronized (self)
    {
        scheduleTimer(&delayedNotificationTimer, 0.5, self, @selector(onDelayedNotificationTimer:), nil, NO);
    }
}

- (void)cancelDelayedPageLoadNotification
{
    @synchronized (self)
    {
        invalidateTimer(&delayedNotificationTimer);
    }
}

- (void)onDelayedNotificationTimer:(NSTimer *)timer
{
    @synchronized (self)
    {
        delayedNotificationTimer = nil;
        
        PALogDebug(@"Web browser delayed page loading notification");
        pageFullyLoadedAtLeastOnce = YES;
        
        [self onPageLoadingFinish];
        
        if (backgroundLoading)
        {
            [self stopBackgroundLoading];
            PAUISetLandingPageLoaded();
        }
        else
        {
            [self notifyAdCycleStatisticsPageLoaded:YES];
            PAUISetPageLoaded();
            [self notifyDelegate:self.delegate selector:@selector(adWebBrowserPageDidFinishLoad:)];
        }
    }    
}

#pragma mark -
#pragma mark Landing statistics

- (void)notifyAdCycleStatisticsPageLoaded:(BOOL)succeed
{
    [self.adCycleStatistics notifyPageFinishedLoadingSucceed:succeed];
    [self sendAdCycleStatisticsIfDone];
}

- (void)sendAdCycleStatisticsIfDone
{
    if (self.adCycleStatistics != nil && [self.adCycleStatistics isPageLoaded] && [self.adCycleStatistics isPageClosed])
    {
        [self.adCycleStatistics sendToServer];
        self.adCycleStatistics = nil;
    }
}

#pragma mark -
#pragma mark Actions

- (IBAction)close:(id)sender 
{    
    [self cancelDelayedPageLoadNotification];
    [self cancelCannotCloseTimer];
    
    [self.viewControllerForPresenting dismissModalViewControllerAnimated:YES];
    
    [self.adCycleStatistics notifyPageClosed];
    [self sendAdCycleStatisticsIfDone];
    
    [self notifyDelegate:self.delegate selector:@selector(adWebBrowserClosed:)];
    
    self.delegate = nil;
    self.landingPageURL = nil;
}

- (IBAction)forward:(id)sender 
{
    if ([self.webView canGoForward])
    {
        [self.webView goForward];
    }
    else
    {
        PAAssertMsg([self.webView canGoForward], @"Can't go forward");
    }
}

- (IBAction)back:(id)sender 
{ 
    if ([self.webView canGoBack])
    {
        [self.webView goBack];
    }
    else
    {
        PAAssertMsg([self.webView canGoBack], @"Can't go backward");
    }
}

- (IBAction)reload:(id)sender 
{
    [self.webView reload];
}

#pragma mark -
#pragma mark UIWebViewDelegate Methods

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (!backgroundLoading && [self trySpecialHandlerForURL:request.URL])
    {
        shouldCloseBrowserAfterAppStore = !pageFullyLoadedAtLeastOnce;
        PALogDebug(@"Use special handler for URL: %@ close browser when done: %@", request.URL, shouldCloseBrowserAfterAppStore ? @"YES" : @"NO");        
        return NO;
    }
    
    BOOL clickTimeStampValid = [self webViewClickTimeStampValid];
    [self clearWebViewClickTimeStamp];
    
    PALogDebug(@"Web browser's shouldStartLoadWithRequest: %@", request);
    [self cancelDelayedPageLoadNotification];
    
    if (clickTimeStampValid)
    {
        if (navigationType == UIWebViewNavigationTypeLinkClicked ||
            navigationType == UIWebViewNavigationTypeFormSubmitted ||
            navigationType == UIWebViewNavigationTypeOther)
        {   
            if ([self.delegate respondsToSelector:@selector(adWebBrowserLinkClicked:withRequest:navigationType:)])
                [self.delegate adWebBrowserLinkClicked:self withRequest:request navigationType:navigationType];
        }
    }
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self updateBrowserControls];
    
    [self showLoadingAnimation];
    [self cancelDelayedPageLoadNotification];
    
    if (initialLoadFlag)
    {
        initialLoadFlag = NO;
        
        [self onPageLoadingStart];        
        // we should be sure if the delegate is notifyed only once        
        
        [self notifyDelegate:self.delegate selector:@selector(adWebBrowserPageDidStartLoad:)];
    }    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self updateBrowserControls];
    
    if (!initialLoadFinishedFlag)
    {
        initialLoadFinishedFlag = YES;
        
        [UIView beginAnimations:@"PAAdWebBrowser" context:nil];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:1.0];
        self.webView.alpha = 1.0;
        [UIView commitAnimations];
    }
    
    PALogDebug(@"Web browser's webViewDidFinishLoad");
    
    [self startDelayedPageLoadNotification];
}

- (void)webView:(UIWebView *)aWebView didFailLoadWithError:(NSError *)error
{
    @synchronized (self)
    {
        [self updateBrowserControls];
        
        [self cancelDelayedPageLoadNotification];
        [self onPageLoadingFinish];
        
        // Ignore NSURLErrorDomain error -999.
        if (error.code == NSURLErrorCancelled) return;
        
        // Ignore "Fame Load Interrupted" errors. Seen after app store links.
        if (error.code == 102 && [error.domain isEqual:@"WebKitErrorDomain"]) return;
        
        PALogDebug(@"Web browser's webViewDidFailLoadWithError: %@", error);        
        
        if (backgroundLoading)
        {
            [self stopBackgroundLoadingWithError:error];
        }
        else
        {
            if (self.delegate != nil)
            {
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error" 
                                                                message:@"Can’t load content. Please, try again later." 
                                                               delegate:nil 
                                                      cancelButtonTitle:@"OK" 
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }

            
            [self notifyAdCycleStatisticsPageLoaded:NO];
            PAUISetPageNotLoaded();
            [self notifyDelegate:self.delegate selector:@selector(adWebBrowserPageDidFinishWithError:error:) withError:error];
        }
    }
}

#pragma mark -
#pragma mark Special request handler

- (BOOL)trySpecialHandlerForURL:(NSURL *)url
{
    if ([[PAAppStoreHandler sharedInstance] tryAppStoreHandlerForURL:url delegate:self])
    {        
        return YES;
    }
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        NSString *scheme = [url scheme];
        if ([scheme isEqualToString:@"http"] || [scheme isEqualToString:@"https"])
        {
            return NO; // let browser open web pages
        }
        
        BOOL opened = [[UIApplication sharedApplication] openURL:url];
        return opened;
    }    
    
    return NO;
}

#pragma mark -
#pragma mark Secondary clicks

- (void)webViewTapped
{
    PALogDebug(@"Browser's view tapped");
    [self setWebViewClickTimeStamp];
}

- (void)setWebViewClickTimeStamp
{
    NSDate *now = [[NSDate alloc] init];
    self.lastWebViewTapTimeStamp = now;
    [now release];
}

- (void)clearWebViewClickTimeStamp
{    
    self.lastWebViewTapTimeStamp = nil;
}

- (BOOL)webViewClickTimeStampValid
{
    return self.lastWebViewTapTimeStamp != nil && -[self.lastWebViewTapTimeStamp timeIntervalSinceNow] <= kPAAdWebBrowserSecondaryClickTimeout;
}

#pragma mark -
#pragma mark PAAppStoreHandlerDelegate

- (void)appStoreDidClose:(PAAppStoreHandler *)handler
{
    if (shouldCloseBrowserAfterAppStore)
    {
        PALogInfo(@"AppStore item's page is done: closing the browser...");
        shouldCloseBrowserAfterAppStore = NO;
        [self performSelector:@selector(close:) withObject:nil afterDelay:.1];
    }
}

@end
