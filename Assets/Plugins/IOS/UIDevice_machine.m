//
//  UIDevice+machine.m
//  ViewTest
//
//  Created by Alex Lementuev on 08.07.11.
//  Copyright 2011 PressOK Entertainment. All rights reserved.
//

#import "UIDevice_machine.h"
#import "UIDevice_IdentifierAddition.h"
#include <sys/types.h>
#include <sys/sysctl.h>

/** Adobe's windows port of the iOS tool chain won't link a category
 * unless it contains at least one non-category implementation in the
 * source file. */
@interface HACKUIDCategories
@end
@implementation HACKUIDCategories
@end


@implementation UIDevice (machine)

- (NSString *)PAUuid
{
    CFUUIDRef uuidRef = CFUUIDCreate(NULL);
    NSString *uuidString = [((NSString *)CFUUIDCreateString(NULL, uuidRef)) autorelease];
    CFRelease(uuidRef);    
    return uuidString;
}

- (NSString *)PAUniqueIdentifier
{
    NSString* uniqId = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    return uniqId;
}

@end
