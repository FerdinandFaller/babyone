//
//  PAAdCycleStatistics.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 2/10/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "NSMutableDictionary_Types.h"

#import "PAAdCycleStatistics.h"
#import "PADebug.h"

#import "PAAdServerAPI.h"

#define kParamBannerLoaded              @"banner_loaded"
#define kParamPageLoaded                @"page_loaded"
#define kParamPageFullyLoaded           @"page_loaded_fully"
#define kParamPageClosed                @"page_closed"

@implementation PALoadingEvent

@synthesize startDate, duration;

- (void)dealloc
{
    [startDate release];    
    [super dealloc];
}

- (void)started
{
    @synchronized (self)
    {
        [startDate release];
        startDate = [[NSDate alloc] init];
        duration = 0;
        succeed = NO;
        finished = NO;
    }
}

- (void)finishedSucceed:(BOOL)flag
{
    @synchronized (self)
    {
        duration = -[startDate timeIntervalSinceNow];
        succeed = flag;
        finished = YES;
    }
}

- (BOOL)isFinished
{
    @synchronized (self)
    {
        return finished;
    }
}

@end

@interface PAAdCycleStatistics (Private)

- (void)statisticsRequestFinished:(PAHttpRequest *)request;
- (void)statisticsRequestFailed:(PAHttpRequest *)request withError:(NSError *)error;

@end

@implementation PAAdCycleStatistics

- (id)initWithRequestId:(NSString *)aRequestId
{
    self = [super init];
    if (self)
    {
        requestId = [aRequestId copy];
    }
    return self;
}

- (void)dealloc
{
    [requestId release];
    [bannerLoadingEvent release];
    [pageLoadingEvent release];
    [pageClosedDate release];
    
    [super dealloc];
}

- (void)notifyBannerStartedLoading
{
    @synchronized (self)
    {
        [bannerLoadingEvent release];
        bannerLoadingEvent = [[PALoadingEvent alloc] init];
        [bannerLoadingEvent started];
    }
}

- (void)notifyBannerFinishedLoadingSucceed:(BOOL)succeed
{
    @synchronized (self)
    {
        [bannerLoadingEvent finishedSucceed:succeed];
    }
}

- (void)notifyPageStartedLoading
{
    @synchronized (self)
    {
        [pageLoadingEvent release];
        [pageClosedDate release]; pageClosedDate = nil;
        
        pageLoadingEvent = [[PALoadingEvent alloc] init];
        [pageLoadingEvent started];
    }
}

- (void)notifyPageFinishedLoadingSucceed:(BOOL)succeed
{
    @synchronized (self)
    {
        [pageLoadingEvent finishedSucceed:succeed];
    }
}

- (void)notifyPageClosed
{
    @synchronized (self)
    {
        [pageClosedDate release];
        pageClosedDate = [[NSDate alloc] init];
    }
}

- (BOOL)isPageLoaded
{
    @synchronized (self)
    {
        return [pageLoadingEvent isFinished];
    }
}

- (BOOL)isPageClosed
{
    @synchronized (self)
    {
        return pageClosedDate != nil;
    }
}

- (void)sendToServer
{
    @synchronized (self)
    {
        NSTimeInterval pageCloseTime = [pageClosedDate timeIntervalSinceDate:pageLoadingEvent.startDate];
        BOOL fullyLoaded = pageLoadingEvent.duration > 0 && pageCloseTime >= pageLoadingEvent.duration;
        
        NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithCapacity:5];
        if (requestId != nil)
        {            
            [params safeSetObject:requestId forKey:kPAAdRequestParamRequestId];
        }
        else
        {
            PALogWarn(@"Sending ad cycle statistics without 'request_id' param");
        }
        [params setDoubleValue:bannerLoadingEvent.duration forKey:kParamBannerLoaded];
        [params setDoubleValue:pageLoadingEvent.duration forKey:kParamPageLoaded];
        [params setDoubleValue:pageCloseTime forKey:kParamPageClosed];
        [params setBoolValue:fullyLoaded forKey:kParamPageFullyLoaded];
        
        PALogDebug(@"Send ad cycle statistics: %@", [params description]);
        
        NSString *urlString = [[NSString alloc] initWithFormat:@"%@/%@", [PAAdServerAPI baseUrl], kPA_AdServer_StatsURL];
        PAHttpRequest *request = [[PAHttpRequest alloc] initWithURL:urlString andParams:params];
        [[PAAdServerAPI sharedInstance] queueRequest:request
                                              target:self
                                        finishAction:@selector(statisticsRequestFinished:) 
                                          failAction:@selector(statisticsRequestFailed:withError:)];
        [request release];
        [params release];
        [urlString release];
    }
}

- (void)statisticsRequestFinished:(PAHttpRequest *)request
{
    PALogDebug(@"Ad cycle statistics - OK (took %f sec)", request.duration);
}

- (void)statisticsRequestFailed:(PAHttpRequest *)request withError:(NSError *)error
{
    PALogWarn(@"Ad cycle statistics failed: %@", error);
}

@end
