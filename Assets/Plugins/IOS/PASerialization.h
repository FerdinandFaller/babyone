//
//  PASerialization.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 5/22/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PASerialization : NSObject

+ (void)saveObject:(id)object forKey:(id)key;
+ (id)loadObjectForKey:(id)key;

@end
