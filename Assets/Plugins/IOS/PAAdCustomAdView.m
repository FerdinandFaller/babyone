//
//  PAAdBannerView.m
//  PlacePlayAds Sample
//
//  Created by Alex Lementuev on 16.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import "PAAdCustomAdView.h"

#import "PABannerData.h"
#import "PADebug.h"
#import "PAAppStoreHandler.h"

@implementation PAAdCustomAdView

@synthesize delegate;
@synthesize contentDelegate;
@synthesize viewControllerForPresenting;
@synthesize bannerData;
@synthesize adCycleStatistics;

- (id)initWithBannerData:(PABannerData *)data andFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        state = PAAdCustomAdViewStateCreated;
        bannerData = [data retain];
        adCycleStatistics = [[PAAdCycleStatistics alloc] initWithRequestId:bannerData.requestId];
    }
    return self;
}

- (void)dealloc
{
    [self stop];
    
    PAAdWebBrowserController* browser = [self sharedBrowser];
    if (browser.delegate == self)
    {
        browser.delegate = nil;
    }
    
    if (browser.adCycleStatistics == self.adCycleStatistics)
    {
        browser.adCycleStatistics = nil;
    }
    
    self.delegate = nil;
    self.contentDelegate = nil;
    [bannerData release];
    [adCycleStatistics release];    
    [super dealloc];
}

- (void)loadContent
{
    PALogDebug(@"Start loading content for: %@", self.networkId);
    state = PAAdCustomAdViewStateContentLoading;
    
    [adCycleStatistics notifyBannerStartedLoading];
}

- (void)contentLoaded
{
    PALogDebug(@"Content loaded for: %@", self.networkId);
    state = PAAdCustomAdViewStateContentLoaded;   
    
    [adCycleStatistics notifyBannerFinishedLoadingSucceed:YES];
    
    [self notifyAdContentLoaded];
}

- (void)contentLoadingFailedWithError:(NSError *)error
{
    [adCycleStatistics notifyBannerFinishedLoadingSucceed:NO];
    
    [self notifyAdContentFailedWithError:error];
}

- (BOOL)isContentLoaded
{
    return state == PAAdCustomAdViewStateContentLoaded;
}

- (BOOL)isContentLoading
{
    return state == PAAdCustomAdViewStateContentLoading;
}

- (void)cancelLoadContent
{
    PALogDebug(@"Cancel loading content: %@", self.networkId);
    state = PAAdCustomAdViewStateContentNotLoaded;
    [self notifyAdContentCanceled];
}

- (void)showBrowserWithRequest:(NSURLRequest *)request
{
    if (![[PAAppStoreHandler sharedInstance] tryAppStoreHandlerForURL:request.URL delegate:self])
    {
        PAAdWebBrowserController *browser = [self sharedBrowser];
        PAAssertMsg(browser != nil, @"Try to present nil browser");
        
        browser.delegate = self;
        browser.closeButtonTimeout = self.closeButtonTimeout;
        browser.adCycleStatistics = self.adCycleStatistics;
        
        [browser presentWithController:viewControllerForPresenting andRequest:request];
    }
}

- (void)appStoreDidClose:(PAAppStoreHandler *)handler
{
    [self notifyAdClosed];
}

- (void)appStoreDidFail:(PAAppStoreHandler *)handler
{
    [self notifyAdClosed];
}

- (PAAdWebBrowserController *)sharedBrowser
{
    PAAssertMsg(false, @"-(PAAdWebBrowserController *)sharedBrowser");
    return nil;
}

- (void)start
{
    [self notifyAdStarted];
}

- (void)stop
{
    if ([self isContentLoading])
    {
        [self cancelLoadContent];
    }
    self.delegate = nil;
    self.viewControllerForPresenting = nil;
}

- (void)notifyDelegate:(id)aDelegate selector:(SEL)selector
{
    if ([aDelegate respondsToSelector:selector])
    {
        [aDelegate performSelector:selector withObject:self];
    }
}

- (void)notifyDelegate:(id)aDelegate selector:(SEL)selector withError:(NSError *)error
{
    if ([aDelegate respondsToSelector:selector])
    {
        [aDelegate performSelector:selector withObject:self withObject:error];
    }
}

- (void)notifyAdContentLoaded
{
    [self notifyDelegate:contentDelegate selector:@selector(adContentDidLoad:)];
}

- (void)notifyAdContentFailedWithError:(NSError *)error
{
    [self notifyDelegate:contentDelegate selector:@selector(adContentDidFail:withError:) withError:error];
}

- (void)notifyAdContentCanceled
{   
    [self notifyDelegate:contentDelegate selector:@selector(adContentDidCancel:)];
}

- (void)notifyAdTapped
{
    [self notifyDelegate:delegate selector:@selector(adTapped:)];
}

- (void)notifyAdSecondaryClick
{
    [self notifyDelegate:delegate selector:@selector(adTappedSecondaryClick:)];
}

- (void)notifyAdClosed
{
    [self notifyDelegate:delegate selector:@selector(adClosed:)];
}

- (void)notifyAdStarted
{
    [self notifyDelegate:delegate selector:@selector(adDidStart:)];
}

- (void)notifyAdFinished
{
    [self notifyDelegate:delegate selector:@selector(adDidFinish:)];
}

- (void)notifyAdFailed:(NSError *)error
{
    [self notifyDelegate:delegate selector:@selector(adDidFinish:withError:) withError:error];
}

#pragma mark PAAdWebBrowserControllerDelegate methods

- (void)adWebBrowserClosed:(PAAdWebBrowserController *)controller
{
    [self notifyAdClosed];
}

- (void)adWebBrowserLinkClicked:(PAAdWebBrowserController *)controller withRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    [self notifyAdSecondaryClick];
}

#pragma mark Properties

- (BOOL)needSecondaryClick
{
    return bannerData.needsSecondaryClicks;
}

- (NSTimeInterval)closeButtonTimeout
{
    return bannerData.closeButtonTimeout;
}

- (NSString *)networkId
{
    return bannerData.networkId;
}

- (NSString *)networkType
{
    return bannerData.networkType;
}

- (NSString *)requestId
{
    return bannerData.requestId;
}

- (NSInteger)landingVersion
{
    return bannerData.landingVersion;
}

@end
