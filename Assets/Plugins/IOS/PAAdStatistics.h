//
//  Foo.h
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 21.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BannerRequestInfo.h"

@interface PAAdStatistics : NSObject
{
    NSMutableArray *requests;
    
    int impressions;
    int clicks;
    
    int impressionsFailed;
    int clicksFailed;
}

@property (nonatomic, assign, readonly) int impressions;
@property (nonatomic, assign, readonly) int clicks;
@property (nonatomic, assign, readonly) int impressionsFailed;
@property (nonatomic, assign, readonly) int clicksFailed;
@property (nonatomic, readonly) BannerRequestInfo *currentRequestInfo;

+ (PAAdStatistics *)sharedInstance;

- (void)addRequestInfoWithDuration:(NSTimeInterval)duration;

- (NSArray *)requests;

- (void)addClick;
- (void)addImpression;

- (void)addClickFailed;
- (void)addImpressionFailed;

@end
