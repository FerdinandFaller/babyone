//
//  PABannerCache.m
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 1/20/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "NSMutableDictionary_Types.h"

#import "PABannerCache.h"

#import <CoreLocation/CoreLocation.h>

#import "PABannerData.h"
#import "PAHtmlBannerData.h"
#import "PAButtonBannerData.h"

#import "PAAdCustomAdView.h"
#import "PAAdHtmlBasedAdView.h"
#import "PAAdButtonBasedAdView.h"

#import "PAHttpRequest.h"
#import "PAHttpJSonRequest.h"
#import "PAAdServerAPI.h"

#import "PAAdJsonHelper.h"

#import "PADebug.h"
#import "UIDevice_machine.h"

#if PA_AD_STATISTICS
#import "PAAdStatistics.h"
#endif

#import "PAUIStatistics.h"

@interface PABannerCache (Private)

- (NSUInteger)cacheSize;
- (BOOL)hasCachedBanners;
- (BOOL)isCacheFull;
- (void)addBannerToCache:(PAAdCustomAdView *)adView;
- (PAAdCustomAdView *)peekFirstCachedBanner;
- (void)removeFirstCachedBanner;

- (void)loadBannerContentIfNeeded:(PAAdCustomAdView *)adView;

- (BOOL)canInitiateAdRequest;
- (void)tryInitiateAdRequest;
- (void)initiateAdRequest;
- (void)adRequestFinished:(PAHttpJSonRequest *)request;
- (void)adRequestFailed:(PAHttpRequest *)request withError:(NSError *)error;

- (void)notifyBannerDataReceived:(PABannerData *)bannerData;
- (void)notifyBannerReceived:(PAAdCustomAdView *)adView;
- (void)notifyFailureWithError:(NSError *)error;

- (void)notifyDelegateBannerReceived:(PAAdCustomAdView *)adView;
- (void)notifyDelegateBannerFailedWithError:(NSError *)error;

- (void)loadLandingInfoForAdView:(PAAdCustomAdView *)adView;

@end

#define kPAAdNetworkTypeHtml                    @"type_html"
#define kPAAdNetworkTypeBanner                  @"type_banner"

#define kPAAdRequestParamPublisher              @"publisher"
#define kPAAdRequestParamRequestId              @"request_id"
#define kPAAdRequestParamAdNetwork              @"ad_network"

#define kPAAdResponseParamAdNetworkType         @"ad_network_type"

@implementation PABannerCache

@synthesize delegate;

- (id)initWithDelegate:(id<PABannerCacheDelegate>)aDelegate andCapacity:(NSUInteger)aCapacity
{
    self = [super init];
    if (self)
    {
        capacity = aCapacity;
        dataArray = [[NSMutableArray alloc] initWithCapacity:aCapacity];
        landingManager = [[PALandingManager alloc] init];
        landingManager.delegate = self;
        newAdRequestsEnabled = YES;
        
        self.delegate = aDelegate;
    }
    return self;
}

- (void)dealloc
{
    [dataArray release];
    
    landingManager.delegate = nil;
    [landingManager release];
    
    [super dealloc];
}

- (void)activate
{
    @synchronized(self)
    {
        if (!active)
        {
            PALogDebug(@"Banner cache activated");
        }
        active = YES;
    }
}

- (void)deactivate
{
    @synchronized(self)
    {
        if (active)
        {
            PALogDebug(@"Banner cache deactivated");
        }
        active = NO;
        needBannerImmediately = NO;
    }
}

- (BOOL)isActive
{
    @synchronized(self)
    {
        return active;
    }
}

- (NSUInteger)cacheSize
{
    return dataArray.count;
}

- (BOOL)hasCachedBanners
{
    return [self cacheSize] > 0;
}

- (BOOL)isCacheFull
{
    return [self cacheSize] >= capacity;
}

- (void)addBannerToCache:(PAAdCustomAdView *)adView
{    
    [dataArray addObject:adView];
    PALogDebug(@"Add banner to cache (%d/%d): %@", [self cacheSize], capacity, adView.networkId);
    
    PAUISetCacheSize([self cacheSize]);
    if ([self isCacheFull])
    {
        PAUISetCacheFull();
    }
}

- (PAAdCustomAdView *)peekFirstCachedBanner
{
    PAAssertMsg([self hasCachedBanners], @"Tried to pick the banner from empty cache");
    if ([self hasCachedBanners])
    {
        return [dataArray objectAtIndex:0];
    }
    return nil;
}

- (void)removeFirstCachedBanner
{
    PAAssertMsg([self hasCachedBanners], @"Tried to remove banner from empty cache");
    if ([self hasCachedBanners])
    {
        PALogDebug(@"Remove first banner from cache (%d/%d): %@", [self cacheSize], capacity, [self peekFirstCachedBanner].networkId);
        [dataArray removeObjectAtIndex:0];
        
        PAUISetCacheSize([self cacheSize]);
    }
}

- (void)loadBannerContentIfNeeded:(PAAdCustomAdView *)adView
{
    @synchronized (self)
    {
        if ([adView isContentLoaded])
        {
            PALogDebug(@"Ad view '%@' content is loaded. Try load landing info", adView.networkId);        
            [self loadLandingInfoForAdView:adView];
        }
        else
        {
            adView.contentDelegate = self;
            [self disableAdRequests];
            
            if ([adView isContentLoading])
            {
                PALogDebug(@"Ad view '%@' content is loading. Waiting til done...", adView.networkId);
            }
            else
            {        
                PALogDebug(@"Ad view '%@' didn't try to load its content yet. So load it now!", adView.networkId);            
                [adView loadContent];

                PAUISetStatus(@"Loading banner into ad view...");
            }
        }
    }
}

#pragma mark Public interface

- (void)requestBanner
{
    @synchronized (self)
    {        
        if ([self isActive])
        {        
            PALogDebug(@"Request banner from cache");
            if ([self hasCachedBanners])
            {
                PALogDebug(@"Cache has %d banner(s)", [self cacheSize]);                
                
                if ([delegate bannerCanBeDisplayedNow])
                {                
                    // if we have cached banner - pop it from the cache and return to delegate
                    PAAdCustomAdView *nextBanner = [self peekFirstCachedBanner];
                    [self notifyBannerReceived:nextBanner];        
                    [self removeFirstCachedBanner]; // don't try to use nextBanner after this call  
                }
                else 
                {
                    PALogWarn(@"Delegate cannot display the banner now");
                }
            }
            else
            {
                PALogDebug(@"Cache is empty: requesting banner from the server");        
                needBannerImmediately = YES;
                [self tryInitiateAdRequest];
            }
        }
        else
        {
            PAAssertMsg([self isActive], @"Tried to request banner from innactive cache");
        }
    }
}

#pragma mark Banner request

- (BOOL)canInitiateAdRequest
{
    @synchronized (self)
    {
        if ([self isCacheFull])
        {
            PALogDebug(@"Cache is full: %d. Cancel ad request", capacity);
            PAUISetCacheFull();
            return NO;
        }
        else if (!newAdRequestsEnabled)
        {
            PALogDebug(@"New ad requests are not allowed");
            return NO;
        }
        else if (![self isActive])
        {
            PALogDebug(@"Banner cache is innactive. Can't make ad request");
            return NO;
        }
        return YES;
    }
}

- (void)tryInitiateAdRequest
{
    @synchronized (self)
    {
        if ([self canInitiateAdRequest])
        {
            PALogDebug(@"Request banner while cache has %d banner(s)", [self cacheSize]);
            [self initiateAdRequest];
        }
    }
}

- (void)initiateAdRequest
{   
    @synchronized (self)
    {
        NSString *uuid = [[UIDevice currentDevice] PAUuid];
        
        NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithCapacity:10];
        [params safeSetObject:kPA_AdServer_Publisher forKey:kPAAdRequestParamPublisher];
        [params safeSetObject:uuid forKey:kPAAdRequestParamRequestId];
        
        // [params setObject:@"att"      forKey:kPAAdRequestParamAdNetwork];      // ATTENTION:  This line is debug-only. Make sure to remove it in production code
        // [params setObject:@"citygrid" forKey:kPAAdRequestParamAdNetwork];      // ATTENTION:  This line is debug-only. Make sure to remove it in production code    
        // [params setObject:@"madvertise" forKey:kPAAdRequestParamAdNetwork];    // ATTENTION:  This line is debug-only. Make sure to remove it in production code    
        
        PALogDebug(@"Request banner with params: %@", [params description]);
                
        NSString *urlString = [[NSString alloc] initWithFormat:@"%@/%@", [PAAdServerAPI baseUrl], kPA_AdServer_GetAdURL];
        PAHttpRequest *request = [[PAHttpJSonRequest alloc] initWithURL:urlString andParams:params];
        [[PAAdServerAPI sharedInstance] queueRequest:request
                                              target:self
                                        finishAction:@selector(adRequestFinished:) 
                                          failAction:@selector(adRequestFailed:withError:)];
        [request release];
        [urlString release];
        [params release];
        
        PAUISetStatus(@"Requesting banner...");        
        PAUISetCachePopulating();
    }
}

- (void)adRequestFinished:(PAHttpJSonRequest *)request
{
    @synchronized (self)
    {   
        PAUISetStatus(@"Banner received");
        
        #if PA_AD_STATISTICS
        [[PAAdStatistics sharedInstance] addRequestInfoWithDuration:request.duration];
        #endif
        
        id responseObject = request.responseJson;        
        
        PALogDebug(@"Response banner with params (took %f sec): %@", request.duration, [responseObject description]);
        
        PAAdJsonHelper *json = [[PAAdJsonHelper alloc] initWithJSonObj:responseObject];        
        PABannerData *bannerData = nil;
        
        NSString *networkType = [json stringForKey:kPAAdResponseParamAdNetworkType];
        if ([networkType isEqualToString:kPAAdNetworkTypeHtml])
        {
            bannerData = [[PAHtmlBannerData alloc] initWithJSon:json andResponseData:request.responseData];
            [self notifyBannerDataReceived:bannerData];            
        }
        else if ([networkType isEqualToString:kPAAdNetworkTypeBanner])
        {        
            bannerData = [[PAButtonBannerData alloc] initWithJSon:json andResponseData:request.responseData];     
            [self notifyBannerDataReceived:bannerData];            
        }
        else 
        {
            PALogError(@"Unknown network type: '%@'", networkType);
            [self notifyFailureWithError:nil];
        }
        
        [bannerData release];
        [json release];
    }
}

- (void)adRequestFailed:(PAHttpRequest *)request withError:(NSError *)error
{    
    @synchronized (self)
    {
        PAUISetStatus(@"Can't receive the banner");
        
        #if PA_AD_STATISTICS
        [[PAAdStatistics sharedInstance].currentRequestInfo setBannerFailed];
        #endif
        
        [self notifyFailureWithError:error];
    }
}

- (void)notifyBannerDataReceived:(PABannerData *)bannerData
{
    if (bannerData == nil)
    {
        PALogError(@"Banner data is incorrect. Notifying an error....");
        [self notifyFailureWithError:nil];
        return;
    }
    
    PALogDebug(@"Banner data receivied. Need it immediately: %@; delegate can accept it: %@", needBannerImmediately ? @"YES" : @"NO", [delegate bannerCanBeDisplayedNow] ? @"YES" : @"NO");
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
   
    PAAdCustomAdView *adView = [bannerData createAdViewWithFrame:[delegate bannerFrame]];

    // if we need the banner right now - don't put it into the cache
    if ([self isActive])
    {
        if (needBannerImmediately && [delegate bannerCanBeDisplayedNow]) 
        {
            [self notifyBannerReceived:adView];
        }
        else
        {
            needBannerImmediately = NO;
            
            [self addBannerToCache:adView];
            [self tryInitiateAdRequest];
        }
    }
    else
    {
        PALogDebug(@"Banner data received while cache is innactive");
        [self addBannerToCache:adView];
    }
    
    [pool release];
}

- (void)notifyBannerReceived:(PAAdCustomAdView *)adView
{
    if ([self isActive])
    {
        PALogDebug(@"Notify delegate about received banner: %@", adView.networkId);
        [self loadBannerContentIfNeeded:adView];
        [self notifyDelegateBannerReceived:adView];
    }
    else
    {
        PAAssert([self isActive]);
    }
}

- (void)notifyFailureWithError:(NSError *)error
{
    @synchronized (self)
    {
        if ([self isActive])
        {
            PALogError(@"Unable to get PlacePlay banner: %@", error);    
            if (needBannerImmediately)
            {
                PALogDebug(@"We needed the banner immediately and it fails, so notify the delegate");
                [self notifyDelegateBannerFailedWithError:error];
            }
            else if ([error isKindOfClass:[PAHttpRequestError class]])
            {
                // if we got "no content" from the server - ask again
                PAHttpRequestError *paError = (PAHttpRequestError *)error;
                if (paError.responseCode == 204)
                {            
                    PALogDebug(@"We got 'no content' from the server. So ask again");
                    [self tryInitiateAdRequest];
                }
            }
        }
        else
        {
            PALogError(@"PlacePlay banner error occured while cache is not active: %@", error);
        }
    }
}

#pragma mark Landing loading

- (void)loadLandingInfoForAdView:(PAAdCustomAdView *)adView;
{
    if ([landingManager needLoadLandingInfoForAdView:adView])
    {
        PAUISetStatus(@"Loading landing template...");
        [landingManager loadLandingInfoForForAdView:adView];
    }
    else
    {
        PALogDebug(@"'%@' doesn't need any landing loading. Safe to populate cache", adView.networkId);
        [self enableAdRequests];
    }
}

#pragma mark Delegate notifications

- (void)notifyDelegateBannerReceived:(PAAdCustomAdView *)adView
{
    @synchronized (self)
    {
        if ([self isActive])
        {
            needBannerImmediately = NO;
            [delegate bannerReceived:self view:adView];
        }
        else
        {
            PAAssert([self isActive]);
        }
    }
}

- (void)notifyDelegateBannerFailedWithError:(NSError *)error
{
    @synchronized (self)
    {
        if ([self isActive])
        {
            [delegate bannerFailed:self withError:error];
        }
        else
        {
            PAAssert([self isActive]);
        }
    }
}

#pragma mark Helpers

- (void)disableAdRequests
{
    @synchronized (self)
    {
        PALogInfo(@"Banner cache disabled ad requests");
        newAdRequestsEnabled = NO;        
        
        PAUISetCacheDisabled();
    }
}

- (void)enableAdRequests
{
    @synchronized (self)
    {
        PALogInfo(@"Banner cache enabled ad requests");
        
        BOOL oldFlag = newAdRequestsEnabled;
        newAdRequestsEnabled = YES;
        if (!oldFlag)
        {
            [self tryInitiateAdRequest];
        }
    }
}

#pragma mark PAAdCustomAdViewContentDelegate

- (void)adContentDidLoad:(PAAdCustomAdView *)adView
{
    @synchronized (self)
    {        
        adView.contentDelegate = nil;        
        [self loadLandingInfoForAdView:adView];
    }
}

- (void)adContentDidFail:(PAAdCustomAdView *)adView withError:(NSError *)error
{
    @synchronized (self)
    {
        PALogDebug(@"Banner ad view's content loading failed");
        adView.contentDelegate = nil;
    }
}

- (void)adContentDidCancel:(PAAdCustomAdView *)adView
{
    @synchronized (self)
    {
        PALogDebug(@"Banner ad view's content loading canceled");
        adView.contentDelegate = nil;
    }
}

#pragma mark PALandingManagerDelegate

- (void)landingInfoReceived:(PALandingInfo *)info
{
    @synchronized (self)
    {
        PALogDebug(@"Landing info received. Safe to populate cache");
        [self enableAdRequests];
    }
}

- (void)landingInfoFailed:(PALandingInfo *)info withError:(NSError *)error
{
    @synchronized (self)
    {
        PALogDebug(@"Landing info failed. Move along...");
        [self enableAdRequests];
    }
}

@end
