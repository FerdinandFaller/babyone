//
//  PAHttpImageRequest.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 1/14/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PAHttpRequest.h"

@interface PAHttpImageRequest : PAHttpRequest
{
@private
    UIImage *responseImage;
    BOOL useRetinaScale;
}

@property (nonatomic, retain) UIImage *responseImage;
@property (nonatomic, assign) BOOL useRetinaScale;

@end