//
//  PAAdServerAPI.m
//  PlacePlayAds
//
//  Created by Alex Koloskov on 9/22/11.
//  Copyright 2011 PressOK Entertainment. All rights reserved.
//

#import "PAAdServerAPI.h"

#import "UIDevice_machine.h"

#import "PAAd.h"
#import "PAAdDateFormatter.h"

#define kPAAdPlatform @"iOS"
#define kPAAdUserAgent @"PlacePlay Ads 1.0 (alpha)"

#define kPAAdRequestParamAppId                  @"app_id"
#define kPAAdRequestParamUID                    @"uid"
#define kPAAdRequestParamLatitude               @"lat"
#define kPAAdRequestParamLongitude              @"lon"
#define kPAAdRequestParamGeoSource              @"geo_source"
#define kPAAdRequestParamOsVersion              @"os_version"
#define kPAAdRequestParamSDKVersion             @"sdk_version"
#define kPAAdRequestParamPlatform               @"platform"
#define kPAAdRequestParamProtocolVersion        @"protocol"
#define kPAAdRequestParamDeviceFamily           @"device_family"
#define kPAAdRequestParamAdSize                 @"ad_size"
#define kPAAdRequestParamPluginPackage          @"package"
#define kPAAdRequestParamTimestamp              @"timestamp"

#define kPAAdRequestHeaderUserAgent             @"User-Agent"

#define kPAAdRequestParamLocCity                @"city"
#define kPAAdRequestParamLocCountry             @"country"
#define kPAAdRequestParamLocRegion              @"region"
#define kPAAdRequestParamLocPostal              @"postal"
#define kPAAdRequestParamLocTimestamp           @"geo_timestamp"

#define kPAAdGeoSourceDevice                    @"device"
#define kPAAdGeoSourceIP                        @"ip"

static NSString *serverBaseUrl;
static PAAdServerAPI *instance;

@interface PAAdServerAPI (Private)

- (void)fillAppSpecificParams:(PAHttpRequest *)request;
- (NSString *)evaluateUserAgent;

@end

@implementation PAAdServerAPI

@synthesize userAgent;

- (id)initWithDataSource:(id<PAAdServerAPIDataSource>)aDataSource
{
    self = [super init];
    if (self)
    {
        dataSource = aDataSource;
        userAgent = [[self evaluateUserAgent] copy];
    }
    return self;
}

- (void)dealloc
{
    [userAgent release];
    
    [super dealloc];
}

+ (void)createSharedInstance:(id<PAAdServerAPIDataSource>)dataSource
{
    PAAssertMsg(instance == nil, @"PAAdServerAPI already exists");
    instance = [[PAAdServerAPI alloc] initWithDataSource:dataSource];
}

+ (PAAdServerAPI *)sharedInstance
{
    PAAssertMsg(instance != nil, @"PAAdServerAPI is nil");
    return instance;
}

- (void)queueRequest:(PAHttpRequest *)request target:(id)target finishAction:(SEL)finishAction failAction:(SEL)failAction
{
    [request setFinishTarget:target andAction:finishAction];
    [request setErrorTarget:target andAction:failAction];
    [self fillAppSpecificParams:request];
    [request start];
}

- (void)cancelAllRequests
{
    [[PARequestManager sharedInstance] cancelAll];
}

- (void)fillAppSpecificParams:(PAHttpRequest *)request
{
    //  Set up app Id
    [request setParam:[dataSource appId] forKey:kPAAdRequestParamAppId];
    
    //  Set up user Id
    [request setParam:[dataSource userId] forKey:kPAAdRequestParamUID];
    
    // time stamp
    [request setParam:[PAAdDateFormatter getTimestampInRFC3339FormatForDate:[NSDate date]] forKey: kPAAdRequestParamTimestamp];
    
    // location data
    PALocationInfo* locationInfo = [dataSource locationInfo];
    if ((locationInfo != nil) && [locationInfo isValid])
    {
        if (locationInfo.city) [request setParam:locationInfo.city forKey:kPAAdRequestParamLocCity];
        if (locationInfo.country) [request setParam:locationInfo.country forKey:kPAAdRequestParamLocCountry];
        if (locationInfo.region) [request setParam:locationInfo.region forKey:kPAAdRequestParamLocRegion];
        if (locationInfo.postal) [request setParam:locationInfo.postal forKey:kPAAdRequestParamLocPostal];
        if (locationInfo.timestamp) {
            [request setParam:locationInfo.timestamp forKey:kPAAdRequestParamLocTimestamp];
        }
    }
    
    //  Set up lat/lon
    CLLocation* location = [dataSource deviceLocation];
    if (location)
    {
        double lat = location.coordinate.latitude;
        double lon = location.coordinate.longitude;
        
        [request setParam:[NSString stringWithFormat:@"%f", lat] forKey:kPAAdRequestParamLatitude];
        [request setParam:[NSString stringWithFormat:@"%f", lon] forKey:kPAAdRequestParamLongitude];
        [request setParam:kPAAdGeoSourceDevice forKey:kPAAdRequestParamGeoSource];
    }
    else if ((locationInfo != nil) && [locationInfo isValid])
    {
        double lat = locationInfo.latitude;
        double lon = locationInfo.longitude;
        
        [request setParam:[NSString stringWithFormat:@"%f", lat] forKey:kPAAdRequestParamLatitude];
        [request setParam:[NSString stringWithFormat:@"%f", lon] forKey:kPAAdRequestParamLongitude];
        [request setParam:kPAAdGeoSourceIP forKey:kPAAdRequestParamGeoSource];
    }
    else
    {
        // if there's no way to get user's location - set it at Downtown Seattle
        PALogWarn(@"No location params provided!");
    }
    
    // Set other parameters
    NSString* osVersion = [UIDevice currentDevice].systemVersion;    
    [request setParam:osVersion forKey: kPAAdRequestParamOsVersion];
    
    // sdk version
    [request setParam:[dataSource sdkVersion] forKey:kPAAdRequestParamSDKVersion];
    
    // protocol version
    [request setParam:[dataSource protocolVersion] forKey:kPAAdRequestParamProtocolVersion];
    
    // platform
    [request setParam:kPAAdPlatform forKey:kPAAdRequestParamPlatform];
    
    // device family
    [request setParam:[[UIDevice currentDevice] model] forKey:kPAAdRequestParamDeviceFamily];
    
    // banner size
    if ([dataSource bannerSizeString] != nil) {
        [request setParam:[dataSource bannerSizeString] forKey:kPAAdRequestParamAdSize];
    }
    
    // targeting params
    NSDictionary* targetingParams = [dataSource targetingParams];
    if (targetingParams)
    {
        for (id key in targetingParams)
        {
            if ([key isKindOfClass:[NSString class]])
            {
                id value = [targetingParams objectForKey:key];
                [request setParam:value forKey:key];
            }
            else 
            {
                PALogError(@"Targeting params should contain string keys only");
            }
        }
    }
    
    // plugin package
    NSString *pluginPackage = [dataSource pluginPackageParam];
    if (pluginPackage)
    {
        [request setParam:pluginPackage forKey:kPAAdRequestParamPluginPackage];
    }
    
    // testing params
    NSDictionary *testingParams = [dataSource testingParams];
    if (testingParams)
    {
        for (id key in testingParams)
        {
            if ([key isKindOfClass:[NSString class]])
            {
                id value = [testingParams objectForKey:key];
                [request setParam:value forKey:key];
            }
            else 
            {
                PALogError(@"Testing params should contain string keys only");
            }
        }
    }
    
    // user agent header
    [request setHttpHeader:self.userAgent forKey:kPAAdRequestHeaderUserAgent];
    
    PALogDebug(@"Default params: %@", request.params);
    PALogDebug(@"User-agent: %@", self.userAgent);
}

- (NSString *)evaluateUserAgent
{
    UIWebView* dummyWebView = [[UIWebView alloc] initWithFrame:CGRectZero];
    NSString* navigatorUserAgent = [dummyWebView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
    [dummyWebView release];
    
    PALogInfo(@"Evaluated user-agent: %@", navigatorUserAgent);
    PAAssertMsg(navigatorUserAgent != nil, @"Unable to evaluate user agent");
    
    return navigatorUserAgent;
}

+ (void)setBaseUrl:(NSString *)baseUrl
{
    serverBaseUrl = [baseUrl copy];
}

+ (NSString *)baseUrl
{
    return serverBaseUrl == nil ? kPA_AdServer_BaseURLString : serverBaseUrl;
}

@end