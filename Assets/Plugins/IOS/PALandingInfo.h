//
//  PALandingCacheData.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 2/4/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    PALandingDataStateUndefined,    
    PALandingDataStateReceived,
    PALandingDataStateInvalid
}
PALandingDataState;

@interface PALandingInfo : NSObject
{
@private
    NSString *networkId;
    NSString *networkType;
    NSInteger version;
    
    NSString *type;
    PALandingDataState state;
    NSString *data;
    
    NSData *responseData; // ad request binary response for constructing html template (if any)
}

@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *data;

@property (nonatomic, readonly) NSString *networkId;
@property (nonatomic, readonly) NSString *networkType;
@property (nonatomic, retain) NSData *responseData;

@property (nonatomic, assign) NSInteger version;

- (id)initWithNetworkId:(NSString *)networkId andNetworkType:(NSString *)networkType;

- (void)setReceivedWithType:(NSString *)type andData:(NSString *)data;
- (void)invalidate;
- (void)clear;

- (BOOL)isValid;
- (BOOL)isInvalid;
- (BOOL)isUndefined;

@end
