//
//  PAAdBannerView.h
//  PlacePlayAds Sample
//
//  Created by Alex Lementuev on 16.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PAAdCustomAdViewDelegate.h"
#import "PAAdCustomAdViewContentDelegate.h"

#import "PAAdWebBrowserController.h"

#import "PABannerData.h"
#import "PAAdCycleStatistics.h"

@class PAAdCustomAdView;
@class PABannerData;

typedef enum {
    PAAdCustomAdViewStateCreated,
    PAAdCustomAdViewStateContentLoading,
    PAAdCustomAdViewStateContentLoaded,
    PAAdCustomAdViewStateContentNotLoaded
} PAAdCustomAdViewState;

@interface PAAdCustomAdView : UIView<PAAdWebBrowserControllerDelegate>
{
    id<PAAdCustomAdViewDelegate> delegate;
    id<PAAdCustomAdViewContentDelegate> contentDelegate;
    UIViewController *viewControllerForPresenting;
    PAAdCustomAdViewState state;    
    
    PABannerData *bannerData;
    PAAdCycleStatistics *adCycleStatistics;
}

@property (nonatomic, assign) id<PAAdCustomAdViewDelegate> delegate;
@property (nonatomic, assign) id<PAAdCustomAdViewContentDelegate> contentDelegate;
@property (nonatomic, assign) UIViewController *viewControllerForPresenting;

@property (nonatomic, readonly) BOOL needSecondaryClick;
@property (nonatomic, readonly) NSTimeInterval closeButtonTimeout;
@property (nonatomic, readonly) NSString *networkId;
@property (nonatomic, readonly) NSString *networkType;
@property (nonatomic, readonly) NSString *requestId;
@property (nonatomic, readonly) NSInteger landingVersion;
@property (nonatomic, readonly) PABannerData *bannerData;
@property (nonatomic, readonly) PAAdCycleStatistics *adCycleStatistics;

- (id)initWithBannerData:(PABannerData *)data andFrame:(CGRect)frame;

- (void)loadContent;
- (void)cancelLoadContent;
- (void)contentLoaded;
- (void)contentLoadingFailedWithError:(NSError *)error;
- (BOOL)isContentLoaded;
- (BOOL)isContentLoading;

- (void)start;
- (void)stop;

- (void)showBrowserWithRequest:(NSURLRequest *)request;
- (PAAdWebBrowserController *)sharedBrowser;

- (void)notifyAdContentLoaded;
- (void)notifyAdContentFailedWithError:(NSError *)error;
- (void)notifyAdContentCanceled;
- (void)notifyAdTapped;
- (void)notifyAdSecondaryClick;
- (void)notifyAdClosed;
- (void)notifyAdStarted;
- (void)notifyAdFinished;
- (void)notifyAdFailed:(NSError *)error;

@end
