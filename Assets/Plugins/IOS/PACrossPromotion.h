//
//  PACrossPromotion.h
//  PlacePlayAdsDebugSample
//
//  Created by Soumya Behera on 25/07/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAAdDelegate.h"

@interface PACrossPromotion : NSObject <PAAdDelegate>
{
    NSString *appIdString;
}

@property (nonatomic, retain) NSString *appIdString;

+ (id)sharedInstance;
- (void)trackInstallationForAppID:(NSString *)appId;

@end
