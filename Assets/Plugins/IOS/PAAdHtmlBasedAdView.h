//
//  PAAdHtmlBasedAdView.h
//  PlacePlayAds Sample
//
//  Created by Alex Lementuev on 16.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PAAdCustomAdView.h"
#import "WebAdGestureRecognizer.h"

@interface PAAdHtmlBasedAdView : PAAdCustomAdView<UIWebViewDelegate>
{
    BOOL webViewTapped;
    BOOL fullscreenAdShowed;
    
    NSTimer *subBannerTimer;
    int subBannersShownCount;
    
    BOOL firstLoad;
    WebAdGestureRecognizer *tapRecognizer;
}

@property (nonatomic, retain) WebAdGestureRecognizer *tapRecognizer;

@property (nonatomic, readonly) NSUInteger subBannersCount;
@property (nonatomic, readonly) NSString *htmlSnippet;

+ (void)readBannerPageFromFile:(NSString *)networkId;

@end
