//
//  PAHttpRequest.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 1/13/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PAHttpRequestError.h"

typedef enum {
	PAHttpRequestStateNotStarted,
	PAHttpRequestStateStarted,
	PAHttpRequestStateFinished,
	PAHttpRequestStateFinishedError,
    PAHttpRequestStateCanceled
} PAHttpRequestState;

@interface PAHttpRequest : NSObject
{
@protected
    NSString *urlString;
    NSMutableDictionary *params;
    NSMutableDictionary *headers;
    NSString *method;
    
    NSTimeInterval timeout;
    
    NSURLConnection *connection;
    NSInteger responseCode;
    NSMutableData *responseData;
    
    id finishTarget;
    SEL finishAction;
    
    id errorTarget;
    SEL errorAction;
    
    id cancelTarget;
    SEL cancelAction;
    
    PAHttpRequestState state;
    
    NSDate *startDate;
    NSTimeInterval duration;
    
    id userData;
    int tag;
}

@property (nonatomic, copy) NSString *urlString;
@property (nonatomic, retain) NSDictionary *params;
@property (nonatomic, retain) NSDictionary *headers;
@property (nonatomic, readonly) NSTimeInterval timeout;
@property (nonatomic, retain) NSURLConnection *connection;
@property (nonatomic, readonly) NSInteger responseCode;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSDate *startDate;
@property (nonatomic, readonly) NSTimeInterval duration;
@property (nonatomic, retain) id userData;
@property (nonatomic, assign) int tag;

- (id)initWithURL:(NSString *)urlString;
- (id)initWithURL:(NSString *)urlString andTimeout:(NSTimeInterval)timeout;
- (id)initWithURL:(NSString *)urlString andParams:(NSDictionary *)params;
- (id)initWithURL:(NSString *)urlString andTimeout:(NSTimeInterval)timeout andParams:(NSDictionary *)params;
- (id)initWithURL:(NSString *)urlString andTimeout:(NSTimeInterval)timeout andParams:(NSDictionary *)params andHeaders:(NSDictionary *)headers;

- (void)setParam:(id)param forKey:(id)key;
- (void)setHttpHeader:(id)value forKey:(id)key;

- (void)start;
- (void)cancel;

- (void)setMethodPost;
- (void)setMethodGet;

- (void)setFinishTarget:(id)target andAction:(SEL)action;
- (void)setErrorTarget:(id)target andAction:(SEL)action;
- (void)setCancelTarget:(id)target andAction:(SEL)action;

@end
