//
//  PABannerCategoryChainExtractor.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 3/25/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PABannerCategoryChainExtractor.h"
#import "PABannerCategoryFactory.h"

#import "PADebug.h"

#define kParamChain         @"chain"

@implementation PABannerCategoryChainExtractor

- (id)initWithJson:(PAAdJsonHelper *)json
{
    self = [super initWithJson:json];
    if (self)
    {        
        if (![PAAdJsonHelper allParamsArePresent:json, kParamChain, nil])
        {
            PAAssertMsg(false, @"PABannerCategoryChainExtractor is missing required params");
            
            [self release];
            return nil;
        }
        
        childExtractors = [[NSMutableArray alloc] init];
        
        NSArray* objects = [json arrayForKey:kParamChain];
        for (id obj in objects) 
        {
            if ([obj isKindOfClass:[NSDictionary class]])
            {
                PAAdJsonHelper* childJson = [[PAAdJsonHelper alloc] initWithJSonObj:obj];
                
                PABannerCategoryExtractor* childExtractor = NULL;
                [PABannerCategoryFactory tryParseCategoryExtractorFromJson:childJson pointer:&childExtractor];
                [childJson release];
                
                PAAssertMsgv(childExtractor != nil, @"Unable to parse child extractor for chain: %@", obj);
                
                if (childExtractor != nil)
                {
                    [childExtractors addObject:childExtractor];
                    [childExtractor release];
                }
            }
            else 
            {
                PAAssertMsg(@"Unexpected chain entry class: %@", NSStringFromClass([obj class]));                
                [childExtractors release];
                [self release];
                return nil;
            }
        }
    }
    return self;
}

- (void)dealloc
{
    [childExtractors release];
    [super dealloc];
}

- (NSString *)tryExtractCategoryFromJSon:(PAAdJsonHelper *)json
{
    for (PABannerCategoryExtractor *extractor in childExtractors)
    {
        NSString* category = [extractor tryExtractCategoryFromJSon:json];
        if (category != nil)
        {
            return category;
        }
    }
    
    return nil;
}

@end
