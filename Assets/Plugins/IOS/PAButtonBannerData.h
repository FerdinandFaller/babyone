//
//  PAImageBannerData.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 1/22/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PABannerData.h"

@interface PAButtonBannerData : PABannerData
{
    NSString *imageUrl;
    NSString *clickUrl;
}

@property (nonatomic, readonly) NSString *imageUrl;
@property (nonatomic, readonly) NSString *clickUrl;

@end
