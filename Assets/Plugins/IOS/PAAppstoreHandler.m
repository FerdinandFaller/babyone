//
//  PAAppStoreHandler.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 4/17/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PAAppStoreHandler.h"
#import "PAAd.h"

static PAAppStoreHandler *instance = nil;

@interface PAAppStoreHandler (Private)

- (void)addSuspendObserver;
- (void)removeSuspendObserver;

- (void)resignActive:(NSNotification *)notification;
- (void)becomeActive:(NSNotification *)notification;

@end

@implementation PAAppStoreHandler

- (void)dealloc
{
    [self removeSuspendObserver];
    [super dealloc];
}

- (BOOL)tryAppStoreHandlerForURL:(NSURL *)url delegate:(id)aDelegate
{
    NSString* scheme = [url scheme];    
    if ([scheme isEqualToString:@"itms-apps"] || [scheme isEqualToString:@"itms"])
    {
        delegate = aDelegate;
        return [self tryOpenAppStoreURL:url];
    } 
    
    return NO;
}

- (BOOL)tryOpenAppStoreURL:(NSURL *)url
{
    PAAssert(url != nil);
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        PALogDebug(@"Try to open AppStore for the URL: %@", url);
        
        BOOL succeed = [[UIApplication sharedApplication] openURL:url];
        
        PAAssert(!appstoreRequested);
        appstoreRequested = succeed;
        
        if (succeed)
        {
            PALogDebug(@"[UIApplication openURL] succeed");
            [self addSuspendObserver];
            
            if ([delegate respondsToSelector:@selector(appStoreWillOpen:)])
                [delegate appStoreWillOpen:self];
        }
        else 
        {
            PALogDebug(@"[UIApplication openURL] failed");
            if ([delegate respondsToSelector:@selector(appStoreDidFail:)])
                [delegate appStoreDidFail:self];
            delegate = nil;
        }
        
        return succeed;
    }
    
    PALogError(@"[UIApplication openURL] is unable to handle the following URL: %@", url);
    return NO;
}

#pragma mark - Suspend/Resume observers

- (void)addSuspendObserver
{
    NSNotificationCenter *notifCenter = [NSNotificationCenter defaultCenter];
    [notifCenter addObserver:self
                    selector:@selector(resignActive:)
                        name:UIApplicationWillResignActiveNotification
                      object:nil];
    [notifCenter addObserver:self
                    selector:@selector(becomeActive:)
                        name:UIApplicationDidBecomeActiveNotification
                      object:nil];
}

- (void)removeSuspendObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)becomeActive:(NSNotification *)notification
{   
    PAAssert(appstoreRequested);
    PALogDebug(@"Application become active. App store open requested: %@", appstoreRequested ? @"true" :  @"false");
    
    if (appstoreRequested)
    {
        if ([delegate respondsToSelector:@selector(appStoreDidClose:)])
            [delegate appStoreDidClose:self];        
    }
    
    delegate = nil;
    appstoreRequested = NO;
    [self removeSuspendObserver];
}

- (void)resignActive:(NSNotification *)notification
{
    PAAssert(appstoreRequested);
    PALogDebug(@"Application resign active. App store open requested: %@", appstoreRequested ? @"true" :  @"false");
}

+ (PAAppStoreHandler *)sharedInstance
{
    if (!instance)
    {
        instance = [[PAAppStoreHandler alloc] init];
    }
    return instance;
}

@end
