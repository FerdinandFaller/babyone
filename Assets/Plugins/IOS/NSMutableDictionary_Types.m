//
//  NSDictionary+Types.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 2/10/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "NSMutableDictionary_Types.h"

#import "PADebug.h"

/** Adobe's windows port of the iOS tool chain won't link a category
 * unless it contains at least one non-category implementation in the
 * source file. */
@interface HACKNSMDCategories 
@end

@implementation HACKNSMDCategories
@end

@implementation NSMutableDictionary (Types)

- (void)setIntValue:(NSInteger)value forKey:(NSString *)key
{
    NSNumber *number = [[NSNumber alloc] initWithInt:value];
    [self safeSetObject:number forKey:key];
    [number release];    
}

- (void)setUIntValue:(NSUInteger)value forKey:(NSString *)key
{
    NSNumber *number = [[NSNumber alloc] initWithUnsignedInt:value];
    [self safeSetObject:number forKey:key];
    [number release];    
}

- (void)setBoolValue:(BOOL)value forKey:(NSString *)key
{
    NSNumber *number = [[NSNumber alloc] initWithBool:value];
    [self safeSetObject:number forKey:key];
    [number release];    
}

- (void)setDoubleValue:(double)value forKey:(NSString *)key
{
    NSNumber *number = [[NSNumber alloc] initWithDouble:value];
    [self safeSetObject:number forKey:key];
    [number release];        
}

- (void)safeSetObject:(id)object forKey:(NSString *)key
{
    if (object != nil)
    {
        [self setObject:object forKey:key];
    }
    else
    {
        PAAssertMsgv(object != nil, @"Attempt to set nil object for key '%@'", key);
    }
}

@end
