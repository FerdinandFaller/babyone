//
//  BannerRequestInfo.h
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 21.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "StopWatch.h"

typedef enum {
    PAAdBannerRequestInfoStateStarted = 0,
    PAAdBannerRequestInfoStateBannerFailed = 1,
    PAAdBannerRequestInfoStatePageFailed = 2,
    PAAdBannerRequestInfoStateSucceed = 3
} PAAdBannerRequestInfoState;

@interface BannerRequestInfo : NSObject
{
    NSString *name;
    NSTimeInterval responseDuration;
    StopWatch *pageStopWatch;
    
    PAAdBannerRequestInfoState requestState;
}

@property (nonatomic, retain) NSString *name;
@property (nonatomic, assign, readonly) StopWatch *pageStopWatch;
@property (nonatomic, assign) NSTimeInterval responseDuration;

- (void)setBannerFailed;
- (void)setPageFailed;
- (void)setSucceed;

@end
