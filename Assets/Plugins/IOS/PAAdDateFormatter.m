//
//  PAAdDateFormatter.m
//  PlacePlayAdsDebugSample
//
//  Created by Soumya Behera on 10/07/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PAAdDateFormatter.h"

@implementation PAAdDateFormatter

+ (NSString *)getTimestampInRFC3339FormatForDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
    dateFormatter.timeZone = [NSTimeZone localTimeZone];
    NSString *dateString = [dateFormatter stringFromDate:date];
    [dateFormatter release];
    
    NSDateFormatter *timezoneFormatter = [[NSDateFormatter alloc] init];
    timezoneFormatter.dateFormat = @"Z";
    timezoneFormatter.timeZone = [NSTimeZone localTimeZone];
    NSString *timezone = [timezoneFormatter stringFromDate:date];
    NSString *timeZoneString = [NSString stringWithFormat:@"%@:%@", [timezone substringToIndex:3], [timezone substringFromIndex:3]];
    [timezoneFormatter release];
    
    NSString *timestamp = [NSString stringWithFormat:@"%@%@", dateString, timeZoneString];
    
    return timestamp;
}

+ (NSDate *)dateForTimestampInRFC3339Format:(NSString *)timestamp
{
    NSString *dateString = [timestamp substringToIndex:([timestamp length] - 1) - 5];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
    
    NSDate *date;
    NSError *error;
    BOOL success = [formatter getObjectValue:&date forString:dateString range:nil error:&error];
    if (!success) {
        date = nil;
    }
    [formatter release];
    
    return date;
}

@end
