//
//  PAUnityScriptMessenger.h
//  Unity-iPhone
//
//  Created by Alex Lementuev on 10/23/12.
//
//

#import <Foundation/Foundation.h>

@interface PAUnityScriptMessenger : NSObject
{
    NSString *_targetObjectName;
    NSString *_targetMethodName;
}

- (id)initWithTargetObject:(NSString *)objectName andTargetMethod:(NSString *)methodName;

- (void)sendMessage:(NSString *)message;

@end
