//
//  PABannerData.h
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 1/20/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PAAdJsonHelper.h"
#import "PAAdCustomAdView.h"
#import "PALandingInfo.h"

@interface PABannerData : NSObject
{
    NSString *requestId;
    NSString *networkType;
    NSString *networkId;
    NSTimeInterval timeout;
    BOOL needsSecondaryClicks;
    NSTimeInterval closeButtonTimeout;
    NSInteger landingVersion;
    NSString* category;
    NSData *responseData; // binary source for this banner data
    BOOL cacheResponse;
}

@property (nonatomic, readonly) NSString *requestId;
@property (nonatomic, readonly) NSString *networkType;
@property (nonatomic, readonly) NSString *networkId;
@property (nonatomic, readonly) NSTimeInterval timeout;
@property (nonatomic, readonly) BOOL needsSecondaryClicks;
@property (nonatomic, readonly) NSTimeInterval closeButtonTimeout;
@property (nonatomic, readonly) NSInteger landingVersion;
@property (nonatomic, retain) NSData *responseData;
@property (nonatomic, readonly) BOOL cacheResponse;
@property (nonatomic, readonly) NSString* category;

- (id)initWithJSon:(PAAdJsonHelper *)json andResponseData:(NSData *)data;;
- (PAAdCustomAdView *)createAdViewWithFrame:(CGRect)frame;

- (PALandingInfo *)landingInfo;

@end
