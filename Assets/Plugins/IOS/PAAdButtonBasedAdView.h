//
//  PAAdButtonBasedAdView.h
//  PlacePlayAds Sample
//
//  Created by Alex Lementuev on 16.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PAAdCustomAdView.h"
#import "PAHttpRequest.h"

@interface PAAdButtonBasedAdView : PAAdCustomAdView
{
    NSTimer *bannerTimer;
    
    BOOL fullscreenAdShowed;
    PAHttpRequest *imageRequest;
}

@property (nonatomic, retain) PAHttpRequest *imageRequest;

@property (nonatomic, readonly) NSString *imageUrl;
@property (nonatomic, readonly) NSString *clickUrl;

@end
