//
//  PAHttpRequest+Inheritance.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 1/14/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PAHttpRequest.h"

@interface PAHttpRequest (Inheritance)

- (NSURLRequest *)createRequest;
- (void)setHttpHeaders:(NSMutableURLRequest *)request;

- (void)releaseConnection;
- (void)cancelConnection;

- (void)finish;
- (void)finishWithError:(NSError *)error;
- (void)finishWithErrorMessage:(NSString *)message andErrorCode:(NSInteger)code;

- (BOOL)isCanceled;

- (void)notifyFinishTarget;
- (void)notifyErrorTarget:(NSError *)error;
- (void)notifyCancelTarget;

@end
