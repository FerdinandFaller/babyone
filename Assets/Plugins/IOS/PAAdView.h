//
//  PAAdView2.h
//  PlacePlayAdsSample
//
//  Created by Alex Koloskov on 10/17/11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import "PAAdDelegate.h"
#import "PAAdCustomAdViewDelegate.h"
#import "PABannerCache.h"

#import "PAAdServerAPI.h"
#import "PALocationInfo.h"
#import "PALocationManager.h"

#define kPAAdSdkVersion @"0.7.3"
#define KPAAdProtocolVersion @"4"
#define kPAAdPlatform @"iOS"

typedef enum {
    PAAdViewAnchorTop = 1,
    PAAdViewAnchorLeft = 2,  
    PAAdViewAnchorRight = 4,  
    PAAdViewAnchorBottom = 8,  
    PAAdViewAnchorHorCenter = 16,  
    PAAdViewAnchorVerCenter = 32 
} PAAdViewAnchor;

@class PAAdCustomAdView;
@protocol PAAdDelegate;

@interface PAAdView : UIView <PAAdCustomAdViewDelegate, PABannerCacheDelegate, PAAdServerAPIDataSource>
{
    id<PAAdDelegate>            _delegate;
@private
    BOOL                        modalViewShown;
    
    PALocationManager*          _locationManager;
    
    PAAdCustomAdView*           _currentAdView;
    
    NSUInteger adRequestRetriesCount;    
    NSTimer *retryAdRequestTimer;
    NSArray *adTimeoutData;
    
    BOOL initFinished;
    BOOL active;
    
    PAADViewBannerSize bannerSize;    
    PABannerCache *bannerCache;    
    PALocationInfo *locationInfo_;
    CLLocation *location_;
    NSTimeInterval locationExpiryInterval;
}

@property (nonatomic, retain) PAAdCustomAdView *currentAdView;
@property (nonatomic, retain) PABannerCache *bannerCache;
@property (nonatomic, retain) NSArray *adTimeoutData;
@property (nonatomic, retain) PALocationInfo *locationInfo;
@property (nonatomic, retain) CLLocation *location;

#pragma mark Class Methods

/**
 * Call this method to get the singleton instance of the adView
 */
+ (PAAdView*)sharedAdView:(id<PAAdDelegate>)delegate;

#pragma mark Instance Methods

/**
 *  Call this method to track the app installation.
 */
- (void)trackInstall;

/**
 *  Call this method to request new fresh ad. It will also set up an auto refresh with the default refresh interval
 */
- (void)requestAd;

/**
 *  Call this method to stop ad view activity.
 */
- (void)stop;

/**
 * Call this method to request new fresh ad.
 */
- (void)rollover;

/**
 *  Call this method for quick view alignment in the parent view
 */
- (void)alignInSuperview:(PAAdViewAnchor) anchor;

/**
 * Returns true is ad view is active and is able to make ad requests
 */
- (BOOL)isActive;

#pragma mark Properties

@property (nonatomic, assign) IBOutlet id<PAAdDelegate> delegate;

@end // @interface PAAdView