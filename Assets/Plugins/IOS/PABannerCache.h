//
//  PABannerCache.h
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 1/20/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PAAdCustomAdView.h"
#import "PAAdCustomAdViewContentDelegate.h"

#import "PALandingManager.h"

@class PABannerCache;

@protocol PABannerCacheDelegate <NSObject>

- (void)bannerReceived:(PABannerCache *)cache view:(PAAdCustomAdView *)bannerView;
- (void)bannerFailed:(PABannerCache *)cache withError:(NSError *)error;

- (BOOL)bannerCanBeDisplayedNow;
- (CGRect)bannerFrame;

@end

@interface PABannerCache : NSObject<PAAdCustomAdViewContentDelegate, PALandingManagerDelegate>
{
    NSMutableArray *dataArray;
    id<PABannerCacheDelegate> delegate;
    
    PALandingManager *landingManager;
    
    int capacity;
    BOOL needBannerImmediately;
    BOOL newAdRequestsEnabled;
    BOOL active;
}

@property (nonatomic, assign) id<PABannerCacheDelegate> delegate;

- (id)initWithDelegate:(id<PABannerCacheDelegate>)delegate andCapacity:(NSUInteger)capacity;

- (void)activate;
- (void)deactivate;
- (BOOL)isActive;

- (void)requestBanner;

- (void)enableAdRequests;
- (void)disableAdRequests;

@end
