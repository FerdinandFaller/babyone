//
//  PABannerCategoryRegExExtractor.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 3/25/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PABannerCategoryRegExExtractor.h"
#import "PADebug.h"

#import "PAAdJsonHelper.h"

#define kParamPattern           @"regexp"
#define kParamTemplate          @"tmpl"
#define kParamJSonKey           @"key"

@implementation PABannerCategoryRegExExtractor

@synthesize pattern, capture, key;

- (id)initWithJson:(PAAdJsonHelper *)json
{
    self = [super initWithJson:json];
    if (self)
    {
        if (![PAAdJsonHelper allParamsArePresent:json, kParamPattern, kParamTemplate, kParamJSonKey, nil])
        {
            PAAssertMsg(false, @"PABannerCategoryRegExExtractor is missing required params");
            
            [self release];
            return nil;
        }
        
        self.pattern = [json stringForKey:kParamPattern];
        self.capture = [json stringForKey:kParamTemplate];
        self.key = [json stringForKey:kParamJSonKey];
    }
    return self;
}

- (void)dealloc
{
    self.pattern = nil;
    self.capture = nil;
    self.key = nil;
    [super dealloc];
}

- (NSString *)tryExtractCategoryFromJSon:(PAAdJsonHelper *)json
{    
    if (!PA_CLASS_AVAILABLE(NSRegularExpression))
    {
        return nil;
    }
    
#if PA_IOS_SDK_AVAILABLE(__IPHONE_4_0)
    NSString* searchString = [json stringForKey:self.key];
    PAAssertMsgv(searchString != nil, @"Unable to find '%@' key string in json response", searchString);
    
    if (searchString == nil)
    {
        return nil;
    }
    
    NSError *error = NULL;
    NSRegularExpression *regex = [[NSRegularExpression alloc] initWithPattern:self.pattern 
                                                                      options:NSRegularExpressionCaseInsensitive 
                                                                        error:&error];    
    NSString* category = nil;
    if (error)
    {
        PAAssertMsgv(false, @"Regex error: %@\n%@", pattern, error);
    }
    else 
    {        
        NSArray *matches = [regex matchesInString:searchString
                                          options:0
                                            range:NSMakeRange(0, [searchString length])];
        
        NSRange emptyRange = NSMakeRange(NSNotFound, 0);
        for (NSTextCheckingResult *match in matches) 
        {
            NSRange matchRange = [match range];
            if (NSEqualRanges(matchRange, emptyRange))
            {
                continue;
            }
            
            // pick first non empty match
            category = [regex replacementStringForResult:match
                                                inString:searchString
                                                  offset:0
                                                template:self.capture];            
            break;            
        }
    }
    [regex release];
    
    return category;
#endif // PA_IOS_SDK_AVAILABLE(__IPHONE_4_0)
}

@end
