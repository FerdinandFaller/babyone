//
//  PAAdStatisticsTableViewCell.h
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 22.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PAAdStatisticsTableViewCell : UITableViewCell
{
    UILabel *nameLabel;
    UILabel *bannerLoadLabel;
    UILabel *pageLoadLabel;
}

@property (nonatomic, retain) IBOutlet UILabel *nameLabel;
@property (nonatomic, retain) IBOutlet UILabel *bannerLoadLabel;
@property (nonatomic, retain) IBOutlet UILabel *pageLoadLabel;

@end
