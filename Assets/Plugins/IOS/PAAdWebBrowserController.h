//
//  PAAdWebBrowserController.h
//  PlacePlayAds Sample
//
//  Created by Alex Lementuev on 16.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PAAdCycleStatistics.h"

@class PAAdWebBrowserController;

@protocol PAAdWebBrowserControllerDelegate<NSObject>

@optional
- (void)adWebBrowserOpened:(PAAdWebBrowserController *)controller;
- (void)adWebBrowserPageDidStartLoad:(PAAdWebBrowserController *)controller;
- (void)adWebBrowserPageDidFinishLoad:(PAAdWebBrowserController *)controller;
- (void)adWebBrowserPageDidFinishWithError:(PAAdWebBrowserController *)controller error:(NSError *)error;
- (void)adWebBrowserClosed:(PAAdWebBrowserController *)controller;

- (void)adWebBrowserLinkClicked:(PAAdWebBrowserController *)controller withRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;

@end

@protocol PAAdWebBrowserBackgroundLoadingDelegate<NSObject>

@optional

- (void)adWebBrowserBackgroundLoadingFinished:(PAAdWebBrowserController *)controller;
- (void)adWebBrowserBackgroundLoadingFailed:(PAAdWebBrowserController *)controller withError:(NSError *)error;

@end

#define kPAAdWebBrowserSecondaryClickTimeout 0.5

@interface PAAdWebBrowserController : UIViewController<UIWebViewDelegate>
{
    id<PAAdWebBrowserControllerDelegate> delegate;
    id<PAAdWebBrowserBackgroundLoadingDelegate> backgroundDelegate;
    UIViewController *viewControllerForPresenting;
    
    UIWebView *webView;
    UIToolbar *toolBar;
    
    UIBarButtonItem *closeButton;
    UIBarButtonItem *backButton;
    UIBarButtonItem *forwardButton;
    UIBarButtonItem *reloadButton;
    
    BOOL initialLoadFlag; // true if we load the very first request in the browser
    BOOL initialLoadFinishedFlag; // true if we load the very first request in the browser    
    
    // secondary clicks
    NSDate *lastWebViewTapTimeStamp;    
    
    // delayed notification
    NSTimer *delayedNotificationTimer;
    BOOL pageFullyLoadedAtLeastOnce;
    BOOL shouldCloseBrowserAfterAppStore;
    
    // close button hack
    NSTimeInterval closeButtonTimeout;
    NSTimer *cannotCloseTimer;
    
    // background loading
    BOOL backgroundLoading;
    
    // landing statistics
    PAAdCycleStatistics *adCycleStatistics;
    
    // history
    NSURL *landingPageURL;
}

@property (nonatomic,assign) id<PAAdWebBrowserControllerDelegate> delegate;
@property (nonatomic,assign) id<PAAdWebBrowserBackgroundLoadingDelegate> backgroundDelegate;

@property (nonatomic,assign) UIViewController *viewControllerForPresenting;
@property (nonatomic,retain) NSDate *lastWebViewTapTimeStamp;
@property (nonatomic,assign) NSTimeInterval closeButtonTimeout;
@property (nonatomic,retain) PAAdCycleStatistics *adCycleStatistics;
@property (nonatomic,copy) NSURL *landingPageURL;

@property (nonatomic,retain) IBOutlet UIWebView *webView;
@property (nonatomic,retain) IBOutlet UIToolbar *toolBar;
@property (nonatomic,retain) IBOutlet UIBarButtonItem *closeButton;
@property (nonatomic,retain) IBOutlet UIBarButtonItem *backButton;
@property (nonatomic,retain) IBOutlet UIBarButtonItem *forwardButton;
@property (nonatomic,retain) IBOutlet UIBarButtonItem *reloadButton;

- (void)presentWithController:(UIViewController *)viewController andRequest:(NSURLRequest *)request;
- (void)presentWithController:(UIViewController *)viewController;

- (void)loadHtmlInBackground:(NSString *)htmlString withDelegate:(id<PAAdWebBrowserBackgroundLoadingDelegate>)delegate;
- (void)loadURLInBackground:(NSString *)urlString withDelegate:(id<PAAdWebBrowserBackgroundLoadingDelegate>)delegate;

- (IBAction)close:(id)sender;
- (IBAction)back:(id)sender;
- (IBAction)forward:(id)sender;
- (IBAction)reload:(id)sender;

@end
