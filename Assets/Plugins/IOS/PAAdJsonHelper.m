//
//  PAAdJsonHelper.m
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 27.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import "PAAdJsonHelper.h"

#import "PADebug.h"

@implementation PAAdJsonHelper

@synthesize jsonObj;
@synthesize parent;

- (id)initWithJSonObj:(id)aJsonObj parent:(PAAdJsonHelper *)aParent
{
    self = [super init];
    if (self)
    {
        self.jsonObj = aJsonObj;
        parent = aParent;
    }
    return self;
}

- (id)initWithJSonObj:(id)aJsonObj
{
    return [self initWithJSonObj:aJsonObj parent:nil];
}

- (void)dealloc
{
    self.jsonObj = nil;
    [super dealloc];
}

- (BOOL)contains:(id)key
{
    if ([self.jsonObj isKindOfClass:[NSDictionary class]])
    {
        return [self.jsonObj objectForKey:key] != nil;
    }
    return NO;
}

- (id)objForKey:(id)key
{
    return [self objForKey:key def:nil];
}

- (id)objForKey:(id)key def:(id)defaultValue
{
    if ([self.jsonObj isKindOfClass:[NSDictionary class]])
    {
        id obj = [self.jsonObj objectForKey:key];
        return obj == nil ? defaultValue : obj;
    }
    return defaultValue;
}

- (NSArray *)arrayForKey:(id)key
{
    id obj = [self objForKey:key];
    if ([obj isKindOfClass:[NSArray class]])
    {
        return obj;
    }
    return nil;
}

- (BOOL)jsonForKey:(id)key jsonObj:(PAAdJsonHelper **)outJson
{
    id obj = [self objForKey:key];
    if ([obj isKindOfClass:[NSDictionary class]])
    {
        if (outJson != NULL)
        {
            *outJson = [[PAAdJsonHelper alloc] initWithJSonObj:obj parent:self];
            return YES;
        }
    }
    return NO;
}

- (NSString *)stringForKey:(id)key
{
    return [self stringForKey:key def:nil];
}

- (NSString *)stringForKey:(id)key def:(NSString *)defaultValue
{
    id obj = [self objForKey:key];
    if ([obj isKindOfClass:[NSString class]])
        return obj;
    return defaultValue;
}

- (float)floatForKey:(id)key
{
    return [self floatForKey:key def:0.0];
}

- (float)floatForKey:(id)key def:(float)defaultValue
{
    id obj = [self objForKey:key];
    if ([obj respondsToSelector:@selector(floatValue)])
        return [obj floatValue];
    
    return defaultValue;
}

- (BOOL)boolForKey:(id)key
{
    return [self boolForKey:key def:NO];
}

- (BOOL)boolForKey:(id)key def:(BOOL)defaultValue
{
    id obj = [self objForKey:key];
    if ([obj respondsToSelector:@selector(boolValue)])
        return [obj boolValue];
    
    return defaultValue;
}

- (NSInteger)intForKey:(id)key
{
    return [self intForKey:key def:0];
}

- (NSInteger)intForKey:(id)key def:(NSInteger)defaultValue
{
    id obj = [self objForKey:key];
    if ([obj respondsToSelector:@selector(intValue)])
        return [obj intValue];
    return defaultValue;
}

+ (BOOL)allParamsArePresent:(PAAdJsonHelper *)json, ...
{
    BOOL allPresent = YES;
    
    NSString *name;
    va_list namesList;
    va_start(namesList, json);
    while ((name = va_arg(namesList, NSString*)) != nil)
    {
        if (![json contains:name])
        {
            PALogError(@"Missing param: '%@'", name);
            allPresent = NO;
            break;
        }
    }
    va_end(namesList);    
    return allPresent;
}


@end
