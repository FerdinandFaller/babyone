//
//  PAAdView.m
//  PlacePlayAdsSample
//
//  Created by Alex Koloskov on 10/17/11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import "PAAdView_Internal.h"

#include <sys/socket.h>
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>

#import "NSData_PPBase64.h"
#import "NSString_PAExtensions.h"
#import "UIDevice_machine.h"
#import "NSMutableDictionary_Types.h"

#import "PAAdDelegate.h"
#import "PAAdServerAPI.h"

#import "PAAdButtonBasedAdView.h"
#import "PAAdHtmlBasedAdView.h"

#if PA_AD_STATISTICS
#import "PAAdStatistics.h"
#import "BannerRequestInfo.h"
#endif

#import "PAUIStatistics.h"

#import "patimer.h"

#import "PADebug.h"
#import "PAAdJsonHelper.h"
#import "PASerialization.h"

#import "PAHttpRequest.h"
#import "PAHttpJSonRequest.h"
#import "PAHttpImageRequest.h"
#import "PARequestManager.h"

#import "PABannerData.h"
#import "PALocationInfo.h"

#define kPAAdInitResponseAdRetryTimeout         @"ad_request_timeout_pattern"
#define kPAAdInitResponseAdCacheSize            @"ad_cache_size"
#define kPAAdInitResponseLocationInfo           @"location"
#define kPAAdInitResponseLocationExpiry         @"location_expiry_interval"

#define kPAAdKeyFallbackLocationInfo            @"pa_location_info"
#define kPAAdKeyFallbackLocation                @"pa_location"

#define kPAAdDefaultInitRequestTimeout 3.0
#define kPAAdDefaultCacheSize 1
#define kPAAdDefaultRetryPattern @"5.0 5.0 5.0 5.0 5.0"

#define kPAAdDefaultLocManagerUpdateInterval 600.0

#define kPAInstallTrack @"install_track"

static PAAdView* singletonAdView = nil;

NSString* const kPAADTargetPropertyKeywords             = @"keywords";
NSString* const kPAADTargetPropertyGender               = @"gender";
NSString* const kPAADTargetPropertyAge                  = @"age";
NSString* const kPAADTargetPropertyMarital              = @"marital";
NSString* const kPAADTargetPropertyIncome               = @"income";
NSString* const kPAADTargetPropertyEdu                  = @"edu";
NSString* const kPAADTargetPropertyEth                  = @"eth";

NSString* const kPAADGenderMale                  = @"m";
NSString* const kPAADGenderFemale                = @"f";
NSString* const kPAADMaritalSingle               = @"0";
NSString* const kPAADMaritalMarried              = @"1";
NSString* const kPAADIncome15000                 = @"1";
NSString* const kPAADIncome15000_19999           = @"2";
NSString* const kPAADIncome20000_29999           = @"3";
NSString* const kPAADIncome30000_39999           = @"4";
NSString* const kPAADIncome40000_49999           = @"5";
NSString* const kPAADIncome50000_74999           = @"6";
NSString* const kPAADIncome75000_99999           = @"7";
NSString* const kPAADIncome100000_124999         = @"8";
NSString* const kPAADIncome125000_149999         = @"9";
NSString* const kPAADIncome150000                = @"10";
// education
NSString* const kPAADEduHighSchool               = @"0";
NSString* const kPAADEduCollege                  = @"1";
NSString* const kPAADEduBachelor                 = @"2";
NSString* const kPAADEduGraduate                 = @"3";
// ethnic group
NSString* const kPAADEthnicAfricanAmerican       = @"0";
NSString* const kPAADEthnicAsianAmerican         = @"1";
NSString* const kPAADEthnicMediterranean         = @"2";
NSString* const kPAADEthnicNativeAmerican        = @"3";
NSString* const kPAADEthnicScandinavian          = @"4";
NSString* const kPAADEthnicPolynesian            = @"5";
NSString* const kPAADEthnicMiddleEastern         = @"6";
NSString* const kPAADEthnicJewish                = @"7";
NSString* const kPAADEthnicWesternEuropean       = @"8";
NSString* const kPAADEthnicEasternEuropean       = @"9";
NSString* const kPAADEthnicMiscellaneousOther    = @"10";
NSString* const kPAADEthnicOceania               = @"11";
NSString* const kPAADEthnicHispanic              = @"12";

typedef enum {
    PAAdBannerAnimationTypeFlipFromLeft   = 0,
    PAAdBannerAnimationTypeFlipFromRight  = 1,
    PAAdBannerAnimationTypeCurlUp         = 2,
    PAAdBannerAnimationTypeCurlDown       = 3,
    PAAdBannerAnimationTypeSlideFromLeft  = 4,
    PAAdBannerAnimationTypeSlideFromRight = 5,
    PAAdBannerAnimationTypeFadeIn         = 6,
    PAAdBannerAnimationTypesCount         = 7
} PAAdBannerAnimationType;

@interface PAAdView (Private)

+ (PAAdView*) createAdViewWithDelegate:(id<PAAdDelegate>) delegate;
- (void)updateDelegate:(id<PAAdDelegate>) delegate;
- (id)initWithDelegate:(id<PAAdDelegate>) delegateArg;

- (void)setupInitParams:(PAAdJsonHelper *)json;
- (void)setupInitDefaults;
- (void)initBannerCache:(NSUInteger)capacity;
- (void)initRetryDataWithPattern:(NSString *)patternString;
- (void)initLocationInfo:(PAAdJsonHelper *)json andExpiryInterval:(NSTimeInterval)expiryInterval;
- (void)initLocationInfoFromSaved;
- (void)initLocationManager;

- (void)requestInit;

- (BOOL)canRequestAd;

- (void)initiateInitRequest;
- (void)initiateAdRequest;
- (void)scheduleAdRequestRetry:(BOOL)immediate;
- (void)cancelAdRequestRetryTimer;
- (void)onAdRequestRetryTimer:(NSTimer *)timer;

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation;

- (void)notifyAdReceive;
- (void)notifyAdFailure:(NSError *)error;
- (void)notifyAdTotalFailure;
- (void)notifyAdFinish;
- (void)notifyFullscreenModalView;
- (void)notifyFullscreenModalViewDismiss;

- (BOOL)shouldNotifyTotalFailureIfError:(NSError *)error;
- (BOOL)shouldImmediateRetryIfError:(NSError *)error;

- (void)setupCustomAdView:(PAAdCustomAdView *)adView;
- (void)transitionToCustomAdView:(PAAdCustomAdView *)view;

- (void)queueRequest:(PAHttpRequest *)request finishAction:(SEL)finishAction failAction:(SEL)failAction;

- (void)initRequestFinished:(PAHttpJSonRequest *)request;
- (void)initRequestFailed:(PAHttpRequest *)request withError:(NSError *)error;

- (void)installTrackingFinishedAction:(PAHttpRequest *)request;
- (void)installTrackingFailAction:(PAHttpRequest *)request withError:(NSError *)error;

- (void)trackImpression;
- (void)trackImpressionRequestFinished:(PAHttpRequest *)request;
- (void)trackImpressionRequestFailed:(PAHttpRequest *)request withError:(NSError *)error;

- (void)trackClick:(BOOL)isPrimary;
- (void)trackClickRequestFinished:(PAHttpRequest *)request;
- (void)trackClickRequestFailed:(PAHttpRequest *)request withError:(NSError *)error;
- (void)trackSecondaryClickRequestFinished:(PAHttpRequest *)request;
- (void)trackSecondaryClickRequestFailed:(PAHttpRequest *)request withError:(NSError *)error;

- (NSUInteger)adRetriesCount;
- (NSTimeInterval)adRetryTimeoutForRetryIndex:(NSUInteger)attemptIndex;

NSString* navTypToString(UIWebViewNavigationType type);
CGRect frameFromBannerSize(PAADViewBannerSize bannerSize);

- (void)destroy;

- (void)setModalViewShowing:(BOOL)flag;
- (BOOL)isModalViewShowing;
- (BOOL)checkValidityOfLocation:(CLLocation *)aLocation withExpiryInterval:(NSTimeInterval)expiry;

@end

@implementation PAAdView

@synthesize delegate    = _delegate;
@synthesize currentAdView = _currentAdView;
@synthesize bannerCache;
@synthesize adTimeoutData;
@synthesize locationInfo=locationInfo_;
@synthesize location=location_;

// ===============================================================================
// Public Methods
// ===============================================================================

+ (PAAdView*) sharedAdView:(id<PAAdDelegate>)delegate {
    @synchronized (self) {
        if (singletonAdView == nil) {
            singletonAdView = [self createAdViewWithDelegate:delegate];
        } else {
            [singletonAdView updateDelegate:delegate];
        }
    }
    
    return singletonAdView;
}

// ------------------------------------------------------------------------------------------------------------------------------------------

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self) {
        if (singletonAdView == nil) {
            singletonAdView = [super allocWithZone:zone];
        }
    }
    
    return singletonAdView;
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

/**
 *  Call this method to create a view object that you can add to your own view. You will need to provide a delegate that acts as bridge between your app
 *  and PlacePlay Ads SDK.
 */
+ (PAAdView*) createAdViewWithDelegate:(id<PAAdDelegate>) delegate {
	
	PAAvailabilityDetectSystemVersion();
	
	if (!PA_SYSTEM_VERSION_AVAILABLE(PA_SYSTEM_VERSION_MIN))
	{
		PALogError(@"Current iOS system version is not supported: %@", [UIDevice currentDevice].systemVersion);
        
        if ([delegate respondsToSelector:@selector(didFailedIncompatibility)])
            [delegate didFailedIncompatibility];
        else if ([delegate respondsToSelector:@selector(didFailToReceiveAd:)])
            [delegate didFailToReceiveAd:nil];
        
		return nil;
	}
	
    //  Setup notifications
    if(![delegate respondsToSelector: @selector(viewControllerForPresentingModalView)]) {
        [NSException  raise: @"PAIncompleteDelegateException"
                     format: @"Delegate must implement viewControllerForPresentingModalView"];
    }
    
    if(![delegate respondsToSelector: @selector(appId)]) {
        [NSException  raise: @"PAIncompleteDelegateException"
                     format: @"Delegate must implement appId"];
    }
    
    return [[[PAAdView alloc] initWithDelegate:delegate] autorelease];
}

- (void) updateDelegate:(id<PAAdDelegate>) delegate {
    if (delegate != nil)
    {
        self.delegate = nil;
        
        if(![delegate respondsToSelector: @selector(viewControllerForPresentingModalView)]) {
            [NSException  raise: @"PAIncompleteDelegateException"
                         format: @"Delegate must implement viewControllerForPresentingModalView"];
        }
        
        if(![delegate respondsToSelector: @selector(appId)]) {
            [NSException  raise: @"PAIncompleteDelegateException"
                         format: @"Delegate must implement appId"];
        }
    }
    self.delegate = delegate;
    if (bannerSize == PAADViewBannerSizeUndefined) {
        PAADViewBannerSize aBannerSize = PAADViewBannerSize320x50;
        if ([self.delegate respondsToSelector:@selector(bannerSize)]) {
            bannerSize = [self.delegate bannerSize];
        } else {
            bannerSize = aBannerSize;
        }
        self.frame = frameFromBannerSize(bannerSize);
    }
}

- (void)requestAd
{
    PALogDebug(@"Base URL: %@", [PAAdServerAPI baseUrl]);
    [self activate];
    if (_locationManager == nil) {
        [self initLocationManager];
    }
    
    if (!initFinished)
    {
        PAUISetLandingPageNotLoaded();
        [self requestInit];
    }
    else
    {
        [self initiateAdRequest];
    }
}

- (void)stop
{
    PALogInfo(@"Stop PlacePlayAds");
    [self deactivate];
    
    [self cancelAdRequestRetryTimer];
    [self.currentAdView stop];
}

- (void)rollover
{
    PALogDebug(@"Rollover");
    [self requestAd];
}

- (void) alignInSuperview:(PAAdViewAnchor)anchor
{
    if (self.superview != nil)
    {
        CGSize parentSize = self.superview.bounds.size;
        CGSize size = self.bounds.size;
        
        float cx, cy;
        
        if ((anchor & PAAdViewAnchorHorCenter) != 0)
        {
            cx = 0.5f * parentSize.width;
        }
        else if ((anchor & PAAdViewAnchorRight) != 0)
        {
            cx = parentSize.width - 0.5f * size.width;
        }
        else
        {
            cx = 0.5f * size.width;
        }
        
        if ((anchor & PAAdViewAnchorVerCenter) != 0)
        {
            cy = 0.5f * parentSize.height;
        }
        else if ((anchor & PAAdViewAnchorBottom) != 0)
        {
            cy = parentSize.height - 0.5f * size.height;
        }
        else
        {
            cy = 0.5f * size.height;
        }
        
        self.center = CGPointMake(cx, cy);
    }
}

// ===============================================================================
// Private Methods
// ===============================================================================

- (id) initWithDelegate:(id<PAAdDelegate>) delegateArg
{
    PAADViewBannerSize aBannerSize = PAADViewBannerSize320x50;
    if ([delegateArg respondsToSelector:@selector(bannerSize)])
    {
        aBannerSize = [delegateArg bannerSize];
    }
    
    if(self = [super initWithFrame:frameFromBannerSize(aBannerSize)])
    {
        bannerSize = aBannerSize;
        self.delegate = delegateArg;
        
        self.backgroundColor = [UIColor clearColor];
        // to prevent ugly artifacts if ad network banners are bigger than the
        // default frame
        self.clipsToBounds = YES;
        
        _locationManager = nil;
        self.locationInfo = nil;
        
        self.location = [PASerialization loadObjectForKey:kPAAdKeyFallbackLocation];
        
        [PAAdServerAPI createSharedInstance:self];
    }
    return self;
}

- (void)initLocationManager
{
    // location services usage
    BOOL delegateRespondsToLocationServicesUsageMethod = [self.delegate respondsToSelector:@selector(paLocationServicesEnabled)];
    if (!delegateRespondsToLocationServicesUsageMethod || (delegateRespondsToLocationServicesUsageMethod && [self.delegate paLocationServicesEnabled]))
    {
        if(![_delegate respondsToSelector:@selector(location)])
        {
            _locationManager = [[PALocationManager alloc] initWithUpdateInterval:kPAAdDefaultLocManagerUpdateInterval];
            [_locationManager startUpdatingLocation];
        } else {
            _locationManager = nil;
        }
    }
}

// ------------------------------------------------------------------------------------------------------------------------------------------

- (void)activate
{
    @synchronized (self)
    {
        if (!active)
        {
            PALogDebug(@"PAAdView activated");
        }
        active = YES;
        [self.bannerCache activate];
    }
}

- (void)deactivate
{
    @synchronized (self)
    {
        if (active)
        {
            PALogDebug(@"PAAdView deactivated");
        }
        active = NO;
        [self.bannerCache deactivate];
    }
}

- (BOOL)isActive
{
    @synchronized (self)
    {
        return active;
    }
}

- (void)setModalViewShowing:(BOOL)flag
{
    @synchronized(self)
    {
        PALogDebug(@"PAAdView set modal view shown: %@ was: %@", flag ? @"YES" : @"NO", modalViewShown ? @"YES" : @"NO");
        PAAssertMsgv(modalViewShown ^ flag, @"Set modal view shown: %@ was: %@", flag ? @"YES" : @"NO", modalViewShown ? @"YES" : @"NO");
        modalViewShown = flag;
    }
}

- (BOOL)isModalViewShowing
{
    @synchronized(self)
    {
        return modalViewShown;
    }
}

- (void) destroy
{
    [_locationManager release], _locationManager = nil;
    self.bannerCache = nil;
    self.adTimeoutData = nil;
    self.locationInfo = nil;
    self.location = nil;
}

- (id)retain
{
    return self;
}

- (oneway void)release
{
    // do nothing
}

- (id)autorelease
{
    return self;
}

- (NSUInteger)retainCount
{
    return NSUIntegerMax;
}

// ------------------------------------------------------------------------------------------------------------------------------------------

- (void) initiateAdRequest
{
    [bannerCache requestBanner];
}

// ------------------------------------------------------------------------------------------------------------------------------------------

- (void)setupCustomAdView:(PAAdCustomAdView *)adView
{
    PAUISetPageNotLoaded();
    
    [self transitionToCustomAdView:adView];
    [self notifyAdReceive];
    
    [self trackImpression];
    
    [adView start];
}

static BOOL randSeeded = NO;

- (void)transitionToCustomAdView:(PAAdCustomAdView *)newAdView
{
    UIView *currentAdView = self.currentAdView;
    
    if (currentAdView)
    {
        if (!randSeeded)
        {
            srandom(CFAbsoluteTimeGetCurrent());
            randSeeded = YES;
        }
        PAAdBannerAnimationType animType = random() % PAAdBannerAnimationTypesCount;
        
        switch (animType)
        {
            case PAAdBannerAnimationTypeSlideFromLeft:
            {
                CGRect f = newAdView.frame;
                f.origin.x = -f.size.width;
                newAdView.frame = f;
                [self addSubview:newAdView];
                break;
            }
            case PAAdBannerAnimationTypeSlideFromRight:
            {
                CGRect f = newAdView.frame;
                f.origin.x = self.frame.size.width;
                newAdView.frame = f;
                [self addSubview:newAdView];
                break;
            }
            case PAAdBannerAnimationTypeFadeIn:
                newAdView.alpha = 0;
                [self addSubview:newAdView];
                break;
            default:
                // no setup required for other animation types
                break;
        }
        
        [UIView beginAnimations:@"PAAdTransition" context:[currentAdView retain]];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(onAdAnimationStop:finished:context:)];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:1.0];
        // cache has to set to NO because of VideoEgg
        switch (animType)
        {
            case PAAdBannerAnimationTypeFlipFromLeft:
                [self addSubview:newAdView];
                [currentAdView removeFromSuperview];
                [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft
                                       forView:self
                                         cache:NO];
                break;
            case PAAdBannerAnimationTypeFlipFromRight:
                [self addSubview:newAdView];
                [currentAdView removeFromSuperview];
                [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
                                       forView:self
                                         cache:NO];
                break;
            case PAAdBannerAnimationTypeCurlUp:
                [self addSubview:newAdView];
                [currentAdView removeFromSuperview];
                [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp
                                       forView:self
                                         cache:NO];
                break;
            case PAAdBannerAnimationTypeCurlDown:
                [self addSubview:newAdView];
                [currentAdView removeFromSuperview];
                [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown
                                       forView:self
                                         cache:NO];
                break;
            case PAAdBannerAnimationTypeSlideFromLeft:
            case PAAdBannerAnimationTypeSlideFromRight:
            {
                CGRect f = newAdView.frame;
                f.origin.x = 0;
                newAdView.frame = f;
                break;
            }
            case PAAdBannerAnimationTypeFadeIn:
                newAdView.alpha = 1.0;
                break;
            default:
                [self addSubview:newAdView];
                break;
        }
        [UIView commitAnimations];
    }
    else
    {
        [self addSubview:newAdView];
    }
    
    [self.currentAdView stop];
    
    self.currentAdView = newAdView;
    self.currentAdView.delegate = self;
    self.currentAdView.viewControllerForPresenting = [self.delegate viewControllerForPresentingModalView];
    
}

- (void)onAdAnimationStop:(NSString *)animationID
                 finished:(BOOL)finished
                  context:(void *)context
{
    UIView *adViewToRemove = (UIView *)context;
    [adViewToRemove removeFromSuperview];
    [adViewToRemove release]; // was retained before beginAnimations
}

#pragma mark - Tracking

- (void)trackInstall
{
    PALogDebug(@"Track install started");
    bannerSize = PAADViewBannerSizeUndefined;
    NSString *urlString = [[NSString alloc] initWithFormat:@"%@/%@", [PAAdServerAPI baseUrl], kPA_AdServer_InstallTrackURL];
    PAHttpRequest *request = [[PAHttpJSonRequest alloc] initWithURL:urlString];
    [self queueRequest:request
          finishAction:@selector(installTrackingFinishedAction:)
            failAction:@selector(installTrackingFailAction:withError:)];
    [request release];
    [urlString release];
}

- (void)installTrackingFinishedAction:(PAHttpRequest *)request
{
    PAHttpJSonRequest *jsonRequest = (PAHttpJSonRequest *)request;
    PALogDebug(@"Install tracking successfull with response : %@ ", jsonRequest.responseJson);
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:kPAInstallTrack];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)installTrackingFailAction:(PAHttpRequest *)request withError:(NSError *)error
{
    PALogDebug(@"Install tracking failed with error : %@", [error description]);
}

- (void)trackImpression
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithCapacity:10];
    
    [params safeSetObject:@"imp" forKey:@"type"];
    [params safeSetObject:[self.currentAdView networkId] forKey:kPAAdRequestParamAdNetwork];
    if (self.currentAdView.requestId != nil)
    {
        [params safeSetObject:[self.currentAdView requestId] forKey:kPAAdRequestParamRequestId];
    }
    
    NSString* category = self.currentAdView.bannerData.category;
    if (category != nil)
    {
        [params safeSetObject:category forKey:kPAAdRequestParamCategory];
    }
    
    PALogDebug(@"Track impression with params: %@", [params description]);
    
    NSString *urlString = [[NSString alloc] initWithFormat:@"%@/%@", [PAAdServerAPI baseUrl], kPA_AdServer_TrackURL];
    PAHttpRequest *request = [[PAHttpRequest alloc] initWithURL:urlString andParams:params];
    [self queueRequest:request
          finishAction:@selector(trackImpressionRequestFinished:)
            failAction:@selector(trackImpressionRequestFailed:withError:)];
    [request release];
    [urlString release];
    [params release];
}

- (void)trackImpressionRequestFinished:(PAHttpRequest *)request
{
    PALogDebug(@"Track impression succeed (took %f sec)", request.duration);
#if PA_AD_STATISTICS
    [[PAAdStatistics sharedInstance] addImpression];
#endif
}

- (void)trackImpressionRequestFailed:(PAHttpRequest *)request withError:(NSError *)error
{
    PALogDebug(@"Track impression failed: %@", [error localizedDescription]);
#if PA_AD_STATISTICS
    [[PAAdStatistics sharedInstance] addImpressionFailed];
#endif
    PALogError(@"Error, while registering an PlacePlay ads impression: %@", error.localizedDescription);
}

- (void)trackClick:(BOOL)isPrimary
{
    if (!isPrimary && !self.currentAdView.needSecondaryClick)
    {
        PALogDebug(@"Current ad view doesn't need secondary clicks");
        return;
    }
    
    int secondaryFlagCode;
    SEL finishAction;
    SEL failedAction;
    
    if (isPrimary)
    {
        secondaryFlagCode = 0;
        finishAction = @selector(trackClickRequestFinished:);
        failedAction = @selector(trackClickRequestFailed:withError:);
    }
    else
    {
        secondaryFlagCode = 1;
        finishAction = @selector(trackSecondaryClickRequestFinished:);
        failedAction = @selector(trackSecondaryClickRequestFailed:withError:);
    }
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithCapacity:10];
    [params setIntValue:secondaryFlagCode forKey:kPAAdRequestParamSecondaryClick];
    [params safeSetObject:@"click" forKey:@"type"];
    [params safeSetObject:[self.currentAdView networkId] forKey:kPAAdRequestParamAdNetwork];
    if (self.currentAdView.requestId != nil)
    {
        [params safeSetObject:[self.currentAdView requestId] forKey:kPAAdRequestParamRequestId];
    }
    
    NSString* category = self.currentAdView.bannerData.category;
    if (category != nil)
    {
        [params safeSetObject:category forKey:kPAAdRequestParamCategory];
    }
    
    PALogDebug(@"Track click with params: %@", [params description]);
    
    NSString *urlString = [[NSString alloc] initWithFormat:@"%@/%@", [PAAdServerAPI baseUrl], kPA_AdServer_TrackURL];
    PAHttpRequest *request = [[PAHttpRequest alloc] initWithURL:urlString andParams:params];
    [self queueRequest:request
          finishAction:finishAction
            failAction:failedAction];
    [request release];
    [urlString release];
    [params release];
}

- (void)trackClickRequestFinished:(PAHttpRequest *)request
{
    PALogDebug(@"Track click succeed (took %f sec)", request.duration);
#if PA_AD_STATISTICS
    [[PAAdStatistics sharedInstance] addClick];
#endif
}

- (void)trackClickRequestFailed:(PAHttpRequest *)request withError:(NSError *)error
{
    PALogDebug(@"Track click failed: %@", [error localizedDescription]);
#if PA_AD_STATISTICS
    [[PAAdStatistics sharedInstance] addClickFailed];
#endif
}

- (void)trackSecondaryClickRequestFinished:(PAHttpRequest *)request
{
    PALogDebug(@"Track secondary click succeed (took %f sec)", request.duration);
}

- (void)trackSecondaryClickRequestFailed:(PAHttpRequest *)request withError:(NSError *)error
{
    PALogDebug(@"Track secondary click failed: %@", [error localizedDescription]);
}

// ------------------------------------------------------------------------------------------------------------------------------------------

- (NSUInteger)adRetriesCount
{
    return adTimeoutData.count;
}

- (NSTimeInterval)adRetryTimeoutForRetryIndex:(NSUInteger)attemptIndex
{
    PAAssertMsgv(attemptIndex >= 0 && attemptIndex < [self adRetriesCount], @"Bad attempt index: %d<%d", attemptIndex, [self adRetriesCount]);
    if (attemptIndex >= [self adRetriesCount])
    {
        return 5.0;
    }
    
    return [[adTimeoutData objectAtIndex:attemptIndex] floatValue];
}

// ------------------------------------------------------------------------------------------------------------------------------------------

- (void)notifyAdReceive
{
    if ([self.delegate respondsToSelector:@selector(didReceivedAd:)]) {
        [self.delegate didReceivedAd:self];
    }
}

- (void)notifyAdFailure:(NSError *)error
{
    PALogError(@"PlacePlay Ads failed");
    if ([self shouldNotifyTotalFailureIfError:error])
    {
        PALogError(@"PlacePlayAds received \"no content\" response. Notify about the total failure");
        [self notifyAdTotalFailure];
        return;
    }
    
    if (adRequestRetriesCount == [self adRetriesCount])
    {
        PALogError(@"PlacePlayAds exceeded retries limit: %d. Notify delegate...", [self adRetriesCount]);
        adRequestRetriesCount = 0;
        [self notifyAdTotalFailure];
    }
    else
    {
        adRequestRetriesCount++;
        PALogError(@"PlacPlayAds retry(%d/%d)", adRequestRetriesCount, [self adRetriesCount]);
        [self scheduleAdRequestRetry:[self shouldImmediateRetryIfError:error]];
    }
    
}

- (void)notifyAdTotalFailure
{
    PALogDebug(@"PlacePlayAds total failure!");
    
    if ([self.delegate respondsToSelector:@selector(didFailToReceiveAd:)]) {
        [self.delegate didFailToReceiveAd:self];
    }
}

- (void)notifyAdFinish
{
    [self rollover];
    
    if ([self.delegate respondsToSelector:@selector(didFinishShowingAd:)])
        [self.delegate didFinishShowingAd:self];
}

- (void)notifyFullscreenModalView
{
    [self setModalViewShowing:YES];
    [self.delegate willPresentFullScreenModal];
}

- (void)notifyFullscreenModalViewDismiss
{
    [self setModalViewShowing:NO];
    [self.delegate didDismissFullScreenModal];
}

- (BOOL)shouldNotifyTotalFailureIfError:(NSError *)error
{
    if ([error isKindOfClass:[PAHttpRequestError class]])
    {
        PAHttpRequestError *requestError = (PAHttpRequestError *)error;
        return requestError.responseCode == 204;
    }
    return NO;
}

- (BOOL)shouldImmediateRetryIfError:(NSError *)error
{
    // TODO: move all NSURLErrorDomain's errors to PAHttpRequestErrorDomain
    if ([error.domain isEqualToString:@"NSURLErrorDomain"])
    {
        return NO;
    }
    else if ([error isKindOfClass:[PAHttpRequestError class]])
    {
        return NO;
    }
    
    return YES;
}

// ------------------------------------------------------------------------------------------------------------------------------------------

- (void)queueRequest:(PAHttpRequest *)request finishAction:(SEL)finishAction failAction:(SEL)failAction
{
    [[PAAdServerAPI sharedInstance] queueRequest:request target:self finishAction:finishAction failAction:failAction];
}

- (void)setupInitParams:(PAAdJsonHelper *)json
{
    [self initBannerCache:[json intForKey:kPAAdInitResponseAdCacheSize def:kPAAdDefaultCacheSize]];
    [self initRetryDataWithPattern:[json stringForKey:kPAAdInitResponseAdRetryTimeout def:kPAAdDefaultRetryPattern]];
    
    PAAdJsonHelper *locationJson = nil;
    if ([json contains:kPAAdInitResponseLocationExpiry]) {
        locationExpiryInterval = [json floatForKey:kPAAdInitResponseLocationExpiry def:kPAAdDefaultLocManagerUpdateInterval];
    } else {
        PALogDebug(@"Location expiry interval not available in init response");
        locationExpiryInterval = kPAAdDefaultLocManagerUpdateInterval;
    }
    PALogDebug(@"Location expiry interval is %f seconds. default is = %f", locationExpiryInterval, kPAAdDefaultLocManagerUpdateInterval);
    if (_locationManager != nil) {
        [_locationManager setExpiryInterval:locationExpiryInterval];
    }
    if ([json jsonForKey:kPAAdInitResponseLocationInfo jsonObj:&locationJson])
    {
        [self initLocationInfo:locationJson andExpiryInterval:locationExpiryInterval];
        [locationJson release];
    }
}

- (void)setupInitDefaults
{
    [self initBannerCache:kPAAdDefaultCacheSize];
    [self initRetryDataWithPattern:kPAAdDefaultRetryPattern];
    [self initLocationInfoFromSaved];
}

- (void)initBannerCache:(NSUInteger)capacity
{
    PALogInfo(@"Init banner cache: %d", capacity);
    PABannerCache *cache = [[PABannerCache alloc] initWithDelegate:self andCapacity:capacity];
    self.bannerCache = cache;
    [cache release];
}

- (void)initRetryDataWithPattern:(NSString *)patternString
{
    PALogInfo(@"Init retry timeout: %@", patternString);
    
    NSArray *tokens = [patternString componentsSeparatedByString:@" "];
    NSMutableArray *timeoutData = [[NSMutableArray alloc] initWithCapacity:tokens.count];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en-US"];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    formatter.locale = locale;
    [locale release];
    
    for (NSString *token in tokens)
    {
        NSNumber *timeoutNum = [formatter numberFromString:token];
        PAAssertMsgv(timeoutNum != nil, @"Unable to parse decimal token '%@'", token);
        if (timeoutNum != nil)
        {
            [timeoutData addObject:timeoutNum];
        }
        else
        {
            NSNumber *defaultTimeoutNum = [[NSNumber alloc] initWithFloat:5.0];
            [timeoutData addObject:defaultTimeoutNum];
            [defaultTimeoutNum release];
        }
    }
    [formatter release];
    
    self.adTimeoutData = timeoutData;
    [timeoutData release];
}

- (void)initLocationInfo:(PAAdJsonHelper *)json andExpiryInterval:(NSTimeInterval)expiryInterval
{
    PALocationInfo *info = [[PALocationInfo alloc] initWithJson:json andExpiryInterval:expiryInterval];
    self.locationInfo = info;
    
    [PASerialization saveObject:info forKey:kPAAdKeyFallbackLocationInfo];
    
    [info release];
}

- (void)initLocationInfoFromSaved
{
    PALocationInfo* info = [PASerialization loadObjectForKey:kPAAdKeyFallbackLocationInfo];
    if (info)
    {
        self.locationInfo = info;
        PALogInfo(@"Use saved location info");
    }
    else
    {
        self.locationInfo = nil;
        PALogWarn(@"No saved location info");
    }
}

- (BOOL)checkValidityOfLocation:(CLLocation *)aLocation withExpiryInterval:(NSTimeInterval)expiry
{
    NSDate *saveDate = [aLocation timestamp];
    NSTimeInterval diff = [saveDate timeIntervalSinceNow];
    if ((diff < 0.0) && ((expiry + diff) > 0.0)) {
        PALogDebug(@"Location is valid with diff = %f", diff);
        return TRUE;
    }
    PALogDebug(@"Location is invalid");
    return FALSE;
}

- (void)requestInit
{
    [[PAAdServerAPI sharedInstance] cancelAllRequests];
    [self initiateInitRequest];
}

- (BOOL)canRequestAd
{
    return [self isActive] && ![self isModalViewShowing];
}

- (void)initiateInitRequest
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params safeSetObject:kPA_AdServer_Publisher forKey:kPAAdRequestParamPublisher];
    
    PALogDebug(@"Request init with params (timeout %f sec): %@", kPAAdDefaultInitRequestTimeout, [params description]);
    
    NSString *urlString = [[NSString alloc] initWithFormat:@"%@/%@", [PAAdServerAPI baseUrl], kPA_AdServer_InitURL];
    PAHttpRequest *request = [[PAHttpJSonRequest alloc] initWithURL:urlString andTimeout:kPAAdDefaultInitRequestTimeout andParams:params];
    [self queueRequest:request
          finishAction:@selector(initRequestFinished:)
            failAction:@selector(initRequestFailed:withError:)];
    [request release];
    [params release];
    [urlString release];
}

- (void)initRequestFinished:(PAHttpJSonRequest *)request
{
    id responseJson = request.responseJson;
    
    PALogInfo(@"Received init info from the server (took %f sec): %@", request.duration, [responseJson description]);
    PAAdJsonHelper *jsonHelper = [[PAAdJsonHelper alloc] initWithJSonObj:responseJson];
    [self setupInitParams:jsonHelper];
    [jsonHelper release];
    initFinished = YES;
    
    [PAAdHtmlBasedAdView readBannerPageFromFile:@"att"];
    
    [self requestAd];
}

- (void)initRequestFailed:(PAHttpRequest *)request withError :(NSError *)error
{
    PALogError(@"Unable to get initialization info: %@", error);
    [self setupInitDefaults];
    
    initFinished = YES;
    
    [PAAdHtmlBasedAdView readBannerPageFromFile:@"att"];
    
    [self requestAd];
}

// ------------------------------------------------------------------------------------------------------------------------------------------

- (void)scheduleAdRequestRetry:(BOOL)immediate
{
    if (immediate && [self canRequestAd])
    {
        [self initiateAdRequest];
    }
    else
    {
        NSTimeInterval retryTimeout = [self adRetryTimeoutForRetryIndex:adRequestRetriesCount-1];
        PALogDebug(@"Retry ad request in %f sec", retryTimeout);
        scheduleTimer(&retryAdRequestTimer, retryTimeout, self, @selector(onAdRequestRetryTimer:), nil, YES);
    }
}

- (void)cancelAdRequestRetryTimer
{
    invalidateTimer(&retryAdRequestTimer);
    retryAdRequestTimer = nil;
}

- (void)onAdRequestRetryTimer:(NSTimer *)timer
{
    if ([self canRequestAd])
    {
        [self cancelAdRequestRetryTimer];
        [self initiateAdRequest];
    }
    else
    {
        PALogInfo(@"Can't retry ad request: modal view is shown");
    }
}

// ------------------------------------------------------------------------------------------------------------------------------------------

#pragma mark PABannerCacheDelegate

- (void)bannerReceived:(PABannerCache *)cache view:(PAAdCustomAdView *)bannerView
{
    [self setupCustomAdView:bannerView];
}

- (void)bannerFailed:(PABannerCache *)cache withError:(NSError *)error
{
    [self notifyAdFailure:error];
}

- (BOOL)bannerCanBeDisplayedNow
{
    return [self canRequestAd];
}

- (CGRect)bannerFrame
{
    return frameFromBannerSize(bannerSize);
}

#pragma mark PAAdServerAPIDataSource

- (NSString *)appId
{
    return [self.delegate appId];
}

- (NSString *)userId
{
    if ([self.delegate respondsToSelector:@selector(userId)])
    {
        return [self.delegate userId];
    }
    else
    {
        // Get app-specific unique user ID
        return [[UIDevice currentDevice] PAUniqueIdentifier];
    }
}

- (CLLocation *)deviceLocation
{
    CLLocation *location = [self.delegate respondsToSelector:@selector(location)] ? [self.delegate location] : [_locationManager location];
    if (location && [self checkValidityOfLocation:location withExpiryInterval:locationExpiryInterval])
    {
        if (location.coordinate.latitude != self.location.coordinate.latitude || location.coordinate.longitude != self.location.coordinate.longitude)
        {
            PALogInfo(@"Update stored location");
            [PASerialization saveObject:location forKey:kPAAdKeyFallbackLocation];
            self.location = location;
        }
    }
    else if (self.location && [self checkValidityOfLocation:self.location withExpiryInterval:locationExpiryInterval])
    {
        location = self.location;
        PALogWarn(@"Use stored location");
    } else {
        PALogWarn(@"Device location and fallback location both invalid");
        location = nil;
    }
    
    return location;
}

- (NSString *)sdkVersion
{
    return kPAAdSdkVersion;
}

- (NSString *)protocolVersion
{
    return KPAAdProtocolVersion;
}

- (NSString *)bannerSizeString
{
    switch (bannerSize)
    {
        case PAADViewBannerSizeUndefined:
            return nil;
            
        case PAADViewBannerSize320x50:
            return @"320x50";
            
        case PAADViewBannerSize300x50:
            return @"300x50";
            
        case PAADViewBannerSize768x90:
            return @"768x90";
    }
    
    PAAssertMsgv(false, @"Unknown banner size: %d", bannerSize);
    return @"320x50";
}

- (NSDictionary *)targetingParams
{
    if ([self.delegate respondsToSelector:@selector(targetingParams)])
    {
        return [self.delegate targetingParams];
    }
    return nil;
}

- (NSString *)pluginPackageParam
{
    if ([self.delegate respondsToSelector:@selector(pluginPackageParam)])
    {
        id delegateObj = self.delegate; // eleminate compiler's warning
        return [delegateObj pluginPackageParam];
    }
    
    return nil;
}

- (NSDictionary *)testingParams
{
    if ([self.delegate respondsToSelector:@selector(testingParams)])
    {
        return [self.delegate testingParams];
    }
    return nil;
}

#pragma mark PAAdCustomAdViewDelegate methods

NSString* navTypToString(UIWebViewNavigationType type)
{
    switch (type)
    {
        case UIWebViewNavigationTypeLinkClicked:
            return @"UIWebViewNavigationTypeLinkClicked";
            
        case UIWebViewNavigationTypeReload:
            return @"UIWebViewNavigationTypeReload";
            
        case UIWebViewNavigationTypeFormSubmitted:
            return @"UIWebViewNavigationTypeFormSubmitted";
            
        case UIWebViewNavigationTypeFormResubmitted:
            return @"UIWebViewNavigationTypeFormResubmitted";
            
        case UIWebViewNavigationTypeBackForward:
            return @"UIWebViewNavigationTypeBackForward";
            
        case UIWebViewNavigationTypeOther:
            return @"UIWebViewNavigationTypeOther";
    }
    return nil;
}

- (void)adDidStart:(PAAdCustomAdView *)adView
{
    PALogDebug(@"Ads started");
}

- (void)adTapped:(PAAdCustomAdView *)adView
{
    PALogDebug(@"Ads tapped");
    [self trackClick:YES];
    [self notifyFullscreenModalView];
    [[self bannerCache] disableAdRequests];
}

- (void)adTappedSecondaryClick:(PAAdCustomAdView *)adView
{
    PALogDebug(@"Ads secondary click");
    [self trackClick:NO];
}

- (void)adClosed:(PAAdCustomAdView *)adView
{
    PALogDebug(@"Ads closed");
    [self notifyFullscreenModalViewDismiss];
    [[self bannerCache] enableAdRequests];
}

- (void)adDidFinish:(PAAdCustomAdView *)adView
{
    PALogDebug(@"Ads finished");
    [self notifyAdFinish];
}

- (void)adDidFinish:(PAAdCustomAdView *)adView withError:(NSError *)error
{
    PALogError(@"Ads failed with error: %@", [error localizedDescription]);
    [self notifyAdFailure:error];
}

#pragma mark Helpers

CGRect frameFromBannerSize(PAADViewBannerSize bannerSize)
{
    switch (bannerSize)
    {
        case PAADViewBannerSizeUndefined:
            return CGRectZero;
            
        case PAADViewBannerSize320x50:
            return CGRectMake(0, 0, 320, 50);
            
        case PAADViewBannerSize300x50:
            return CGRectMake(0, 0, 300, 50);
            
        case PAADViewBannerSize768x90:
            return CGRectMake(0, 0, 768, 90);
    }
    
    PAAssertMsgv(false, @"Unknown banner size: %d", bannerSize);
    return CGRectMake(0, 0, 320, 50);
}

@end // @implementation PAAdView
