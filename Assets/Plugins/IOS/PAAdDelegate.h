//
//  PAAdProtocol.h
//  PlacePlayAds
//
//  Created by Alex Koloskov on 9/21/11.
//  Copyright 2011 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CoreLocation.h>

typedef enum {
    PAADViewBannerSizeUndefined = 0,
    PAADViewBannerSize320x50 = 1,
    PAADViewBannerSize300x50 = 2,
    PAADViewBannerSize768x90 = 3
} PAADViewBannerSize;

@class PAAdView;

/* Targeting info properties  */
extern NSString* const kPAADTargetPropertyKeywords; // Keyword or category list separated by pipe-delimiter
extern NSString* const kPAADTargetPropertyGender;   // The user’s gender
extern NSString* const kPAADTargetPropertyAge;      // The user’s age
extern NSString* const kPAADTargetPropertyMarital;  // The user’s marital status
extern NSString* const kPAADTargetPropertyIncome;   // The user’s household income in USD
extern NSString* const kPAADTargetPropertyEdu;      // The user’s education level
extern NSString* const kPAADTargetPropertyEth;      // The user’s ethnic group

// gender
extern NSString* const kPAADGenderMale;
extern NSString* const kPAADGenderFemale;
// marital state
extern NSString* const kPAADMaritalSingle;
extern NSString* const kPAADMaritalMarried;
// The user’s household income in USD
extern NSString* const kPAADIncome15000;            // less than 15,000
extern NSString* const kPAADIncome15000_19999;      // 15,000-19,999
extern NSString* const kPAADIncome20000_29999;      // 20,000-29,999
extern NSString* const kPAADIncome30000_39999;      // 30,000-39,999
extern NSString* const kPAADIncome40000_49999;      // 40,000-49,999
extern NSString* const kPAADIncome50000_74999;      // 50,000-74,999
extern NSString* const kPAADIncome75000_99999;      // 75,000-99,999
extern NSString* const kPAADIncome100000_124999;    // 100,000-124,999
extern NSString* const kPAADIncome125000_149999;    // 125,000-149,999
extern NSString* const kPAADIncome150000;           // over 150,000
// education
extern NSString* const kPAADEduHighSchool;          // high school
extern NSString* const kPAADEduCollege;             // some college/associate
extern NSString* const kPAADEduBachelor;            // bachelor’s
extern NSString* const kPAADEduGraduate;            // graduate
// ethnic group
extern NSString* const kPAADEthnicAfricanAmerican;
extern NSString* const kPAADEthnicAsianAmerican;
extern NSString* const kPAADEthnicMediterranean;
extern NSString* const kPAADEthnicNativeAmerican;
extern NSString* const kPAADEthnicScandinavian;
extern NSString* const kPAADEthnicPolynesian;
extern NSString* const kPAADEthnicMiddleEastern;
extern NSString* const kPAADEthnicJewish;
extern NSString* const kPAADEthnicWesternEuropean;
extern NSString* const kPAADEthnicEasternEuropean;
extern NSString* const kPAADEthnicMiscellaneousOther;
extern NSString* const kPAADEthnicOceania;
extern NSString* const kPAADEthnicHispanic;

@protocol PAAdDelegate <NSObject>

@required

- (NSString*) appId;

/**
 * The view controller with which the ad network will display a modal view
 * (web view, canvas), such as when the user clicks on the ad. You must
 * supply a view controller. You should return the root view controller
 * of your application, such as the root UINavigationController, or
 * any controllers that are pushed/added directly to the root view controller.
 * For example, if your app delegate has a pointer to the root view controller:
 *
 * return [(MyAppDelegate*)[[UIApplication sharedApplication] delegate] rootViewController]
 *
 * will suffice.
 */
- (UIViewController*) viewControllerForPresentingModalView;

@optional

- (BOOL)paLocationServicesEnabled;
- (CLLocation*)     location;
- (NSString*)       userId;
- (PAADViewBannerSize)bannerSize;

#pragma mark Notifications

- (void) didReceivedAd:(PAAdView *) adView;
- (void) didFinishShowingAd:(PAAdView *)adView;
- (void) didFailToReceiveAd:(PAAdView *)adView;
- (void) didFailedIncompatibility;

/**
 * These notifications will let you know when a user is being shown a full screen
 * webview canvas with an ad because they tapped on an ad.  You should listen to
 * these notifications to determine when to pause/resume your game--if you're
 * building a game app.
 */
- (void) willPresentFullScreenModal;
- (void) didDismissFullScreenModal;

/**
 * Called when a user action will result in App switching.
 * This may occur if user taps AppStore link or dial-number button.
 */
- (void) willLeaveApplication:(PAAdView *)adView;

- (void) didReceiveConfig;

- (BOOL) viewActionShouldBegin:(PAAdView*) banner willLeaveApplication: (BOOL) willLeave;

#pragma mark Targeting info
/**
 * The following parameters can be sent with the ad request in order to allow a more precise 
 * targeting of ads. Even though all of these parameters are optional 
 * it is strongly recommended to fill as many of them as possible.
 *
 * Example:
 *    - (NSDictionary *)targetingParams
 *    {
 *        return [NSDictionary dictionaryWithObjectsAndKeys:
 *            @"food|dining", kPAADTargetPropertyKeywords,
 *            kPAADGenderMale, kPAADTargetPropertyGender,
 *            @"25", kPAADTargetPropertyAge,
 *            kPAADMaritalMarried, kPAADTargetPropertyMarital,
 *            kPAADIncome30000_39999, kPAADTargetPropertyIncome,
 *            kPAADEduBachelor, kPAADTargetPropertyEdu,
 *            kPAADEthnicEasternEuropean, kPAADTargetPropertyEth,
 *            nil];
 *    } 
 */
- (NSDictionary *)targetingParams;

#pragma mark Testing location info

- (NSDictionary *)testingParams;

@end
