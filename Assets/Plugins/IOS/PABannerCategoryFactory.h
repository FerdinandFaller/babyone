//
//  PABannerCategoryFactory.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 3/25/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PABannerCategoryExtractor.h"

@interface PABannerCategoryFactory : NSObject

+ (void)tryParseCategoryExtractorFromJson:(PAAdJsonHelper *)json pointer:(PABannerCategoryExtractor **)pExtractor;

@end
