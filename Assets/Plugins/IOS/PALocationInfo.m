//
//  PALocationInfo.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 4/16/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PALocationInfo.h"
#import "PAAdDateFormatter.h"
#import "PADebug.h"

#define kParamCity          @"city"
#define kParamCountry       @"country"
#define kParamLatitude      @"latitude"
#define kParamLongitude     @"longitude"
#define kParamRegion        @"region"
#define kParamPostal        @"postal"
#define kParamTimestamp     @"geo_timestamp"
#define kParamLocExpInt     @"location_expiry_interval"

@implementation PALocationInfo

@synthesize city, country, latitude, longitude, region, postal, timestamp, expiry;

- (id)initWithJson:(PAAdJsonHelper *)json andExpiryInterval:(NSTimeInterval)expiryInterval
{
    self = [super init];
    if (self)
    {
        self.city = [json stringForKey:kParamCity];
        self.country = [json stringForKey:kParamCountry];
        self.latitude = [json floatForKey:kParamLatitude];
        self.longitude = [json floatForKey:kParamLongitude];
        self.region = [json stringForKey:kParamRegion];
        self.postal = [json stringForKey:kParamPostal];
        self.timestamp = [PAAdDateFormatter getTimestampInRFC3339FormatForDate:[NSDate date]];
        self.expiry = expiryInterval;
    }
    return self;
}

- (void)dealloc
{
    self.city = nil;
    self.country = nil;
    self.region = nil;
    self.postal = nil;
    self.timestamp = nil;
    
    [super dealloc];
}

- (BOOL)isValid
{
    NSDate *saveDate = [PAAdDateFormatter dateForTimestampInRFC3339Format:timestamp];
    if (saveDate != nil) {
        NSTimeInterval diff = [saveDate timeIntervalSinceNow];
        if ((diff < 0.0) && ((expiry + diff) > 0.0)) {
            PALogDebug(@"PALocationInfo is valid with diff = %f", diff);
            return TRUE;
        }
    }
    
    PALogDebug(@"PALocationInfo is invalid");
    return FALSE;
}

#pragma mark -
#pragma mark NSCoder

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.city forKey:kParamCity];
    [aCoder encodeObject:self.country forKey:kParamCountry];
    [aCoder encodeDouble:self.latitude forKey:kParamLatitude];
    [aCoder encodeDouble:self.longitude forKey:kParamLongitude];
    [aCoder encodeObject:self.region forKey:kParamRegion];
    [aCoder encodeObject:self.postal forKey:kParamPostal];
    [aCoder encodeObject:self.timestamp forKey:kParamTimestamp];
    [aCoder encodeInt:self.expiry forKey:kParamLocExpInt];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self)
    {
        self.city = [aDecoder decodeObjectForKey:kParamCity];
        self.country = [aDecoder decodeObjectForKey:kParamCountry];
        self.latitude = [aDecoder decodeDoubleForKey:kParamLatitude];
        self.longitude = [aDecoder decodeDoubleForKey:kParamLongitude];
        self.region = [aDecoder decodeObjectForKey:kParamRegion];
        self.postal = [aDecoder decodeObjectForKey:kParamPostal];
        self.timestamp = [aDecoder decodeObjectForKey:kParamTimestamp];
        self.expiry = [aDecoder decodeIntForKey:kParamLocExpInt];
    }
    return self;
}


@end
