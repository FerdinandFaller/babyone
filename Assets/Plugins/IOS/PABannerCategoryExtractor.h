//
//  PABannerCategoryExtractor.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 3/25/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PAAdJsonHelper.h"

@interface PABannerCategoryExtractor : NSObject

- (id)initWithJson:(PAAdJsonHelper *)json;
- (NSString *)tryExtractCategoryFromJSon:(PAAdJsonHelper *)json;

@end
