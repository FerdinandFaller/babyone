//
//  PABannerCategoryRegExExtractor.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 3/25/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PABannerCategoryExtractor.h"

@interface PABannerCategoryRegExExtractor : PABannerCategoryExtractor
{
@private
    NSString* pattern;
    NSString* capture;
    NSString* key;
}

@property (nonatomic, copy) NSString* pattern;
@property (nonatomic, copy) NSString* capture;
@property (nonatomic, copy) NSString* key;

@end
