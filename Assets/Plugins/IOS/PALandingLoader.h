//
//  PALandingLoader.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 2/8/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PALandingInfo.h"
#import "PAAdWebBrowserController.h"

@class PALandingLoader;

@protocol PALandingLoaderDelegate <NSObject>

- (void)landingInfoReceived:(PALandingLoader *)loader;
- (void)landingInfoFailed:(PALandingLoader *)loader withError:(NSError *)error;

@end

@interface PALandingLoader : NSObject<PAAdWebBrowserBackgroundLoadingDelegate>
{
@private
    PALandingInfo *landingInfo;
    id<PALandingLoaderDelegate> delegate;
    PAAdWebBrowserController *browser;
}

@property (nonatomic, readonly) PALandingInfo *landingInfo;
@property (nonatomic, readonly) PAAdWebBrowserController *browser;
@property (nonatomic, assign) id<PALandingLoaderDelegate> delegate;

- (id)initWithInfo:(PALandingInfo *)landingInfo andBrowser:(PAAdWebBrowserController *)browser;
- (void)load;

@end
