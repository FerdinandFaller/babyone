//
//  StopWatch.h
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 21.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StopWatch : NSObject
{
    NSDate *startDate;
    NSTimeInterval duration;
}

@property (nonatomic, retain) NSDate *startDate;
@property (nonatomic, assign) NSTimeInterval duration;

- (void)start;
- (void)stop;

@end
