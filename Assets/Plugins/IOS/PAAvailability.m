//
//
//  PAAvailability.h
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 4/21/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//
#import "PAAvailability.h"
#import "PADebug.h"

static PASystemVersion systemVersion;

void PAAvailabilityDetectSystemVersion(void)
{	
	NSArray *tokens = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
	int multiplier = 10000;
	
	systemVersion = 0;	
	for (int i = 0; i < tokens.count; ++i)
	{
		int val = [[tokens objectAtIndex:i] intValue];
		systemVersion += val * multiplier;
		multiplier /= 100;
	}
	
	PALogInfo(@"System version: %d", systemVersion);
}

BOOL PAAvailabilitySystemVersionAvailable(PASystemVersion version)
{
	return systemVersion >= version;
}

