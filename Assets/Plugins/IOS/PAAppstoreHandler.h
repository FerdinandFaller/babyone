//
//  PAAppStoreHandler.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 4/17/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PAAppStoreHandler;

@protocol PAAppStoreHandlerDelegate <NSObject>

@optional
- (void)appStoreWillOpen:(PAAppStoreHandler *)handler;
- (void)appStoreDidClose:(PAAppStoreHandler *)handler;
- (void)appStoreDidFail:(PAAppStoreHandler *)handler;

@end

@interface PAAppStoreHandler : NSObject
{
@private
    BOOL appstoreRequested;
    id delegate;
}

- (BOOL)tryAppStoreHandlerForURL:(NSURL *)url delegate:(id)delegate;
- (BOOL)tryOpenAppStoreURL:(NSURL *)url;

+ (PAAppStoreHandler *)sharedInstance;

@end
