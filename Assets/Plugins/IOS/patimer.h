//
//  PAAdTimerUtils.h
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 24.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

void scheduleTimer(NSTimer** timer, NSTimeInterval interval, id target, SEL selector, id userInfo, BOOL repeats);
void invalidateTimer(NSTimer** timer);