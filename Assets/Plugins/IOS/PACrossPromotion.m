//
//  PACrossPromotion.m
//  PlacePlayAdsDebugSample
//
//  Created by Soumya Behera on 25/07/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PACrossPromotion.h"
#import "PAHttpRequest.h"
#import "PAAdView.h"

#define kPAInstallTrack @"install_track"

@implementation PACrossPromotion

static PACrossPromotion *instance = nil;

@synthesize appIdString;

+ (id)sharedInstance
{
    if (instance == nil) {
        instance = [[super alloc] init];
    }
    
    return instance;
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self) {
        if (instance == nil) {
            instance = [super allocWithZone:zone];
        }
    }
    
    return instance;
}

- (id)retain
{
    return self;
}

- (oneway void)release
{
    // do nothing
}

- (id)autorelease
{
    return self;
}

- (NSUInteger)retainCount
{
    return NSUIntegerMax;
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

- (void)trackInstallationForAppID:(NSString *)appId
{
    BOOL installTracked = [[NSUserDefaults standardUserDefaults] boolForKey:kPAInstallTrack];
    if (!installTracked) {
        appIdString = [NSString stringWithString:appId];
        PAAdView *adview = [PAAdView sharedAdView:self];
        [adview trackInstall];
    }
}

#pragma mark - 
#pragma mark PAAdDelegate methods

- (NSString *)appId
{
    return self.appIdString;
}

- (UIViewController *)viewControllerForPresentingModalView
{
    return [UIApplication sharedApplication].keyWindow.rootViewController;
}

@end
