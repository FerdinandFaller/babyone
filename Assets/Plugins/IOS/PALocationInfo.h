//
//  PALocationInfo.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 4/16/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#import "PAAdJsonHelper.h"

@interface PALocationInfo : NSObject<NSCoding>
{
@private
    NSString* city;
    NSString* country;
    CLLocationDegrees latitude;
    CLLocationDegrees longitude;
    NSString* region;
    NSString* postal;
    NSString* timestamp;
    NSTimeInterval expiry;
}

@property (nonatomic, copy) NSString* city;
@property (nonatomic, copy) NSString* country;
@property (nonatomic, assign) CLLocationDegrees latitude;
@property (nonatomic, assign) CLLocationDegrees longitude;
@property (nonatomic, copy) NSString* region;
@property (nonatomic, copy) NSString* postal;
@property (nonatomic, copy) NSString* timestamp;
@property (nonatomic, assign) NSTimeInterval expiry;

- (id)initWithJson:(PAAdJsonHelper *)json andExpiryInterval:(NSTimeInterval)expiryInterval;

- (BOOL)isValid;

@end
