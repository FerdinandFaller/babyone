//
//  PAAdButtonBasedAdView.m
//  PlacePlayAds Sample
//
//  Created by Alex Lementuev on 16.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import "PAAdButtonBasedAdView.h"

#import "PAButtonBannerData.h"

#import "PAHttpRequest.h"
#import "PAHttpImageRequest.h"

#import "PAAdWebBrowserController.h"

#if PA_AD_STATISTICS
#import "PAAdStatistics.h"
#endif

#import "PADebug.h"

#import "patimer.h"

static PAAdWebBrowserController *sharedBrowser;

@interface PAAdButtonBasedAdView (Private)

- (void)buttonTapped:(id)sender;

- (void)startBannerTimer;
- (void)cancelBannerTimer;
- (void)onBannerTimer:(NSTimer *)timer;
- (BOOL)canTickBannerTimer;

@end

@implementation PAAdButtonBasedAdView

@synthesize imageRequest;

- (id)initWithBannerData:(PAButtonBannerData *)data andFrame:(CGRect)frame
{
    self = [super initWithBannerData:data andFrame:frame];
    if (self)
    {
    }
    return self;
}

- (void)dealloc
{
    self.imageRequest = nil;
    [super dealloc];
}

- (void)loadContent
{    
    @synchronized (self)
    {
        [super loadContent];
        
        PAHttpRequest *request = [[PAHttpImageRequest alloc] initWithURL:self.imageUrl];
        
        self.imageRequest = request;
        
        [request setFinishTarget:self andAction:@selector(imageRequestFinished:)];
        [request setErrorTarget:self andAction:@selector(imageRequestFailed:withError:)];
        [request start];    
        [request release];
    }
}

- (void)cancelLoadContent
{
    @synchronized (self)
    {
        [self.imageRequest cancel];
        self.imageRequest = nil;
    }
}

- (void)imageRequestFinished:(PAHttpImageRequest *)request
{
    PALogDebug(@"Received image (took %f sec)", request.duration);
    
    UIImage *image = request.responseImage;
    
    CGSize imageSize = image.size;
    CGSize bannerSize = self.bounds.size;
    CGRect frame = self.bounds;
    
    if (!CGSizeEqualToSize(imageSize, bannerSize))
    {
        float scale = MIN(bannerSize.width / imageSize.width, bannerSize.height / imageSize.height);
        if (scale > 1)
        {
            CGFloat width = imageSize.width * scale;
            CGFloat height = imageSize.height * scale;
            
            frame = CGRectMake(0.5 * (frame.size.width - width), 0.5 * (frame.size.height - height), width, height);
        }
    }       
    
    UIButton *bannerButton = [UIButton buttonWithType:UIButtonTypeCustom];        
    [bannerButton setFrame:frame];
    [bannerButton addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchDown];
    [bannerButton setBackgroundImage:image forState:UIControlStateNormal];        
    
    [self addSubview:bannerButton];

    [self contentLoaded];
}

- (void)imageRequestFailed:(PAHttpRequest *)request withError:(NSError *)error
{
    PALogError(@"Unable to get image (%@): %@", request.urlString, [error localizedDescription]);
    [self contentLoadingFailedWithError:error];
    [self notifyAdFailed:error];    
}

- (void)buttonTapped:(id)sender
{
    NSURL *url = [[NSURL alloc] initWithString:self.clickUrl];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    [url release];
    
    [self showBrowserWithRequest:request];
    [request release];
    
    [self notifyAdTapped];
}

#pragma mark - Stop

- (void)stop
{
    [super stop];
    [self cancelBannerTimer];    
}

#pragma mark - Notify routine

- (void)notifyAdStarted
{
    [super notifyAdStarted];
    [self startBannerTimer]; // start banner lifetime "ticks"
}

- (void)notifyAdTapped
{
    fullscreenAdShowed = YES;
    [super notifyAdTapped];
}

- (void)notifyAdClosed
{
    fullscreenAdShowed = NO;
    [super notifyAdClosed];
}

- (void)notifyAdFailed:(NSError *)error
{
    [self cancelBannerTimer];    
    [super notifyAdFailed:error];    
}

- (void)notifyAdFinished
{
    [self cancelBannerTimer];
    [super notifyAdFinished];    
}

#pragma mark Sub Banner timer

- (void)startBannerTimer
{
    PALogDebug(@"button based started timer: %f", bannerData.timeout);
    scheduleTimer(&bannerTimer, bannerData.timeout, self, @selector(onBannerTimer:), nil, YES);
}

- (void)cancelBannerTimer
{
    @synchronized (self)
    {
        PALogDebug(@"button based canceled banner timer");
        invalidateTimer(&bannerTimer);
    }  
}

- (void)onBannerTimer:(NSTimer *)timer
{
    if (![self canTickBannerTimer])
    {
        PALogDebug(@"button based banner timer tick ignored");
        return;
    }
    
    PALogDebug(@"button based banner timer finished");
    [self cancelBannerTimer];
    [self notifyAdFinished];
}

- (BOOL)canTickBannerTimer
{
    return !fullscreenAdShowed;
}

#pragma mark Shared browser

- (PAAdWebBrowserController *)sharedBrowser
{
    @synchronized ([PAAdButtonBasedAdView class])
    {
        if (sharedBrowser == nil)
        {            
            sharedBrowser = [[PAAdWebBrowserController alloc] init];
            PALogInfo(@"Button based banner created shared browser instance");
        }
        return sharedBrowser;
    }
}

#pragma mark PAAdWebBrowserControllerDelegate methods

- (void)adWebBrowserPageDidStartLoad:(PAAdWebBrowserController *)controller
{
#if PA_AD_STATISTICS    
    [[PAAdStatistics sharedInstance].currentRequestInfo.pageStopWatch start];
#endif
}

- (void)adWebBrowserPageDidFinishLoad:(PAAdWebBrowserController *)controller
{
#if PA_AD_STATISTICS
    [[PAAdStatistics sharedInstance].currentRequestInfo.pageStopWatch stop];
    [[PAAdStatistics sharedInstance].currentRequestInfo setSucceed];
#endif
}

- (void)adWebBrowserPageDidFinishWithError:(PAAdWebBrowserController *)controller error:(NSError *)error
{
#if PA_AD_STATISTICS
    [[PAAdStatistics sharedInstance].currentRequestInfo.pageStopWatch stop];
    [[PAAdStatistics sharedInstance].currentRequestInfo setPageFailed];
#endif
}

#pragma mark Properties

- (PAButtonBannerData *)buttonBannerData
{
    return (PAButtonBannerData *)bannerData;
}

- (NSString *)imageUrl
{
    return [self buttonBannerData].imageUrl;
}

- (NSString *)clickUrl
{
    return [self buttonBannerData].clickUrl;
}

@end
