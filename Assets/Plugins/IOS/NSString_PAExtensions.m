//
//  NSString+PAExtensions.m
//  PlacePlayAds
//
//  Created by Alex Koloskov on 9/21/11.
//  Copyright 2011 PressOK Entertainment. All rights reserved.
//

#import "NSString_PAExtensions.h"
#import <CommonCrypto/CommonDigest.h>

/** Adobe's windows port of the iOS tool chain won't link a category
 * unless it contains at least one non-category implementation in the
 * source file. */
@interface HACKNSSPACategories
@end

@implementation HACKNSSPACategories
@end


@implementation NSString (PAExtensions)

// method to calculate a standard md5 checksum of the string, check against: http://www.adamek.biz/md5-generator.php
- (NSString*) md5
{
	const char* cString = [self UTF8String];
	unsigned char result [CC_MD5_DIGEST_LENGTH];
	CC_MD5( cString, strlen(cString), result );
	
	return [NSString 
			stringWithFormat: @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
			result[0], result[1],
			result[2], result[3],
			result[4], result[5],
			result[6], result[7],
			result[8], result[9],
			result[10], result[11],
			result[12], result[13],
			result[14], result[15]
			];
}

@end
