//
//  PALandingLoader.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 2/8/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "NSMutableDictionary_Types.h"

#import "PALandingLoader.h"

#import "PABannerData.h"
#import "PAAdServerAPI.h"

#import "PADebug.h"
#import "PADataFile.h"

#define kPAAdResponseParamLandingInfo           @"landing_info"

#define kParamLandingPage                       @"page"
#define kParamLandingPageType                   @"type"
#define kParamLandingPageData                   @"data"

#define kLandingPageTypeURL                     @"url"
#define kLandingPageTypeHTML                    @"html"
#define kLandingPageTypeTemplate                @"template"

#define kLandingSerializeFormatVersion          1

#define kPropertyKeyFormatVersion               @"format_version"
#define kPropertyKeyVersion                     @"version"
#define kPropertyKeyType                        @"type"
#define kPropertyKeyData                        @"data"

@interface PALandingLoader (Private)

- (BOOL)tryWriteToFile:(PALandingInfo *)info;
- (NSData *)trySerializeToData:(PALandingInfo *)info;

- (BOOL)tryReadFromFile:(PALandingInfo *)info;
- (BOOL)tryDeserializeFromData:(NSData *)data landingInfo:(PALandingInfo *)info;

- (NSString *)filenameFor:(PALandingInfo *)info;

- (void)initiateAdLandingInfoRequest;
- (void)adLandingInfoRequestFinished:(PAHttpJSonRequest *)request;
- (void)adLandingInfoRequestFailed:(PAHttpJSonRequest *)request withError:(NSError *)error;

- (void)parseLandingInfo:(PAAdJsonHelper *)json;
- (BOOL)isLandingTypeValid:(NSString *)type;

- (void)loadLandingInfoIntoTheBrowser:(PALandingInfo *)info;

@end

@implementation PALandingLoader

@synthesize landingInfo;

@synthesize delegate;
@synthesize browser;

- (id)initWithInfo:(PALandingInfo *)aLandingInfo andBrowser:(PAAdWebBrowserController *)aBrowser
{
    self = [super init];
    if (self)
    {
        landingInfo = aLandingInfo;
        browser = aBrowser;
    }
    return self;
}

- (void)dealloc
{
    self.delegate = nil;
    [super dealloc];
}

- (void)load
{
    PAAssert([landingInfo isUndefined]);
    
    NSInteger expectedVersion = landingInfo.version;
    if ([self tryReadFromFile:landingInfo])
    {
        if (expectedVersion == landingInfo.version)
        {
            PALogDebug(@"Landing data '%@' on disk appeared to be up to date. Use it!", landingInfo.networkId);
            [self loadLandingInfoIntoTheBrowser:landingInfo];
        }
        else
        {
            PALogDebug(@"Landing data '%@' expected version %d but was %d", landingInfo.networkId, expectedVersion, landingInfo.version);
            [landingInfo clear];
            [self initiateAdLandingInfoRequest];
        }
    }
    else
    {   
        PALogDebug(@"Unable to read data '%@' from disk. Requesting it from the server...", landingInfo.networkId);
        [self initiateAdLandingInfoRequest];
    }
}

#pragma mark File storage

- (BOOL)tryWriteToFile:(PALandingInfo *)info
{
    NSData *data = [self trySerializeToData:info];
    if (data == nil)
    {
        return NO;
    }
    
    NSString *filename = [self filenameFor:info];
    PADataFile *file = [[PADataFile alloc] initWithName:filename];
    if ([file exists]) {
        PALogDebug(@"Landing file %@ exists", filename);
        BOOL deleted  = [file deleteFile];
        
        if (deleted) {
            PALogDebug(@"Landing file %@ deleted successfully", filename);
            [file release];
            file = [[PADataFile alloc] initWithName:filename];
        } else{
            PALogDebug(@"Unable to delete Landing file %@", filename);
        }
    }
    if (file)
    {
        NSError *error = nil;
        BOOL succeed = [file writeData:data outError:&error];
        [file release];
        
        if (!succeed)
        {
            PALogError(@"Unable to write landing data to file '%@': %@", filename, error);
            return NO;
        }
        
        PALogError(@"Landing data '%@' saved to file '%@'", info.networkId, filename);
        return YES;
    }    
    
    PALogError(@"Unable to open file '%@' for landing info", filename);
    return NO;
}

- (NSData *)trySerializeToData:(PALandingInfo *)info
{
    NSString *errorStr = nil;
    NSDictionary *propertyList = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSNumber numberWithInt:kLandingSerializeFormatVersion], kPropertyKeyFormatVersion,
                                  [NSNumber numberWithInt:info.version], kPropertyKeyVersion,
                                  info.type, kPropertyKeyType,
                                  info.data, kPropertyKeyData, nil];    
    
    NSData *data = [NSPropertyListSerialization dataFromPropertyList:propertyList
                                                              format:NSPropertyListBinaryFormat_v1_0
                                                    errorDescription:&errorStr];
    
    if (data == nil)
    {
        PALogError(@"Unable to serialize '%@' landing data: %@", info.networkId, errorStr);
        return nil;
    }
    
    return data;
}

- (BOOL)tryReadFromFile:(PALandingInfo *)info
{
    NSString *filename = [self filenameFor:info];
    PADataFile *file = [[PADataFile alloc] initWithName:filename];
    if (![file exists])
    {
        [file release];        
        PALogDebug(@"Landing file '%@' does not exists", filename);
        return NO;
    }
    
    BOOL succeed = NO;
    
    NSError* error = nil;
    NSData *data = [file readData:&error];    
    if (data != nil)
    {
        succeed = [self tryDeserializeFromData:data landingInfo:info];
    }
    
    if (!succeed)
    {    
        PALogError(@"Unable to read data from '%@' landing file: %@", filename, error);
        [file deleteFile];
    }
    
    [file release];
    return succeed;
}

- (BOOL)tryDeserializeFromData:(NSData *)data landingInfo:(PALandingInfo *)info
{
    NSString *errorStr = nil;    
    NSPropertyListFormat format = NSPropertyListBinaryFormat_v1_0;
    NSDictionary *propertyList = [NSPropertyListSerialization propertyListFromData: data
                                                                  mutabilityOption: NSPropertyListImmutable
                                                                            format: &format
                                                                  errorDescription: &errorStr];
    if (propertyList != nil)
    {
        NSNumber *formatVersion = [propertyList objectForKey:kPropertyKeyFormatVersion];
        NSNumber *versionNumber = [propertyList objectForKey:kPropertyKeyVersion];
        NSString *type = [propertyList objectForKey:kPropertyKeyType];
        NSString *data = [propertyList objectForKey:kPropertyKeyData];
        
        if ([formatVersion intValue] != kLandingSerializeFormatVersion)
        {
            PALogDebug(@"Landing info format version %d doesn't match expected format version %d", [formatVersion intValue], kLandingSerializeFormatVersion);
            return NO;
        }
        
        NSInteger version = [versionNumber intValue];        
        if (version > 0)
        {        
            landingInfo.version = version;
            [landingInfo setReceivedWithType:type andData:data];
            
            return YES;
        }
        else
        {
            PAAssert(version > 0);
            return NO;
        }
    }
    
    NSLog(@"Unable to deserialize landing info for '%@' from data: %@", info.networkId, errorStr);
    return NO;
}

- (NSString *)filenameFor:(PALandingInfo *)info
{
    return [NSString stringWithFormat:@"landing-%@.db", info.networkId];
}

#pragma mark Landing request

- (void)initiateAdLandingInfoRequest
{
    @synchronized (self)
    {
        NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithCapacity:1];
        [params safeSetObject:landingInfo.networkId forKey:kPAAdRequestParamAdNetwork];
        
        PALogDebug(@"Request landing info with params: %@", [params description]);
        
        NSString *urlString = [[NSString alloc] initWithFormat:@"%@/%@", [PAAdServerAPI baseUrl], kPA_AdServer_LandingURL];
        PAHttpRequest * request = [[PAHttpJSonRequest alloc] initWithURL:urlString andParams:params];
        [[PAAdServerAPI sharedInstance] queueRequest:request
                                              target:self
                                        finishAction:@selector(adLandingInfoRequestFinished:) 
                                          failAction:@selector(adLandingInfoRequestFailed:withError:)];
        [request release];
        [urlString release];
        [params release];
    }
}

- (void)adLandingInfoRequestFinished:(PAHttpJSonRequest *)request
{
    @synchronized (self)
    {
        PAAdJsonHelper *json = [[PAAdJsonHelper alloc] initWithJSonObj:request.responseJson];
        [self parseLandingInfo:json];
        [json release];
        
        if ([landingInfo isValid])
        {
            [self tryWriteToFile:landingInfo];            
        }
        [self loadLandingInfoIntoTheBrowser:landingInfo];
    }
}

- (void)adLandingInfoRequestFailed:(PAHttpJSonRequest *)request withError:(NSError *)error
{
    @synchronized (self)
    {
        [delegate landingInfoFailed:self withError:error];
    }    
}

- (void)parseLandingInfo:(PAAdJsonHelper *)json
{
    PAAdJsonHelper *pageJson = nil;
    if ([json jsonForKey:kParamLandingPage jsonObj:&pageJson])
    {
        NSString *type = [pageJson stringForKey:kParamLandingPageType];
        NSString *data = [pageJson stringForKey:kParamLandingPageData];        
        if (type != nil && data != nil)
        {
            if ([self isLandingTypeValid:type])
            {            
                [landingInfo setReceivedWithType:type andData:data];            
            }
            else
            {
                PAAssertMsgv([self isLandingTypeValid:type], @"Unknown landing type: %@", type);
                [landingInfo invalidate];
            }
        }
        else
        {
            PAAssertMsgv(type != nil && data != nil, @"type='%@' data='%@'", type, data);
            [landingInfo invalidate];
        }
        [pageJson release];
    }
    else
    {
        PALogDebug(@"No landing info for '%@' but was expected", landingInfo.networkId);
        [landingInfo invalidate];
    }
}

- (BOOL)isLandingTypeValid:(NSString *)type
{
    if (type == nil)
    {
        return NO;
    }
    
    return [type isEqualToString:kLandingPageTypeTemplate] || 
    [type isEqualToString:kLandingPageTypeHTML] || 
    [type isEqualToString:kLandingPageTypeURL];
}


#pragma mark Browser loading

- (void)loadLandingInfoIntoTheBrowser:(PALandingInfo *)info
{
    if ([info isValid])
    {
        PALogDebug(@"Loading landing info '%@' into the browser...", info.networkId);
        
        if ([info.type isEqualToString:kLandingPageTypeTemplate])
        {
            // take the ad response data and make a string from it
            NSMutableString *rawResponse = [[NSMutableString alloc] initWithData:info.responseData encoding:NSUTF8StringEncoding];
            info.responseData = nil; // release response data asap because it may occupy a big piece of memory
            
            if (rawResponse != nil)
            {
                // we need to fix the initial response string, because it may turn itself into invalid html code
                [rawResponse replaceOccurrencesOfString:@"</script>" withString:@"<\\/script>" options:0 range:NSMakeRange(0, rawResponse.length)];
                
                // construct mutable instance from landing template data (it's assumed to be an html page)
                NSMutableString *htmlPage = [[NSMutableString alloc] initWithString:info.data];
                if (htmlPage != nil)
                {
                    // set the ad response string as an input param for landing template (landing template is assumed to be javascript injected html page)
                    [htmlPage replaceOccurrencesOfString:@"${pa.raw_response}" withString:rawResponse options:0 range:NSMakeRange(0, htmlPage.length)];
                    [rawResponse release];
                    
                    [browser loadHtmlInBackground:htmlPage withDelegate:self];
                    [htmlPage release];
                }
                else
                {
                    [rawResponse release];
                    PAAssertMsg(htmlPage != nil, @"Can't construct html page");
                    [landingInfo invalidate];
                }
            }
            else
            {
                PAAssertMsg(rawResponse != nil, @"Can't construct raw response string");
                [landingInfo invalidate];
            }
        }
        else if ([info.type isEqualToString:kLandingPageTypeHTML])
        {
            [browser loadHtmlInBackground:info.data withDelegate:self];
        }
        else if ([info.type isEqualToString:kLandingPageTypeURL])
        {
            [browser loadURLInBackground:info.data withDelegate:self];
        }
    }
    else
    {
        PALogError(@"Unable to load invalid landing info '%@' into the browser", info.networkId);
        [delegate landingInfoReceived:self];
    }
}

#pragma mark PAAdWebBrowserBackgroundLoadingDelegate

- (void)adWebBrowserBackgroundLoadingFinished:(PAAdWebBrowserController *)controller
{
    [delegate landingInfoReceived:self];
}

- (void)adWebBrowserBackgroundLoadingFailed:(PAAdWebBrowserController *)controller withError:(NSError *)error
{
    [delegate landingInfoFailed:self withError:error];
}

@end
