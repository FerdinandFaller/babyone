//
//  PALandingManager.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 2/8/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PALandingManager.h"

#import "NSMutableDictionary_Types.h"

#import "PADebug.h"
#import "PALandingLoader.h"

@interface PALandingManager (Private)

- (void)addLandingLoader:(PALandingLoader *)loader;
- (void)removeLandingLoader:(PALandingLoader *)loader;

- (void)notifyLandingInfoReceived:(PALandingInfo *)info;
- (void)notifyLandingInfoFailed:(PALandingInfo *)info withError:(NSError *)error;

@end

@implementation PALandingManager

@synthesize delegate;

- (id)init
{
    self = [super init];
    if (self)
    {
        landingDictionary = [[NSMutableDictionary alloc] init];
        loaders = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [landingDictionary release];
    [loaders release];
    
    self.delegate = nil;
    [super dealloc];
}

- (BOOL)needLoadLandingInfoForAdView:(PAAdCustomAdView *)adView
{
    return adView.landingVersion > 0 && [[self landingInfoForAdView:adView] isUndefined];
}

- (void)loadLandingInfoForForAdView:(PAAdCustomAdView *)adView
{   
    PALandingInfo *info = [self landingInfoForAdView:adView];
    PAAssert([info isUndefined]);
    PAAssert(adView.landingVersion > 0);
    
    if (adView.landingVersion > 0 && [info isUndefined])
    {    
        PALogDebug(@"'%@' has undefined landing info. Expected version: %d", info.networkId, adView.landingVersion);
        info.version = adView.landingVersion;
        info.responseData = adView.bannerData.responseData;
        
        adView.bannerData.responseData = nil; // we need to release the response data asap
        
        PALandingLoader *loader = [[PALandingLoader alloc] initWithInfo:info andBrowser:[adView sharedBrowser]];    
        [self addLandingLoader:loader];
        [loader load];
        [loader release];
    }
    else
    {
        PALogDebug(@"'%@' doesn't need any landing loading", info.networkId);
        [info invalidate];
        [self notifyLandingInfoReceived:info];
    }
}

- (PALandingInfo *)landingInfoForAdView:(PAAdCustomAdView *)adView
{
    if (adView != nil)
    {
        NSString *networkId = adView.networkId;        
        
        PALandingInfo *info = [landingDictionary objectForKey:networkId];
        if (info == nil)
        {
            NSString *networkType = adView.networkType;
            info = [[PALandingInfo alloc] initWithNetworkId:networkId andNetworkType:networkType];
            [landingDictionary safeSetObject:info forKey:networkId];
            [info release]; // dictionary retains the object, so it's safe to release it here
        }
        
        return info;
    }
    else
    {
        PAAssert(adView != nil);
        return nil;
    }
}

- (void)addLandingLoader:(PALandingLoader *)loader
{
    @synchronized (self)
    {
        [loaders addObject:loader];
        loader.delegate = self;        
    }
}

- (void)removeLandingLoader:(PALandingLoader *)loader
{
    @synchronized (self)
    {
        loader.delegate = nil;
        [loaders removeObject:loader];
    }
}

- (void)notifyLandingInfoReceived:(PALandingInfo *)info
{
    [self.delegate landingInfoReceived:info];
}

- (void)notifyLandingInfoFailed:(PALandingInfo *)info withError:(NSError *)error
{
    [self.delegate landingInfoFailed:info withError:error];
}

#pragma mark PALandingLoaderDelegate

- (void)landingInfoReceived:(PALandingLoader *)loader
{
    [self notifyLandingInfoReceived:loader.landingInfo];
    [self removeLandingLoader:loader];
}

- (void)landingInfoFailed:(PALandingLoader *)loader withError:(NSError *)error
{
    [self notifyLandingInfoFailed:loader.landingInfo withError:error];
    [self removeLandingLoader:loader];
}

@end
