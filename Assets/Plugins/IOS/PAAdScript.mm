#import "PAAdScript.h"
#import "PAAdViewController.h"
#import "PACrossPromotion.h"

@implementation PAAdScript

@end

#pragma mark -
#pragma mark Script to native bridge

static NSString* CreateNSString(const char* string);
static PAAdViewAnchor parserAnchor(NSString *anchor);

extern "C"
{
    static PAAdViewController *adController;
    
    void _StartPlacePlayAds(void)
    {
        // add to the view hierarchy
        UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
        UIView *rootView = keyWindow;
        if ([keyWindow respondsToSelector:@selector(rootViewController)])
        {
            UIViewController *rootController = keyWindow.rootViewController;
            adController.rootController = rootController;
            rootView = rootController.view;
        }
        [rootView addSubview:adController.view];
        
        // request ads
        [adController requestAd];
    }
    
    void _PausePlacePlayAds(void)
    {
        [adController stopAd];
        [adController.view removeFromSuperview];
    }
    
    void _GetPlacePlayAds(const char *gameIdStr, float x, float y, float width, float height, bool repeat,
                          const char *anchorStr, bool isInstallTrackEnabled)
    {
        NSString *gameId = CreateNSString(gameIdStr);
        
        if (adController == nil)
        {
            adController = [[PAAdViewController alloc] init]; // TODO: release the memory
        }
        adController.gameId = gameId;
        adController.anchor = parserAnchor(CreateNSString(anchorStr));

        if (isInstallTrackEnabled)
        {
            [[PACrossPromotion sharedInstance] trackInstallationForAppID:gameId];
        }

        _StartPlacePlayAds();
    }
}

#pragma mark -
#pragma mark Helpers

static NSString* CreateNSString(const char* string)
{
	if (string != NULL)
		return [NSString stringWithUTF8String: string];
	else
		return [NSString stringWithUTF8String: ""];
}

static PAAdViewAnchor parserAnchor(NSString *anchor)
{
    PAAdViewAnchor result = (PAAdViewAnchor)0;
    NSArray *array = [anchor componentsSeparatedByString:@"|"];
    for (int i=0; i<[array count]; i++)
    {
        NSString *tagToken = [array objectAtIndex:i];
        
        // vertical alignment
        if ([tagToken isEqualToString:@"Top"] || [tagToken isEqualToString:@"PAAdViewAnchorTop"]) {
            result = (PAAdViewAnchor)(result|PAAdViewAnchorTop);
        }
        else if ([tagToken isEqualToString:@"VerCenter"] || [tagToken isEqualToString:@"PAAdViewAnchorVerCenter"]) {
            result = (PAAdViewAnchor)(result|PAAdViewAnchorVerCenter);
        }
        else if ([tagToken isEqualToString:@"Bottom"] || [tagToken isEqualToString:@"PAAdViewAnchorBottom"]) {
            result = (PAAdViewAnchor)(result|PAAdViewAnchorBottom);
        }
        
        // horizontal alignment
        if ([tagToken isEqualToString:@"Left"] || [tagToken isEqualToString:@"PAAdViewAnchorLeft"]) {
            result = (PAAdViewAnchor)(result|PAAdViewAnchorLeft);
        }
        else if ([tagToken isEqualToString:@"HorCenter"] || [tagToken isEqualToString:@"PAAdViewAnchorHorCenter"]) {
            result = (PAAdViewAnchor)(result|PAAdViewAnchorHorCenter);
        }
        else if ([tagToken isEqualToString:@"Right"] || [tagToken isEqualToString:@"PAAdViewAnchorRight"]) {
            result = (PAAdViewAnchor)(result|PAAdViewAnchorRight);
        }
    }
    
    if (result == 0)
    {
        NSLog(@"PlacePlayAds banner anchor is incorrect: '%@'. Use default instead.", anchor);
        return (PAAdViewAnchor)(PAAdViewAnchorTop|PAAdViewAnchorHorCenter);
    }
    
    return result;
}