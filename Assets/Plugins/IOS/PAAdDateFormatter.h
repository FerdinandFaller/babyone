//
//  PAAdDateFormatter.h
//  PlacePlayAdsDebugSample
//
//  Created by Soumya Behera on 10/07/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAAdDateFormatter : NSObject

+ (NSString *)getTimestampInRFC3339FormatForDate:(NSDate *)date;
+ (NSDate *)dateForTimestampInRFC3339Format:(NSString *)timestamp;

@end
