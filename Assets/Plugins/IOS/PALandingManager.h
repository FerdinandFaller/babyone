//
//  PALandingManager.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 2/8/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PALandingInfo.h"
#import "PALandingLoader.h"

#import "PAAdCustomAdView.h"

@protocol PALandingManagerDelegate <NSObject>

- (void)landingInfoReceived:(PALandingInfo *)info;
- (void)landingInfoFailed:(PALandingInfo *)info withError:(NSError *)error;

@end

@interface PALandingManager : NSObject<PALandingLoaderDelegate>
{
@private
    NSMutableDictionary *landingDictionary;
    NSMutableArray *loaders;
    
    id<PALandingManagerDelegate> delegate;
}

@property (nonatomic, assign) id<PALandingManagerDelegate> delegate;

- (BOOL)needLoadLandingInfoForAdView:(PAAdCustomAdView *)adView;
- (void)loadLandingInfoForForAdView:(PAAdCustomAdView *)adView;
- (PALandingInfo *)landingInfoForAdView:(PAAdCustomAdView *)adView;

@end
