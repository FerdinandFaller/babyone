//
//  PAAdTimerUtils.m
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 24.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import "patimer.h"

void scheduleTimer(NSTimer** timer, NSTimeInterval interval, id target, SEL selector, id userInfo, BOOL repeats)
{
    @synchronized (*timer)
    {
        invalidateTimer(timer);
        *timer = [NSTimer scheduledTimerWithTimeInterval:interval target:target selector:selector userInfo:userInfo repeats:repeats];
    }
}

void invalidateTimer(NSTimer** timer)
{
    @synchronized (*timer)
    {
        NSTimer *obj = *timer;
        if ([obj isValid])
        {
            [obj invalidate];        
        }
        *timer = nil;
    }    
}