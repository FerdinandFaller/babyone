//
//  PAAdCycleStatistics.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 2/10/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PALoadingEvent : NSObject 
{
@private
    NSDate *startDate;
    NSTimeInterval duration;
    BOOL succeed;
    BOOL finished;
}

@property (nonatomic, readonly) NSDate *startDate;
@property (nonatomic, readonly) NSTimeInterval duration;

- (void)started;
- (void)finishedSucceed:(BOOL)succeed;

- (BOOL)isFinished;

@end

@interface PAAdCycleStatistics : NSObject
{
    NSString *requestId;
    
    PALoadingEvent *bannerLoadingEvent;
    PALoadingEvent *pageLoadingEvent;
    
    NSDate *pageClosedDate;
}

- (id)initWithRequestId:(NSString *)requestId;

- (void)notifyBannerStartedLoading;
- (void)notifyBannerFinishedLoadingSucceed:(BOOL)succeed;

- (void)notifyPageStartedLoading;
- (void)notifyPageFinishedLoadingSucceed:(BOOL)succeed;
- (void)notifyPageClosed;

- (BOOL)isPageLoaded;
- (BOOL)isPageClosed;

- (void)sendToServer;

@end
