//
//  UIDevice+machine.h
//  ViewTest
//
//  Created by Alex Lementuev on 08.07.11.
//  Copyright 2011 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIkit.h>

@interface UIDevice (machine)

- (NSString *)PAUuid;
- (NSString *)PAUniqueIdentifier;

@end
