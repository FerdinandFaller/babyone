//
//  PAAdStatisticsController.h
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 21.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PAAdStatisticsController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    UILabel *clicksLabel;
    UILabel *impressionsLabel;
    UILabel *appIdLabel;
    
    NSString *appId;
}

@property (nonatomic, retain) IBOutlet UILabel *clicksLabel;
@property (nonatomic, retain) IBOutlet UILabel *impressionsLabel;
@property (nonatomic, retain) IBOutlet UILabel *appIdLabel;

- (id)initWithAppId:(NSString *)appId;

- (IBAction)onCloseClicked:(id)sender;

@end
