//
//  PAAdJsonHelper.h
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 27.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAAdJsonHelper : NSObject
{
    id jsonObj;    
    PAAdJsonHelper* parent;
}

@property (nonatomic, retain) id jsonObj;
@property (nonatomic, readonly) PAAdJsonHelper* parent;

- (id)initWithJSonObj:(id)jsonObj;

- (BOOL)contains:(id)key;

- (id)objForKey:(id)key;
- (id)objForKey:(id)key def:(id)defaultValue;

- (NSArray *)arrayForKey:(id)key;
- (BOOL)jsonForKey:(id)key jsonObj:(PAAdJsonHelper **)outJson;

- (NSString *)stringForKey:(id)key;
- (NSString *)stringForKey:(id)key def:(NSString *)defaultValue;

- (NSInteger)intForKey:(id)key;
- (NSInteger)intForKey:(id)key def:(NSInteger)defaultValue;

- (float)floatForKey:(id)key;
- (float)floatForKey:(id)key def:(float)defaultValue;

- (BOOL)boolForKey:(id)key;
- (BOOL)boolForKey:(id)key def:(BOOL)defaultValue;

+ (BOOL)allParamsArePresent:(PAAdJsonHelper *)json, ...;

@end
