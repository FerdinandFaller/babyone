//
//  PABannerCategoryFactory.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 3/25/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PABannerCategoryFactory.h"

#import "PABannerCategoryChainExtractor.h"
#import "PABannerCategoryRegExExtractor.h"
#import "PABannerCategorySimpleExtractor.h"

#import "PADebug.h"

#define kParamType          @"type"

#define kParamTypeChain     @"chain"
#define kParamTypeRegexp    @"regexp"
#define kParamTypeSimple    @"simple"

@implementation PABannerCategoryFactory

+ (void)tryParseCategoryExtractorFromJson:(PAAdJsonHelper *)json pointer:(PABannerCategoryExtractor **)pExtractor;
{
    if (pExtractor != NULL)
    {
        NSString* type = [json stringForKey:kParamType];
        if (type != nil)
        {
            if ([type isEqualToString:kParamTypeRegexp])
            {
                *pExtractor = [[PABannerCategoryRegExExtractor alloc] initWithJson:json];
            }
            else if ([type isEqualToString:kParamTypeChain])
            {
                *pExtractor = [[PABannerCategoryChainExtractor alloc] initWithJson:json];
            }
            else if ([type isEqualToString:kParamTypeSimple])
            {
                *pExtractor = [[PABannerCategorySimpleExtractor alloc] initWithJson:json];
            }
            else
            {
                PAAssertMsgv(false, @"Unknown extractor type: %@", type);
            }
        }
        else 
        {
            PAAssertMsg(@"Banner category extractor has not type: %@", [json jsonObj]);
        }
    }
}

@end
