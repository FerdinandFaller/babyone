//
//  PAAdServerAPI.h
//  PlacePlayAds
//
//  Created by Alex Koloskov on 9/22/11.
//  Copyright 2011 PressOK Entertainment. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import "PAHttpRequest.h"
#import "PAHttpJSonRequest.h"
#import "NSMutableDictionary_Types.h"
#import "PALocationInfo.h"

#define kPA_AdServer_BaseURLString               @"http://ads.placeplay.com"
#define kPA_AdServer_Publisher                   @"mobliss"
#define kPA_AdServer_GetAdURL                    @"api/ads.json"
#define kPA_AdServer_TrackURL                    @"api/events"
#define kPA_AdServer_StatsURL                    @"api/stats"
#define kPA_AdServer_InitURL                     @"api/init"
#define kPA_AdServer_LandingURL                  @"api/landing"
#define kPA_AdServer_InstallTrackURL             @"api/install"

#define kPAAdRequestParamRequestId               @"request_id"
#define kPAAdRequestParamAdNetwork               @"ad_network"
#define kPAAdRequestParamSecondaryClick          @"secondary_click"
#define kPAAdRequestParamPublisher               @"publisher"
#define kPAAdRequestParamCategory                @"category"

@protocol PAAdServerAPIDataSource <NSObject>

- (NSString *)appId;
- (NSString *)userId;
- (CLLocation *)deviceLocation;

- (NSString *)sdkVersion;
- (NSString *)protocolVersion;
- (NSString *)bannerSizeString;

- (PALocationInfo *)locationInfo;
- (NSDictionary *)targetingParams;

- (NSString *)pluginPackageParam;
- (NSDictionary *)testingParams;

@end

@interface PAAdServerAPI : NSObject
{
    id<PAAdServerAPIDataSource> dataSource;
    NSString *userAgent;
}

@property (nonatomic, readonly) NSString *userAgent;

- (id)initWithDataSource:(id<PAAdServerAPIDataSource>)dataSource;
- (void)queueRequest:(PAHttpRequest *)request target:(id)target finishAction:(SEL)finishAction failAction:(SEL)failAction;
- (void)cancelAllRequests;

+ (void)createSharedInstance:(id<PAAdServerAPIDataSource>)dataSource;
+ (PAAdServerAPI *)sharedInstance;

+ (void)setBaseUrl:(NSString *)baseUrl;
+ (NSString *)baseUrl;

@end
