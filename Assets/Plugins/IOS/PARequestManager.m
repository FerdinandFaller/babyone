//
//  PARequestManager.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 1/14/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PARequestManager.h"

static PARequestManager *instance = nil;

@implementation PARequestManager

+ (PARequestManager *)sharedInstance
{
    if (instance == nil)
    {
        instance = [[PARequestManager alloc] init];
    }
    return instance;
}

- (id)init
{
    self = [super init];
    if (self) {
        requests = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [requests release];
    [super dealloc];
}

- (void)queueRequest:(PAHttpRequest *)request
{
    @synchronized (self)
    {
        [requests addObject:request];
    }
}

- (void)removeRequest:(PAHttpRequest *)request
{
    @synchronized (self)
    {
        [requests removeObject:request];
    }
}

- (void)cancelAll
{
    @synchronized (self)
    {
        NSArray *temp = [[NSArray alloc] initWithArray:requests];        
        for (PAHttpRequest *request in temp)
        {
            [request cancel];
        }
        [requests removeAllObjects];        
        [temp release];
    }
}

@end
