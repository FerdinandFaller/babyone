//
//  PALocationManager.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 5/22/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface PALocationManager : NSObject<CLLocationManagerDelegate>
{
@private
    CLLocationManager *locationManager_;
    CLLocation *lastKnownLocation_;
    NSTimer *locationUpdateTimer_;
    NSTimeInterval updateInterval;
}

@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, copy) CLLocation *lastKnownLocation;
@property (nonatomic, assign) NSTimer *locationUpdateTimer;

- (void)startUpdatingLocation;
- (void)stopUpdatingLocation;

- (CLLocation *)location;

- (id)initWithUpdateInterval:(NSTimeInterval)interval;

- (void)setExpiryInterval:(NSTimeInterval)interval;

@end
