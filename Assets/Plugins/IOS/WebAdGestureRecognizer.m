//
//  WebAdGestureRecognizer.m
//
//  Created by Alex Koloskov on 9/30/11.
//  Copyright 2011 PressOK Entertainment. All rights reserved.
//

#import "WebAdGestureRecognizer.h"

@implementation WebAdGestureRecognizer

- (id)initWithTarget:(id)aTarget action:(SEL)anAction
{
	if (self = [super initWithTarget:aTarget action:anAction])
	{
		[self setCancelsTouchesInView:NO];
		[self setDelegate:self];
		target = aTarget;
		action = anAction;        
	}

    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [target performSelector:action];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
}

- (void)reset
{
}

- (void)ignoreTouch:(UITouch *)touch forEvent:(UIEvent *)event
{
}

#if PA_IOS_SDK_AVAILABLE(__IPHONE_3_2)

- (BOOL)canBePreventedByGestureRecognizer:(UIGestureRecognizer *)preventingGestureRecognizer
{
    return NO;
}

- (BOOL)canPreventGestureRecognizer:(UIGestureRecognizer *)preventedGestureRecognizer
{
    return NO;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}

#endif // A_IOS_SDK_AVAILABLE(__IPHONE_3_2)

@end
