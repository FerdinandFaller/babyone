//
//  PAAdView+Internal.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 2/13/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PAAdView.h"
#import "PALocationInfo.h"

@interface PAAdView (Internal)

@property (nonatomic, retain) PAAdCustomAdView *currentAdView;
@property (nonatomic, retain) PABannerCache *bannerCache;
@property (nonatomic, retain) NSArray *adTimeoutData;

- (void)activate;
- (void)deactivate;

@end
