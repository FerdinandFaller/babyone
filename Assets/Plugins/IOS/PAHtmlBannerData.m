//
//  PAHtmlBannerData.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 1/22/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PAHtmlBannerData.h"

#import "PAAdHtmlBasedAdView.h"

#define kPAAdResponseParamHtmlSnippet   @"html_snippet"
#define kPAAdResponseParamTimeChunks    @"time_chunks"

@implementation PAHtmlBannerData

@synthesize htmlSnippet;
@synthesize subBannersCount;

- (id)initWithJSon:(PAAdJsonHelper *)json andResponseData:(NSData *)data
{
    self = [super initWithJSon:json andResponseData:data];
    if (self)
    {        
        if (![PAAdJsonHelper allParamsArePresent:json, kPAAdResponseParamHtmlSnippet, nil]) 
        {
            [self release];
            return nil;
        }
        
        htmlSnippet = [[json stringForKey:kPAAdResponseParamHtmlSnippet] copy];
        subBannersCount = [json intForKey:kPAAdResponseParamTimeChunks def:1];
    }
    return self;
}

- (PAAdCustomAdView *)createAdViewWithFrame:(CGRect)frame
{
    return [[[PAAdHtmlBasedAdView alloc] initWithBannerData:self andFrame:frame] autorelease];
}

- (void)dealloc
{
    [htmlSnippet release];
    [super dealloc];
}

@end
