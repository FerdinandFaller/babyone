//
//  PAUnityScriptMessenger.m
//  Unity-iPhone
//
//  Created by Alex Lementuev on 10/23/12.
//
//

#import "PAUnityScriptMessenger.h"

#import "PADebug.h"

@implementation PAUnityScriptMessenger

- (id)initWithTargetObject:(NSString *)objectName andTargetMethod:(NSString *)methodName
{
    self = [super init];
    if (self)
    {
        _targetObjectName = [objectName copy];
        _targetMethodName = [methodName copy];
    }
    
    return self;
}

- (void)dealloc
{
    [_targetObjectName release];
    [_targetMethodName release];
    
    [super dealloc];
}

- (void)sendMessage:(NSString *)message
{
    if (message != nil)
    {
        const char * obj = _targetObjectName.UTF8String;
        const char * method = _targetMethodName.UTF8String;
        const char * msg = message.UTF8String;
        
        UnitySendMessage(obj, method, msg);
    }
    else
    {
        PAAssertMsg(message != nil, @"Can't send nil message to the object");
    }
}

@end
