//
//  PAUIStatistics.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 2/10/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#if PA_AD_STATISTICS

#import "PAUIStatistics.h"

static PAUIStatistics *sharedInstance;

@implementation PAUIStatistics

@synthesize cacheLabel;
@synthesize landingLabel;
@synthesize pageLabel;
@synthesize statusLabel;
@synthesize locationLabel;

+ (PAUIStatistics *)sharedInstance
{
    @synchronized ([PAUIStatistics class])
    {
        if (sharedInstance == nil)
        {
            sharedInstance = [[PAUIStatistics alloc] init];
        }
        return sharedInstance;
    }
}

- (void)setLabelColor:(UILabel *)label color:(UIColor *)color
{
    label.textColor = color;
}

- (void)setLabelRed:(UILabel *)label
{
    [self setLabelColor:label color:[UIColor redColor]];
}

- (void)setLabelYellow:(UILabel *)label
{   
    [self setLabelColor:label color:[UIColor orangeColor]];
}

- (void)setLabelGreen:(UILabel *)label
{
    UIColor *color = [[UIColor alloc] initWithRed:0 green:0.34 blue:0.15 alpha:1.0];
    [self setLabelColor:label color:color];
    [color release];
}


- (void)setCacheSize:(NSUInteger)size
{
    NSString *text = [[NSString alloc] initWithFormat:@"Cache: %d", size];
    cacheLabel.text = text;
    [text release];
}

- (void)setCacheDisabled
{
    [self setLabelRed:cacheLabel];
}

- (void)setCachePopulating
{
    [self setLabelYellow:cacheLabel];
}

- (void)setCacheFull
{
    [self setLabelGreen:cacheLabel];
}

- (void)setLandingPageNotLoaded
{
    [self setLabelRed:landingLabel];
}

- (void)setLandingPageLoading
{
    [self setLabelYellow:landingLabel];
}

- (void)setLandingPageLoaded
{
    [self setLabelGreen:landingLabel];
}

- (void)setPageNotLoaded
{
    [self setLabelRed:pageLabel];
}

- (void)setPageLoading
{
    [self setLabelYellow:pageLabel];
}

- (void)setPageLoaded
{
    [self setLabelGreen:pageLabel];
}

- (void)setStatus:(NSString *)text
{
    self.statusLabel.text = text;
}

- (void)setLocationStatus:(NSString *)text
{
    self.locationLabel.text = text;
}

@end

#endif // #if PA_AD_STATISTICS
