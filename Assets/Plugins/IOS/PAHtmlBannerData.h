//
//  PAHtmlBannerData.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 1/22/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PABannerData.h"

@interface PAHtmlBannerData : PABannerData
{
    NSString *htmlSnippet;
    NSUInteger subBannersCount;
}

@property (nonatomic, readonly) NSString *htmlSnippet;
@property (nonatomic, readonly) NSUInteger subBannersCount;

@end
