//
//  PAAvailability.h
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 4/21/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#ifndef PlacePlayAdsSample_PAAvailability_h
#define PlacePlayAdsSample_PAAvailability_h

#import <UIKit/UIKit.h>

#ifndef __IPHONE_3_2
#define __IPHONE_3_2     30200
#endif

#ifndef __IPHONE_4_0
#define __IPHONE_4_0     40000
#endif

#ifndef __IPHONE_5_0
#define __IPHONE_5_0     50000
#endif

#define PA_SYSTEM_VERSION_MIN __IPHONE_3_2

#define PA_IOS_SDK_AVAILABLE(sdk_ver) (__IPHONE_OS_VERSION_MAX_ALLOWED >= (sdk_ver))
#define PA_SYSTEM_VERSION_AVAILABLE(sys_ver) PAAvailabilitySystemVersionAvailable(sys_ver)
#define PA_SELECTOR_AVAILABLE(obj, sel) [(obj) respondsToSelector:@selector(sel)]
#define PA_CLASS_AVAILABLE(className) (NSClassFromString(@#className) != nil)

typedef NSUInteger PASystemVersion;

void PAAvailabilityDetectSystemVersion(void);
BOOL PAAvailabilitySystemVersionAvailable(PASystemVersion version);

#endif
