//
//  ViewController.m
//  Unity-iPhone
//
//  Created by Deepthi Taduvayi on 07/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PAAdViewController.h"
#import "PAAdStatisticsController.h"

#import "PADebug.h"

#define TIME_INTERVAL 5.0;

static NSString * const kPAUnityScriptTargetObjectName  = @"Main Camera";
static NSString * const kPAUnityScriptTargetMethodName  = @"dispatchNativeMessage";

static NSString * const kPAMessageAdReceived            = @"com.placeplay.adreceived";
static NSString * const kPAMessageAdFailed              = @"com.placeplay.adfailed";
static NSString * const kPAMessageWillPresentFullscreen = @"com.placeplay.willpresentfullscreen";
static NSString * const kPAMessageDidDismissFullscreen  = @"com.placeplay.diddismissfullscreen";

@implementation PAAdViewController

@synthesize gameId=_gameId,
            rootController=_rootController,
            anchor=_anchor;

- (id)init
{
    self = [super init];
    if (self)
    {
        _scriptMessanger = [[PAUnityScriptMessenger alloc] initWithTargetObject:kPAUnityScriptTargetObjectName
                                                                andTargetMethod:kPAUnityScriptTargetMethodName];
    }
    return self;
}

- (void)dealloc
{
    self.gameId = nil;
    
    [_scriptMessanger release];
    
    [super dealloc];
}

#pragma mark - 
#pragma mark View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // add ads view
    _adView = [PAAdView sharedAdView:self];
    [self.view addSubview:_adView];
    
    // resize it
    [self adjustAdViewPosition];
}

#pragma mark -
#pragma mark Ad requests

- (void)requestAd
{
    [self adjustAdViewPosition];
    [_adView requestAd];
}

- (void)stopAd
{
    [_adView stop];
}

#pragma mark -
#pragma mark View positioning

- (void)adjustAdViewPosition
{
    UIInterfaceOrientation currentOrientation = [self currentIntefaceOrientation];
    CGSize bounds = [self windowBounds:currentOrientation];
    
    CGFloat statusBarHeight = [UIApplication sharedApplication].isStatusBarHidden ? 0 : 20;
    
    CGFloat adX = 0;
    CGFloat adY = 0;
    CGSize adSize = _adView.bounds.size;
    
    UIViewAutoresizing autoresizingMask = UIViewAutoresizingNone;
    
    if ((_anchor & PAAdViewAnchorHorCenter) != 0)
    {
        adX = 0.5 * (bounds.width - adSize.width);
        autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    }
    else if ((_anchor & PAAdViewAnchorRight) != 0)
    {
        adX = bounds.width - adSize.width;
        autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    }
    
    if ((_anchor & PAAdViewAnchorVerCenter) != 0)
    {
        adY = 0.5 * (bounds.height - adSize.height);
        autoresizingMask |= UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    }
    else if ((_anchor & PAAdViewAnchorBottom) != 0)
    {
        adY = bounds.height - adSize.height;
        autoresizingMask |= UIViewAutoresizingFlexibleTopMargin;
    }
    else
    {
        adY = statusBarHeight;
    }
    
    self.view.frame = CGRectMake(adX, adY, adSize.width, adSize.height);
    self.view.autoresizingMask = autoresizingMask;
}

- (CGSize)windowBounds:(UIInterfaceOrientation)orientation
{
    CGSize bounds = [UIApplication sharedApplication].keyWindow.bounds.size;
    if (UIInterfaceOrientationIsLandscape(orientation))
    {
        return CGSizeMake(bounds.height, bounds.width);
    }
    
    return bounds;
}

- (UIInterfaceOrientation)currentIntefaceOrientation
{
    return [UIApplication sharedApplication].statusBarOrientation;
}

#pragma mark -
#pragma mark PAAdDelegate

- (NSString*)appId
{
    return _gameId;
}

- (void)didReceivedAd:(PAAdView *)adView
{
    [_scriptMessanger sendMessage:kPAMessageAdReceived];
}

- (void)didFailToReceiveAd:(PAAdView *)adView
{
    [_scriptMessanger sendMessage:kPAMessageAdFailed];
    
    NSTimeInterval retryDelay = TIME_INTERVAL;
    [NSTimer scheduledTimerWithTimeInterval:retryDelay
                                     target:self
                                   selector:@selector(retryAdRequest)
                                   userInfo:nil
                                    repeats:NO];
}

- (void)willPresentFullScreenModal
{
    [_scriptMessanger sendMessage:kPAMessageWillPresentFullscreen];
}

- (void)didDismissFullScreenModal
{
    [_scriptMessanger sendMessage:kPAMessageDidDismissFullscreen];
}

- (PAADViewBannerSize)bannerSize
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return PAADViewBannerSize768x90;
    }
    else
    {
        return PAADViewBannerSize320x50;
    }
}

- (UIViewController*)viewControllerForPresentingModalView
{
    PAAssert(_rootController != nil);
    return _rootController;
}

- (NSString *)pluginPackageParam
{
    return @"plugin-package-unity-ios";
}

- (NSDictionary *)targetingParams
{
    return nil;
}

#pragma mark -
#pragma mark Interface Rotations

// pre iOS 6.0
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    PAAssert(_rootController != nil);
    return [_rootController shouldAutorotateToInterfaceOrientation:interfaceOrientation];
}

// iOS 6.0+
- (BOOL)shouldAutorotate
{
    PAAssert(_rootController != nil);
    if ([_rootController respondsToSelector:@selector(shouldAutorotate)])
    {
       return [_rootController shouldAutorotate];
    }
    
    PAAssert(false);
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    PAAssert(_rootController != nil);
    if ([_rootController respondsToSelector:@selector(supportedInterfaceOrientations)])
    {
        return [_rootController supportedInterfaceOrientations];
    }
    
    PAAssert(false);
    return 0;
}

#pragma mark -
#pragma mark Helpers

- (void)retryAdRequest
{
    [_adView requestAd];
}

@end
