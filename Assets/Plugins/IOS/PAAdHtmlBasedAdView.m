//
//  PAAdHtmlBasedAdView.m
//  PlacePlayAds Sample
//
//  Created by Alex Lementuev on 16.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import "PAAdHtmlBasedAdView.h"

#if PA_AD_STATISTICS
#import "PAAdStatistics.h"
#endif

#import "patimer.h"
#import "PADebug.h"

#import "PAHtmlBannerData.h"

#import "PADataFile.h"

static PAAdWebBrowserController *sharedBrowser;
static UIWebView *sharedWebView;

@interface PAAdHtmlBasedAdView (Private)

- (void)webViewTapped;

- (void)startSubBannerTimer;
- (void)cancelSubBannerTimer;
- (void)onSubBannerTimer:(NSTimer *)timer;
- (BOOL)canTickSubBannerTimer;

- (void)writeBannerPageToFile:(NSString *)networkId;

- (UIWebView *)sharedWebView;
+ (UIWebView *)sharedWebViewInstance;
+ (void)loadIntoSharedWebView:(NSString *)htmlPage;

@end

@implementation PAAdHtmlBasedAdView

@synthesize tapRecognizer;

- (id)initWithBannerData:(PAHtmlBannerData *)data andFrame:(CGRect)frame
{
    self = [super initWithBannerData:data andFrame:frame];
    if (self) 
    {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)dealloc
{
    if ([self sharedWebView].delegate == self)
    {
        [self sharedWebView].delegate = nil;
    }
    
    [[self sharedWebView] removeGestureRecognizer:self.tapRecognizer];
    self.tapRecognizer = nil;
    [super dealloc];
}

- (void)loadContent
{
    [super loadContent];
    
    firstLoad = YES;
    
    UIWebView *webView = [self sharedWebView];
    webView.hidden = NO;
    webView.alpha = 0;
    webView.delegate = self;
    webView.frame = self.bounds;    
    [self addSubview:webView];    
    WebAdGestureRecognizer *gestureRecognizer = [[WebAdGestureRecognizer alloc] initWithTarget:self action:@selector(webViewTapped)];
    [sharedWebView addGestureRecognizer:gestureRecognizer];
    self.tapRecognizer = gestureRecognizer;
    [gestureRecognizer release];   
    
    [webView loadHTMLString:self.htmlSnippet baseURL:nil];
}

- (void)cancelLoadContent
{
    [super cancelLoadContent];
    [[self sharedWebView] stopLoading];
}

- (void)stop
{
    [super stop];    
    [self cancelSubBannerTimer];
}

- (void)webViewTapped
{    
    @synchronized (self) 
    {
        webViewTapped = YES;
        [self notifyAdTapped];
    }
}

#pragma mark - Notify routine

- (void)notifyAdStarted
{
    [super notifyAdStarted];
    [self startSubBannerTimer]; // start banner lifetime "ticks"
}

- (void)notifyAdClosed
{
    fullscreenAdShowed = NO;
    [super notifyAdClosed];
}

- (void)notifyAdFailed:(NSError *)error
{
    [self cancelSubBannerTimer];    
    [super notifyAdFailed:error];    
}

- (void)notifyAdFinished
{
    [self cancelSubBannerTimer];
    [super notifyAdFinished];    
}

#pragma mark - UIWebViewDelegate delegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if (firstLoad)
    {
        firstLoad = NO;
        [UIView beginAnimations:@"PAAdHtmlBasedAdView" context:nil];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:0.5];
        [self sharedWebView].alpha = 1.0;
        [UIView commitAnimations];
    }
    
    if (self.bannerData.cacheResponse)
    {
        [self writeBannerPageToFile:self.networkId];
    }
    
    [self contentLoaded];    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    if (error.code != NSURLErrorCancelled)
    {    
        [self contentLoadingFailedWithError:error];
        [self notifyAdFailed:error];
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    @synchronized (self)
    {
        if (webViewTapped)
        {
            webViewTapped = NO;          
            fullscreenAdShowed = YES;
            
            PALogDebug(@"Landing page URL: %@", [request URL]);
            
            [self showBrowserWithRequest:request];
            return NO;
        }
    }
    
    return YES;
}

#pragma mark Shared web view

- (UIWebView *)sharedWebView
{
    return [PAAdHtmlBasedAdView sharedWebViewInstance];
}

+ (UIWebView *)sharedWebViewInstance
{
    @synchronized ([PAAdHtmlBasedAdView class])
    {
        if (sharedWebView == nil)
        {
            sharedWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)]; // hack: find a way to set up the size properly
        }
        return sharedWebView;
    }           
}

+ (void)loadIntoSharedWebView:(NSString *)htmlPage
{
    UIWebView *webView = [PAAdHtmlBasedAdView sharedWebViewInstance];
    if (webView.superview == nil)
    {
        webView.hidden = YES;
        UIView *parentView = [UIApplication sharedApplication].keyWindow;
        [parentView addSubview:webView];
    }
    
    [webView loadHTMLString:htmlPage baseURL:nil];
}

#pragma mark Shared browser

- (PAAdWebBrowserController *)sharedBrowser
{
    @synchronized ([PAAdHtmlBasedAdView class])
    {
        if (sharedBrowser == nil)
        {
            sharedBrowser = [[PAAdWebBrowserController alloc] init];
            PALogInfo(@"Html based banner created shared browser instance");
        }
        return sharedBrowser;
    }
}

#pragma mark PAAdWebBrowserControllerDelegate methods

- (void)adWebBrowserPageDidStartLoad:(PAAdWebBrowserController *)controller
{   
    #if PA_AD_STATISTICS    
    [[PAAdStatistics sharedInstance].currentRequestInfo.pageStopWatch start];
    #endif
}

- (void)adWebBrowserPageDidFinishLoad:(PAAdWebBrowserController *)controller
{
    #if PA_AD_STATISTICS
    [[PAAdStatistics sharedInstance].currentRequestInfo.pageStopWatch stop];
    [[PAAdStatistics sharedInstance].currentRequestInfo setSucceed];
    #endif
}

- (void)adWebBrowserPageDidFinishWithError:(PAAdWebBrowserController *)controller error:(NSError *)error
{
    #if PA_AD_STATISTICS
    [[PAAdStatistics sharedInstance].currentRequestInfo.pageStopWatch stop];
    [[PAAdStatistics sharedInstance].currentRequestInfo setPageFailed];
    #endif
}

#pragma mark Sub Banner timer

- (void)startSubBannerTimer
{
    PALogDebug(@"html based started sub banner timer: %f", bannerData.timeout);
    scheduleTimer(&subBannerTimer, bannerData.timeout, self, @selector(onSubBannerTimer:), nil, YES);
}

- (void)cancelSubBannerTimer
{
    @synchronized (self)
    {
        PALogDebug(@"html based canceled sub banner timer");
        invalidateTimer(&subBannerTimer);
    }  
}

- (void)onSubBannerTimer:(NSTimer *)timer
{
    if (![self canTickSubBannerTimer])
    {
        PALogDebug(@"html based sub banner timer tick ignored");
        return;
    }
    
    subBannersShownCount++;
    PALogDebug(@"html base sub banner showed %d total %d", subBannersShownCount, self.subBannersCount);
    if (subBannersShownCount < self.subBannersCount)
    {
        // we're good
    }
    else
    {
        PALogDebug(@"html based sub banners timers finished");
        [self cancelSubBannerTimer];
        [self notifyAdFinished];
    }
}

- (BOOL)canTickSubBannerTimer
{
    return !fullscreenAdShowed;
}

#pragma mark Html file cache

- (void)writeBannerPageToFile:(NSString *)networkId
{
    NSData *htmlData = [self.htmlSnippet dataUsingEncoding:NSUTF8StringEncoding];
    NSString *filename = [[NSString alloc] initWithFormat:@"banner-%@.html", networkId];
    PADataFile *file = [[PADataFile alloc] initWithName:filename];
    [filename release];
    
    NSError *error = nil;
    BOOL writeSucceed = [file writeData:htmlData outError:&error];
    if (writeSucceed)
    {
        PALogDebug(@"'%@' banner page saved to file '%@'", networkId, filename);
    }
    else
    {
        PALogDebug(@"Unable to write banner '%@' page to file '%@': %@", networkId, filename, error);
    }
    
    [file release];
}

+ (void)readBannerPageFromFile:(NSString *)networkId
{
    NSString *filename = [[NSString alloc] initWithFormat:@"banner-%@.html", networkId];
    PADataFile *file = [[PADataFile alloc] initWithName:filename];
    [filename release];
    
    if ([file exists])
    {    
        NSError *error = nil;
        NSData *htmlData = [file readData:&error];
        if (htmlData != nil)
        {            
            NSString *pageHtml = [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
            [self loadIntoSharedWebView:pageHtml];
            [pageHtml release];
            PALogDebug(@"'%@' banner page read from file '%@'", networkId, filename);
        }
        else
        {
            PALogDebug(@"Unable to read banner '%@' page from file '%@': %@", networkId, filename, error);
            [file deleteFile];
        }
    }
    else
    {
        PALogDebug(@"No cache file for '%@' banner", networkId);
    }
    
    [file release];
}

+ (NSString *)bannerPageFilename:(NSString *)networkId
{
    return [NSString stringWithFormat:@"banner-%@.html", networkId];
}

#pragma mark Properties

- (PAHtmlBannerData *)htmlBannerData
{
    return (PAHtmlBannerData *)bannerData;
}

- (NSUInteger)subBannersCount
{
    return [self htmlBannerData].subBannersCount;
}

- (NSString *)htmlSnippet
{
    return [self htmlBannerData].htmlSnippet;
}

@end
