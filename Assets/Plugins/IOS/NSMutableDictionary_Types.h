//
//  NSDictionary+Types.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 2/10/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (Types)

- (void)setIntValue:(NSInteger)value forKey:(NSString *)key;
- (void)setUIntValue:(NSUInteger)value forKey:(NSString *)key;
- (void)setBoolValue:(BOOL)value forKey:(NSString *)key;
- (void)setDoubleValue:(double)value forKey:(NSString *)key;
- (void)safeSetObject:(id)object forKey:(NSString *)key;

@end
