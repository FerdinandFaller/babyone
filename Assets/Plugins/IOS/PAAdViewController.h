//
//  ViewController.h
//  Unity-iPhone
//
//  Created by Deepthi Taduvayi on 07/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PAAdView.h"
#import "PAUnityScriptMessenger.h"

@interface PAAdViewController : UIViewController <PAAdDelegate> {
    PAAdView *_adView;
    NSString *_gameId;
    PAAdViewAnchor _anchor;
    UIViewController *_rootController;
    
    PAUnityScriptMessenger *_scriptMessanger;
}

@property(nonatomic, copy) NSString *gameId;

@property(nonatomic, assign) UIViewController *rootController;
@property(nonatomic, assign) PAAdViewAnchor anchor;

- (void)requestAd;
- (void)stopAd;

@end
