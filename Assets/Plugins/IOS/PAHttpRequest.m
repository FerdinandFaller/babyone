//
//  PAHttpRequest.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 1/13/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PAHttpRequest_Inheritance.h"
#import "NSMutableDictionary_Types.h"

#import "PARequestManager.h"
#import "PADebug.h"
#define kPAHttpRequestDefaultTimeout 60.0

@interface PAHttpRequest (Private)

- (void)setupDefaults;
- (void)startDelayed;

- (void)setupRequestStartDate;
- (void)calculateRequestDuration;

@end

@implementation PAHttpRequest

@synthesize urlString;
@synthesize params;
@synthesize headers;
@synthesize timeout;
@synthesize connection;
@synthesize responseCode;
@synthesize responseData;
@synthesize startDate;
@synthesize duration;
@synthesize userData;
@synthesize tag;

- (id)initWithURL:(NSString *)aUrlString
{
    return [self initWithURL:aUrlString andTimeout:kPAHttpRequestDefaultTimeout];
}

- (id)initWithURL:(NSString *)aUrlString andTimeout:(NSTimeInterval)aTimeout
{
    return [self initWithURL:aUrlString andTimeout:aTimeout andParams:nil];
}

- (id)initWithURL:(NSString *)aUrlString andParams:(NSDictionary *)aParams
{
    return [self initWithURL:aUrlString andTimeout:kPAHttpRequestDefaultTimeout andParams:aParams];
}

- (id)initWithURL:(NSString *)aUrlString andTimeout:(NSTimeInterval)aTimeout andParams:(NSDictionary *)aParams
{
    return [self initWithURL:aUrlString andTimeout:aTimeout andParams:aParams andHeaders:nil];
}

- (id)initWithURL:(NSString *)aUrlString andTimeout:(NSTimeInterval)aTimeout andParams:(NSDictionary *)aParams andHeaders:(NSDictionary *)aHeaders
{
    self = [super init];
    if (self) {
        state = PAHttpRequestStateNotStarted;

        if (aParams)
        {
            NSMutableDictionary *mutableParams = [[NSMutableDictionary alloc] initWithDictionary:aParams];
            self.params = mutableParams;
            [mutableParams release];
        }
        
        if (aHeaders)
        {
            NSMutableDictionary *mutableHeaders = [[NSMutableDictionary alloc] initWithDictionary:aHeaders];
            self.headers = mutableHeaders;
            [mutableHeaders release];
        }
        
        self.urlString = aUrlString;
        timeout = aTimeout;
        [self setupDefaults];
    }
    return self;
}

- (void)setParam:(id)param forKey:(id)key
{
    if (!params)
    {
        params = [[NSMutableDictionary alloc] init];
    }
    [params safeSetObject:param forKey:key];
}

- (void)setHttpHeader:(id)value forKey:(id)key
{
    if (!headers)
    {
        headers = [[NSMutableDictionary alloc] init];
    }

    [headers safeSetObject:value forKey:key];
}

- (void)dealloc
{
    self.urlString = nil;
    self.params = nil;
    self.connection = nil;
    self.responseData = nil;
    self.startDate = nil;
    self.userData = nil;
    
    [finishTarget release];
    [errorTarget release];
    [cancelTarget release];
    
    [super dealloc];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"tag: %d url:\"%@\" params:%@", tag, urlString, [params description]];
}

static NSString * URLEncodedStringFromString(NSString *string);

- (NSURLRequest *)createRequest
{
    // create url string
    NSMutableString *urlBuffer = [[NSMutableString alloc] initWithString:self.urlString];
    if ([params count] > 0) 
    {
        if (![urlBuffer hasSuffix:@"/"]) 
        {
            [urlBuffer appendString:@"/"];
        }
        
        [urlBuffer appendString:@"?"];
        
        int paramIndex = 0;
        for (id paramName in params) 
        {
            id paramValue = [params objectForKey:paramName];
            if (paramIndex > 0) 
            {
                [urlBuffer appendString:@"&"];
            }
            paramIndex++;
            
            [urlBuffer appendFormat:@"%@=%@", URLEncodedStringFromString([paramName description]), 
             URLEncodedStringFromString([paramValue description])];            
        }
    }
    
    // create request
    NSURL *url = [[NSURL alloc] initWithString:urlBuffer];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:timeout];
    PALogInfo(@"Final url is : %@", [url description]);
    
    
    [url release];
    [urlBuffer release];
    
    // setup headers
    [self setHttpHeaders:request];    
    
    return request;
}

- (void)setHttpHeaders:(NSMutableURLRequest *)request
{
    if ([headers count] > 0)
    {
        for (id headerName in headers)
        {
            id headerValue = [headers objectForKey:headerName];
            [request setValue:headerValue forHTTPHeaderField:headerName];
        }
    }
}

- (void)startDelayed
{
    @synchronized (self)
    {
        state = PAHttpRequestStateStarted;
        
        NSURLRequest *request = [self createRequest];    
        NSURLConnection *aConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (aConnection != nil) 
        {            
            self.connection = aConnection;
            self.responseData = [NSMutableData data];
            
            [self setupRequestStartDate];            
            [[PARequestManager sharedInstance] queueRequest:self];
        } 
        else 
        {
            [self finishWithErrorMessage:@"Can't create connection" andErrorCode:PAHttpRequestErrorCodeConnectionError];
        }
        [aConnection release];
    }
}

- (void)start
{
    [self performSelectorOnMainThread:@selector(startDelayed) withObject:nil waitUntilDone:YES];
}

- (void)cancel
{
    @synchronized(self)
    {
        if (state == PAHttpRequestStateFinished)
        {
            [NSObject cancelPreviousPerformRequestsWithTarget:finishTarget selector:finishAction object:nil];
        }
        else if (state == PAHttpRequestStateFinishedError)
        {
            [NSObject cancelPreviousPerformRequestsWithTarget:errorTarget selector:errorAction object:nil];
        }
        else if (state == PAHttpRequestStateStarted)
        {
            [self cancelConnection];
        }
        state = PAHttpRequestStateCanceled;
        [self calculateRequestDuration];
        [[PARequestManager sharedInstance] removeRequest:self];
    }
}

- (void)finish
{   
    @synchronized(self)
    {
        [self calculateRequestDuration];
        if (![self isCanceled])
        {
            [self notifyFinishTarget];
        }
        [self releaseConnection];
    }    
    
    [[PARequestManager sharedInstance] removeRequest:self];
}

- (void)finishWithError:(NSError *)error
{
    @synchronized(self)
    {
        [self calculateRequestDuration];
        if (![self isCanceled])
        {
            [self notifyErrorTarget:error];
        }
        [self releaseConnection];    
    }    
    
    [[PARequestManager sharedInstance] removeRequest:self];
}

- (void)finishWithErrorMessage:(NSString *)message andErrorCode:(NSInteger)code
{
    NSError *error = [[PAHttpRequestError alloc] initWithRequest:self errorCode:code andMessage:message];
    [self finishWithError:error];
    [error release];
}

- (void)notifyFinishTarget
{
    [finishTarget performSelector:finishAction withObject:self];
}

- (void)notifyErrorTarget:(NSError *)error
{
    [errorTarget performSelector:errorAction withObject:self withObject:error];
}

- (void)notifyCancelTarget
{
    [cancelTarget performSelector:cancelAction withObject:self];
}

- (BOOL)isCanceled
{
    return state == PAHttpRequestStateCanceled;
}

- (void)setupDefaults
{
    [self setMethodGet];
}

- (void)setMethodGet
{
    method = @"GET";
}

- (void)setMethodPost
{
    method = @"POST";
}

- (void)setFinishTarget:(id)target andAction:(SEL)action
{
    finishTarget = [target retain];
    finishAction = action;
}

- (void)setErrorTarget:(id)target andAction:(SEL)action
{
    errorTarget = [target retain];
    errorAction = action;
}

- (void)setCancelTarget:(id)target andAction:(SEL)action
{
    cancelTarget = [target retain];
    cancelAction = action;
}

#pragma mark Request duration

- (void)setupRequestStartDate
{
    NSDate *now = [[NSDate alloc] init];
    self.startDate = now;
    [now release];
}

- (void)calculateRequestDuration
{
    duration = -[self.startDate timeIntervalSinceNow];
}

#pragma mark Connection

- (void)releaseConnection
{
    @synchronized(self)
    {
        [self.connection cancel];
        
        self.connection = nil;
        self.responseData = nil;
        self.userData = nil;
    }
}

- (void)cancelConnection
{
    @synchronized (self)
    {
        [self.connection cancel];    
        [self notifyCancelTarget];
    }
}

#pragma mark NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    responseCode = httpResponse.statusCode;
    
    if ([response isKindOfClass:[NSHTTPURLResponse class]])
    {
        if (responseCode != 200)
        {
            NSString *message = [[NSString alloc] initWithFormat:@"Unexpected status code: %d", responseCode];
            [self finishWithErrorMessage:message andErrorCode:PAHttpRequestErrorCodeUnexpectedHttpCode];
            [message release];
            return;
        }
    }
    [responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [self releaseConnection];
    [self finishWithError:error];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [self finish];
    [self releaseConnection];
}

#pragma mark Helpers

static NSString * URLEncodedStringFromString(NSString *string) 
{
    NSString *decodedString = [(NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault, (CFStringRef)string, CFSTR(""), kCFStringEncodingUTF8) autorelease];
    
    static NSString * const kAFLegalCharactersToBeEscaped = @"?!@#$^&%*+,:;='\"`<>()[]{}/\\|~ ";
    
	NSString *encodedString = [(NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)decodedString, NULL, (CFStringRef)kAFLegalCharactersToBeEscaped, CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding)) autorelease];
    
    return encodedString;
}

@end
