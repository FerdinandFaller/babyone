//
//  PAAdStatisticsController.m
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 21.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import "PAAdStatisticsController.h"

#import "PAAdStatisticsTableViewCell.h"

#import "PAAdStatistics.h"

@implementation PAAdStatisticsController

@synthesize clicksLabel;
@synthesize impressionsLabel;
@synthesize appIdLabel;

- (id)initWithAppId:(NSString *)aAppId
{
    self = [super initWithNibName:@"PAAdStatisticsController" bundle:nil];
    if (self) {
        appId = [aAppId copy];
    }
    return self;
}

- (void)dealloc
{
    [appId release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    PAAdStatistics *stat = [PAAdStatistics sharedInstance];
    
    self.appIdLabel.text = appId;
    self.clicksLabel.text = [NSString stringWithFormat:@"%d/%d", stat.clicks, stat.clicksFailed];
    self.impressionsLabel.text = [NSString stringWithFormat:@"%d/%d", stat.impressions, stat.impressionsFailed];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.clicksLabel = nil;
    self.impressionsLabel = nil;
    self.appIdLabel = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark Actions

- (IBAction)onCloseClicked:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark UITableViewDelegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 98;
}

#pragma mark UITableViewDataSource methods

- (NSString *)stringFromDuration:(NSTimeInterval)duration
{
    return duration == -1 ? @"???" : [NSString stringWithFormat:@"%f", duration];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *SimpleTableIdentifier = @"Cell";   
    PAAdStatisticsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SimpleTableIdentifier];
    if (cell == nil) {

        cell = [[[PAAdStatisticsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:SimpleTableIdentifier] autorelease];
    }
    NSUInteger row = [indexPath row];

    BannerRequestInfo *info = [[PAAdStatistics sharedInstance].requests objectAtIndex:row];
    cell.nameLabel.text = info.name;
    cell.bannerLoadLabel.text = [self stringFromDuration:info.responseDuration];
    cell.pageLoadLabel.text = [self stringFromDuration:info.pageStopWatch.duration];

    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [PAAdStatistics sharedInstance].requests.count;
}


@end
