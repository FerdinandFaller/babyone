//
//  PABannerCategorySimpleExtractor.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 3/25/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PABannerCategoryExtractor.h"

@interface PABannerCategorySimpleExtractor : PABannerCategoryExtractor
{
@private
    NSString* category;
}

@property (nonatomic, copy) NSString* category;

@end
