//
//  PABannerCategorySimpleExtractor.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 3/25/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PABannerCategorySimpleExtractor.h"

#import "PADebug.h"

#define kParamCategory  @"category"

@implementation PABannerCategorySimpleExtractor

@synthesize category;

- (id)initWithJson:(PAAdJsonHelper *)json
{
    self = [super initWithJson:json];
    if (self)
    {
        if (![PAAdJsonHelper allParamsArePresent:json, kParamCategory, nil])
        {
            PAAssertMsg(false, @"PABannerCategorySimpleExtractor is missing required params");
            
            [self release];
            return nil;
        }
        
        self.category = [json stringForKey:kParamCategory];
    }
    return self;
}

- (NSString *)tryExtractCategoryFromJSon:(PAAdJsonHelper *)json
{
    return self.category;
}

- (void)dealloc
{
    self.category = nil;
    [super dealloc];
}

@end
