//
//  PAHttpJSonRequest.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 1/14/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PAHttpJSonRequest.h"
#import "PAHttpRequest_Inheritance.h"

#import "PACJSONDeserializer.h"

@implementation PAHttpJSonRequest

@synthesize responseJson;

- (void)dealloc
{
    self.responseJson = nil;
    [super dealloc];
}

- (void)finish
{
    NSError *jsonError = NULL;
    
    id response = [[PACJSONDeserializer deserializer] deserialize:responseData error:&jsonError];
    if (response == nil || [response isKindOfClass:[NSNull class]])
    {
        NSString *message = [[NSString alloc] initWithFormat:@"Unable to parse json: %@", [jsonError localizedDescription]];
        [self finishWithErrorMessage:message andErrorCode:PAHttpRequestErrorCodeJSonParserFailed];
        [message release];
    }
    else
    {     
        self.responseJson = response;
        [super finish];
    }
}

- (void)releaseConnection
{
    [super releaseConnection];
    self.responseJson = nil;
}

@end
