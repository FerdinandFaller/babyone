//
//  PARequestManager.h
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 1/14/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PAHttpRequest.h"

@interface PARequestManager : NSObject
{
    NSMutableArray *requests;
}

+ (PARequestManager *)sharedInstance;

- (void)queueRequest:(PAHttpRequest *)request;
- (void)removeRequest:(PAHttpRequest *)request;

- (void)cancelAll;

@end
