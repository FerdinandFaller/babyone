//
//  PAImageBannerData.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 1/22/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PAButtonBannerData.h"

#import "PAAdButtonBasedAdView.h"

#define kPAAdResponseParamImageUrl  @"image_url"
#define kPAAdResponseParamClickUrl  @"click_url"

@implementation PAButtonBannerData

@synthesize imageUrl;
@synthesize clickUrl;

- (id)initWithJSon:(PAAdJsonHelper *)json andResponseData:(NSData *)data
{
    self = [super initWithJSon:json andResponseData:data];
    if (self)
    {
        if (![PAAdJsonHelper allParamsArePresent:json, kPAAdResponseParamImageUrl, kPAAdResponseParamClickUrl, nil])
        {
            [self release];
            return nil;
        }
        
        imageUrl = [[json stringForKey:kPAAdResponseParamImageUrl] copy];
        clickUrl = [[json stringForKey:kPAAdResponseParamClickUrl] copy];
    }
    return self;
}

- (PAAdCustomAdView *)createAdViewWithFrame:(CGRect)frame
{
    return [[[PAAdButtonBasedAdView alloc] initWithBannerData:self andFrame:frame] autorelease];
}

- (void)dealloc
{
    [imageUrl release];
    [clickUrl release];
    [super dealloc];
}

@end
