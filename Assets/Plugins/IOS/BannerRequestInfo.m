//
//  BannerRequestInfo.m
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 21.12.11.
//  Copyright (c) 2011 PressOK Entertainment. All rights reserved.
//

#import "BannerRequestInfo.h"

@implementation BannerRequestInfo

@synthesize name;
@synthesize responseDuration;
@synthesize pageStopWatch;

- (id)init
{
    self = [super init];
    if (self) 
    {        
        pageStopWatch = [[StopWatch alloc] init];
        requestState = PAAdBannerRequestInfoStateStarted;
    }
    return self;
}

- (void)setBannerFailed
{
    requestState = PAAdBannerRequestInfoStateBannerFailed;
}

- (void)setPageFailed
{
    requestState = PAAdBannerRequestInfoStatePageFailed;
}

- (void)setSucceed
{
    requestState = PAAdBannerRequestInfoStateSucceed;
}

- (void)dealloc
{
    self.name = nil;
    [pageStopWatch release];
    [super dealloc];
}

@end
