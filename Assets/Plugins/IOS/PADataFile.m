//
//  PATextFile.m
//  PlacePlayAdsDebugSample
//
//  Created by Alex Lementuev on 2/8/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PADataFile.h"

@interface PADataFile (Private)

- (NSString *)makePath:(NSString *)filename;

@end

@implementation PADataFile

- (id)initWithName:(NSString *)aName
{
    self = [super init];
    if (self)
    {
        NSString *aPath = [self makePath:aName];
        if (!aPath)
        {
            [self release];
            return nil;
        }
        
        path = [aPath copy];
        name = [aName copy];        
    }
    return self;
}

- (void)dealloc
{
    [name release];
    [path release];
    [super dealloc];
}

- (BOOL)writeData:(NSData *)data outError:(NSError **)error
{
    return [data writeToFile:path options:0 error:error];
}

- (NSData *)readData:(NSError **)error
{
    return [NSData dataWithContentsOfFile:path options:0 error:error];
}

- (BOOL)exists
{
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}

- (BOOL)deleteFile
{
    if ([self exists] && [[NSFileManager defaultManager] isDeletableFileAtPath:path])
    {
        return [[NSFileManager defaultManager] removeItemAtPath:path error:NULL];
    }
    return NO;
}

- (NSString *)makePath:(NSString *)filename
{
    NSArray *pathList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);    
    if (pathList.count > 0)
    {
        NSString *pathElement = [pathList objectAtIndex:0];    
        return [pathElement stringByAppendingPathComponent:filename];
    }
    
    return nil;
}


@end
