//
//  PABannerData.m
//  PlacePlayAdsSample
//
//  Created by Alex Lementuev on 1/20/12.
//  Copyright (c) 2012 PressOK Entertainment. All rights reserved.
//

#import "PABannerData.h"

#import "PADebug.h"
#import "PABannerCategoryExtractor.h"
#import "PABannerCategoryFactory.h"

#define kParamRequestId                     @"request_id"
#define kParamAdNetworkType                 @"ad_network_type"
#define kParamAdNetwork                     @"ad_network"
#define kParamTimeout                       @"timeout"
#define kParamAdNetworkNeedSecondaryClicks  @"need_secondary_clicks"
#define kParamCloseButtonTimeout            @"close_button_block_timeout"
#define kParamLandingVersion                @"landing_version"
#define kParamCacheResponse                 @"cache_response"
#define kParamCategoryExtractor             @"category_extractor"

@interface PABannerData (Private)

- (void)tryParseBannerCategory:(PAAdJsonHelper *)json;

@end

@implementation PABannerData

@synthesize requestId;
@synthesize networkType;
@synthesize networkId;
@synthesize timeout;
@synthesize needsSecondaryClicks;
@synthesize closeButtonTimeout;
@synthesize landingVersion;
@synthesize responseData;
@synthesize cacheResponse;
@synthesize category;

- (id)initWithJSon:(PAAdJsonHelper *)json andResponseData:(NSData *)data
{
    self = [super init];
    if (self)
    {
        if (![PAAdJsonHelper allParamsArePresent:json, kParamAdNetworkType, kParamAdNetwork, kParamTimeout, nil])
        {
            [self release];
            return nil;
        }
        
        requestId = [[json stringForKey:kParamRequestId def:nil] retain];
        networkType = [[json stringForKey:kParamAdNetworkType] retain];
        networkId = [[json stringForKey:kParamAdNetwork] retain];
        timeout = [json floatForKey:kParamTimeout];
        needsSecondaryClicks = [json boolForKey:kParamAdNetworkNeedSecondaryClicks def:NO];
        closeButtonTimeout = [json floatForKey:kParamCloseButtonTimeout];        
        landingVersion = [json intForKey:kParamLandingVersion];
        cacheResponse = [json boolForKey:kParamCacheResponse];
        
        [self tryParseBannerCategory:json];       
        
        if (landingVersion > 0)
        {
            self.responseData = data;
        }
    }
    return self;
}

- (void)dealloc
{
    [requestId release];
    [networkType release];
    [networkId release];
    [category release];
    self.responseData = nil;
    [super dealloc];
}

- (void)tryParseBannerCategory:(PAAdJsonHelper *)json
{
    PAAdJsonHelper* categoryExtractorJson = NULL;
    [json jsonForKey:kParamCategoryExtractor jsonObj:&categoryExtractorJson];
    
    if (categoryExtractorJson != nil)
    {
        PABannerCategoryExtractor* categoryExtractor = nil;
        [PABannerCategoryFactory tryParseCategoryExtractorFromJson:categoryExtractorJson pointer:&categoryExtractor];
        
        if (categoryExtractor != nil)
        {
            category = [[categoryExtractor tryExtractCategoryFromJSon:json] retain];
            if (category != nil)
            {
                PALogDebug(@"Banner's category is: %@", category);
            }
            else 
            {   
                PALogError(@"Unable to parse banner category");
            }
            
            [categoryExtractor release];
        }
        else 
        {
            PALogDebug(@"Unable to create category extractor from: %@", [categoryExtractorJson jsonObj]);                
        }
        [categoryExtractorJson release];
    } 
}

- (PAAdCustomAdView *)createAdViewWithFrame:(CGRect)frame
{
    PAAssertMsg(false, @"-(PAAdCustomAdView *)createAdViewWithFrame:(CGRect)frame should be overriden in subclass");
    return nil;
}

- (PALandingInfo *)landingInfo
{
    PAAssertMsg(false, @"-(PALandingCacheData *)landingInfo should be overriden is subclass");
    return nil;
}

@end
