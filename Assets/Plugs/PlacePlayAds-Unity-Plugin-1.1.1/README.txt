Using PlacePlay Ads in Unity
============================

PlacePlay Ads lets you incorporate higher paying location-enabled banner ads into iOS and Android games. Follow the instructions below to add it to your Unity project. You can also check out our sample project by selecting Open Project, then the "Unity Ads" folder.

Register Your Game
------------------

Before launching your PlacePlay enabled app, please email info@placeplay.com with your app name(s) and the platform(s) you are launching on (iOS, Android) and we will send you the required AppID.
To see test ads, use Game ID 0.

Add Plugin to Your Unity Project
--------------------------------
Drag and drop following files/folder in the "Project View" of Unity:

1. PlacePlayPlugin.cs
2. The Plugins folder

Integration
----------------------

1. Drag and drop PlacePlayPlugin.cs and the Plugins folder onto your Project tab
2. Drag and drop PlacePlayPlugin on the required component (ex. Main Camera)
3. Open the Inspector for the component used above and set the following parameters in the "Place Play Plugin (Script)" section.
4. Game ID: Enter the Game ID you received after signing up for the PlacePlay Ads service, or 0 to see test ads.
5. Anchor Tag: Enter a vertical and horizontal position anchor tag separated by "|" to change ad position on screen.
		
	* Vertical positions:
		* Top
		* VerCenter
		* Bottom	
	* Horizontal positions:
		* Left
		* HorCenter
		* Right
	
	For example:
		
		Top|Center
	
6. Ad Width and Height: Note, this is currently used only for Android. Set them to 320x50 for phones, and 768x90 for tablets. For iOS, 320x50 will automatically be used for iPhone and iPod, and 768x90 will be used for iPad.
7. Is Repeating: Select this if you want PlacePlay to automatically refresh ads at regular intervals.
8. Is Install Tracking enabled: Select this to get cross-promotion credit when your app is installed from an ad in another PlacePlay-enabled app.


Pause/Play of Ads
-----------------
If you want to remove the ads banner from the scene, call the following method on PlacePlayPlugin script object:
		
	pausePlacePlayService()

To resume ads serving and bring the banner view back on your scene, call:
		
	resumePlacePlayService()

Ads State Notifications
-----------------------
You can track the ads state changes with the following callback methods in PlacePlayPlugin script object:
		
	private void onPlacePlayAdsReceived()
	private void onPlacePlayAdsFailed()
	private void onPlacePlayFullscreenAdOpened()
	private void onPlacePlayFullscreenAdClosed()

In order not to lose any of your game progress, you should pause/resume it in the fullscreen callbacks methods:
		
	onPlacePlayFullscreenAdOpened
	onPlacePlayFullscreenAdClosed

PlacePlay App Cross Promotion
-----------------------------

We are currently adding FREE app cross promotion to PlacePlay to help you increase downloads of your app at no cost. We will also enable app developers to purchase additional installs. If you would like to take advantage of these new features, you must check "Is Install Track Enabled" in the "Inspector View" for the PlacePlayPlugin

Troubleshooting
---------------

**If the project does not run on iOS/Android emulator:** Please test the builds on actual devices instead of trying out on emulator. Emulators are not supported using Unity.

**If the iOS project created using Unity does not build. Throws a list of compilation error:** Please ensure following are done after the iOS project is created:

* Added "Message Framework"
* Code-sign using a valid provisioning profile
