
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Timers;

public delegate void PlacePlayNativeCallback();

public class PlacePlayPlugin : MonoBehaviour 
{
	private String imagePath;
	private Dictionary<String, PlacePlayNativeCallback> callbacks;
	
	public string gameID = "0";			//gameID provied by the server
	public string anchorTag = "Top|HorCenter";	
	public int adWidth = 320;			//width of the Ad
	public int adHeight = 50;			//height of the Ad
	public bool isRepeating = true;		//multiple Ad fecthing
	public bool isInstallTrackEnabled = true;
	
#if UNITY_ANDROID
	private AndroidJavaClass ppClass;
#endif
	
#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern void _GetPlacePlayAds(string gameid, float x, float y, float width, float height, bool repeat, string anchor, bool isTrackEnabled);
	[DllImport ("__Internal")]
	private static extern void _StartPlacePlayAds();
	[DllImport ("__Internal")]
	private static extern void _PausePlacePlayAds();
#endif
	
	void Start () 
	{		
		if (callbacks == null)
		{
			callbacks = new Dictionary<String, PlacePlayNativeCallback>();
			callbacks["com.placeplay.adreceived"]            = onPlacePlayAdsReceived;
			callbacks["com.placeplay.adfailed"]              = onPlacePlayAdsFailed;
			callbacks["com.placeplay.willpresentfullscreen"] = onPlacePlayFullscreenAdOpened;
			callbacks["com.placeplay.diddismissfullscreen"]  = onPlacePlayFullscreenAdClosed;
		}
		
		#if UNITY_EDITOR
			Debug.Log("WINDOWS EDITOR ");
		#endif
		
		#if UNITY_ANDROID
			ppClass = new AndroidJavaClass("com.paradigmcreatives.placeplayplugin.PlacePlayPlugin");		
			ppClass.CallStatic("displayAdLayouts", gameID, anchorTag, isRepeating, adWidth, adHeight, isInstallTrackEnabled);		
		#endif	
		
		#if UNITY_IPHONE
			getAdsFromPlacePlay(gameID,0,0,adWidth,adHeight,isRepeating,anchorTag,isInstallTrackEnabled);
		#endif
	}
	
	/* Support function to call iOS SDK of PlacePlay*/
	public void getAdsFromPlacePlay(string gameid, float x, float y, float width, float height, bool repeat, string anchor, bool isTrackEnabled)
	{
		// FIXME: I don't like this code
		if(adHeight != 0)
		{
			adHeight = (int)height;
		}
		
		if((int)width == 0)
		{
			adWidth = Screen.width;
			adHeight = (int)(adWidth/6.4);
		} 
		else 
		{
			adWidth = (int)width;
		}
		
		isRepeating = repeat;
		gameID = gameid;
		anchorTag = anchor;
		isInstallTrackEnabled = isTrackEnabled;
		
		#if UNITY_IPHONE
		if (Application.platform != RuntimePlatform.OSXEditor)
		{
			_GetPlacePlayAds(gameid,0,0,width,height,repeat,anchor,isTrackEnabled);
		}
		#endif
	}
	
	public void pausePlacePlayService()
	{
		#if UNITY_ANDROID
		if (ppClass != null)
		{
			ppClass.CallStatic("pauseAdService");
		}
		#endif

		#if UNITY_IPHONE
		if (Application.platform != RuntimePlatform.OSXEditor)
		{
			_PausePlacePlayAds();
		}
		#endif
	}

	public void resumePlacePlayService()
	{
		#if UNITY_ANDROID
		if (ppClass != null)
		{
			ppClass.CallStatic("resumeAdService");
		}
		#endif

		#if UNITY_IPHONE
		if (Application.platform != RuntimePlatform.OSXEditor)
		{
			_StartPlacePlayAds();
		}
		#endif
	}
	
	/* Native messages dispatcher */
	
	public void dispatchNativeMessage(String message)
	{
		PlacePlayNativeCallback callback = callbacks[message];
		if (callback != null)
		{
			callback();
		}
	}
	
	/* Delegate's callback methods */
	
	private void onPlacePlayAdsReceived()
	{
		Debug.Log("PlacePlay ad received");
	}
	
	private void onPlacePlayAdsFailed()
	{
		Debug.Log("PlacePlay ad failed");
	}
	
	private void onPlacePlayFullscreenAdOpened()
	{
		Debug.Log("PlacePlay opened fullscreen ad");
	}
	
	private void onPlacePlayFullscreenAdClosed()
	{
		Debug.Log("PlacePlay closed fullscreen ad");
	}
}
