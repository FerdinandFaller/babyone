using UnityEngine;

[RequireComponent(typeof(RageGroup))]
[AddComponentMenu("RageTools/Rage EdgeTune")]
[ExecuteInEditMode]
public class RageEdgetune : MonoBehaviour {

	public bool On, StartOnly;
	public GameObject TextMeshGO;
	public RageGroup Group;
	public RageEdgetuneData Data;

	public float StartObjectHeight, StartCameraDistance, StartOrthoSize = 100f, StartResolutionHeight;
	public TextMesh DebugTextMesh;
	public bool DebugDensity;

	private bool _initializeNow;
	private float _lastResolutionHeight, _lastZoffset, _lastObjectHeight, _lastOrthoSize;
	private Quaternion _lastObjectRotation = Quaternion.identity, 
		_lastCameraRotation = Quaternion.identity;
	private bool _changedResolutionHeight, _changedZOffset, _changedOrthoSize, _changedObjectHeight, _changedRotation;

	// Density and Anti-aliasing Factor-In Variables
	private float _dfScale = 1, _dfResolution = 1, _dfZoffset = 1, _dfOrthoSize = 1f;
	private float _aafResolution = 1, _aafZoffset = 1, _aafOrthoSize = 1;

	public void ScheduleInitialize() { _initializeNow = true; }

	public void Awake() {
		AddReferences();
	}

	private void AddReferences() {
		if (Group == null)
			Group = gameObject.GetComponent<RageGroup>() ?? gameObject.AddComponent<RageGroup>();
		if (Data == null)
			Data = ScriptableObject.CreateInstance<RageEdgetuneData>();
	}

	/// <summary> Assigns a rageGroup if needed, then finds the current game object and z distance (if Perspective camera) </summary>
	public void Initialize() {
		if(Group == null) {
			Group = gameObject.GetComponent<RageGroup>() 
				?? gameObject.AddComponent<RageGroup>();
		}

		StartObjectHeight = gameObject.transform.lossyScale.y;
		_lastObjectHeight = StartObjectHeight;
		_lastResolutionHeight = StartResolutionHeight = Data.PixelHeight;
		_lastOrthoSize = StartOrthoSize = Camera.mainCamera.orthographicSize;
		_lastZoffset = StartCameraDistance = OffsetFromCamera();
	}

	public void UpdatePixelHeight() {
		Data.PixelHeight = RageToolsExtensions.GetGameViewSize().y;
	}

	public void Start() {
		if (!On) return;
		if (!StartOnly) return;

		var updateStepBackup = Group.UpdateStep;
		Group.UpdateStep = 0;
		DoEdgeTune();
		Group.UpdateStep = updateStepBackup;
	}

	[ExecuteInEditMode]
	public void Update() {
		TestSnapShot();
		if (StartOnly) return;
		RefreshCheck();
	}

	public void RefreshCheck() {
		if (!On) return;

		_changedResolutionHeight = ChangedResolutionHeight(RageToolsExtensions.GetGameViewSize().y);
		_changedZOffset = ChangedZOffset();
		_changedOrthoSize = ChangedOrthoSize();
		_changedObjectHeight = ChangedObjectHeight();
		_changedRotation = ChangedRotation();

		bool mustRefresh = (_changedResolutionHeight || _changedZOffset || _changedOrthoSize || _changedObjectHeight || _changedRotation);
		if (!mustRefresh) return;
		DoEdgeTune();
	}

	#region Check for Valid Changes
	private bool ChangedResolutionHeight(float thisResolutionHeight) {
		if (Mathfx.Approximately (_lastResolutionHeight, thisResolutionHeight)) return false;
		_lastResolutionHeight = thisResolutionHeight;
		return true;
	}

	private bool ChangedZOffset() {
		if (Camera.mainCamera.isOrthoGraphic) return false;

		float thisZoffset = OffsetFromCamera();
		if (!Mathfx.Approximately(_lastZoffset,thisZoffset)) {
			_lastZoffset = thisZoffset;
			return true;
		}
		return false;
	}

	private bool ChangedOrthoSize() {
		if (!Camera.mainCamera.isOrthoGraphic) return false;

		float thisOrthoSize = Camera.mainCamera.orthographicSize;
		if (!Mathfx.Approximately(_lastOrthoSize, thisOrthoSize)) {
			_lastOrthoSize = thisOrthoSize;
			return true;
		}
		return false;
	}

	private bool ChangedObjectHeight(){
		if (Mathfx.Approximately (_lastObjectHeight, gameObject.transform.lossyScale.y)) return false;
		_lastObjectHeight = gameObject.transform.lossyScale.y;
		return true;
	}

	/// <summary> Checks if either the camera or the object have changed rotation since the last frame </summary>
	private bool ChangedRotation() {
		if (Mathf.Approximately(Data.PerspectiveBlur, 0f))  return false;
		bool changedObject = _lastObjectRotation != gameObject.transform.rotation;
		if (changedObject) _lastObjectRotation = gameObject.transform.rotation;
		bool changedCamera = _lastCameraRotation != Camera.mainCamera.transform.rotation;
		if (changedCamera) _lastCameraRotation = Camera.mainCamera.transform.rotation;
		return (changedObject || changedCamera);
	}
   #endregion

	public void DoEdgeTune () {
		CheckRageGroupAndList();
		CalculateAndUpdateAa();

		if (Data == null) Data = ScriptableObject.CreateInstance<RageEdgetuneData>();
		
		if (Data.AutomaticLod) 
			CalculateAndUpdateDensity();

		Group.QueueRefresh();
	}

	/// <summary> Keeps the AA "look" independent of resolution, shape size and offset from camera </summary>
	private float CalculateAndUpdateAa() {
		if (_changedResolutionHeight) _aafResolution = AaFactorInResolution();
		if (_changedZOffset) _aafZoffset = AaFactorInZOffset();
		if (_changedOrthoSize) _aafOrthoSize = AaFactorInOrthoSize();

		float aaAdjustFactor = _aafResolution * _aafZoffset * _aafOrthoSize;
		aaAdjustFactor = AaZoomOutAttenuation(aaAdjustFactor);
		aaAdjustFactor += AaFactorInAngleDelta();

		if (Data == null) Data = ScriptableObject.CreateInstance<RageEdgetuneData>();
		aaAdjustFactor = Mathf.Pow (aaAdjustFactor, Data.AaFactor);
		if (DebugTextMesh != null && !DebugDensity)
			DebugTextMesh.text = aaAdjustFactor+"";
		Group.List.UpdateAa(aaAdjustFactor, true, !Data.AutomaticLod);
		return aaAdjustFactor;
	}
	
	/// <summary> Keeps the apparent density independent of resolution, shape size and offset from camera </summary>
	private float CalculateAndUpdateDensity() {
		if (_changedObjectHeight) _dfScale = DensityFactorInScale();
		if (_changedResolutionHeight) _dfResolution = DensityFactorInResolution();
		if (_changedZOffset) _dfZoffset = DensityFactorInZOffset();
		if (_changedOrthoSize) _dfOrthoSize = DensityFactorInOrthoSize();
		float densityAdjustFactor = _dfScale * _dfZoffset * _dfResolution * _dfOrthoSize;
		densityAdjustFactor = DensityZoomOutAttenuation(densityAdjustFactor);

		if (DebugTextMesh != null && DebugDensity)
			DebugTextMesh.text = "Scl: "+_dfScale+"  Res: "+_dfResolution+"  Zoffs: "+_dfZoffset+"  Ortho: "+_dfOrthoSize ;
		Group.List.UpdateDensity(densityAdjustFactor, Data.MaxDensity);
		return densityAdjustFactor;
	}


	#region AA Factor Math
	private float AaFactorInResolution() {
		return StartResolutionHeight / RageToolsExtensions.GetGameViewSize().y;
	}

	private float AaFactorInZOffset() {
		 if (!Camera.mainCamera.isOrthoGraphic) return (OffsetFromCamera() / StartCameraDistance);
		return 1f;
	}

	private float AaFactorInOrthoSize() {
		 if (Camera.mainCamera.isOrthoGraphic) return (Camera.mainCamera.orthographicSize / StartOrthoSize);
		return 1f;
	}

	private float AaFactorInAngleDelta() {
		if (Mathf.Approximately(Data.PerspectiveBlur, 0f)) return 0f;
		Vector3 crossFowardVector = Vector3.Cross(Camera.mainCamera.transform.forward, transform.forward);
		var distanceOffset = Vector3.Distance(Vector3.zero, crossFowardVector);
		return Mathf.Lerp(0, Data.PerspectiveBlur, Mathf.Pow(distanceOffset, 10f));
	}

	private float AaZoomOutAttenuation(float factor) {
		if (factor <= 1.0f) return factor;
		if (factor >= 40f) return 40f; 
		//Debug.Log (1 + Mathf.Log (factor, 1.35f));
		return factor - Mathf.Pow(factor, 2f) / 75f;
	}

	private float DensityZoomOutAttenuation(float factor) {
		if (factor >= 1.0f) return factor;
		return Mathf.Pow(factor, 0.6f);
	}
   #endregion

   #region Density Factor Math
	private float DensityFactorInScale() {
		return 1 / Mathf.Pow (StartObjectHeight / Mathf.Abs(gameObject.transform.lossyScale.y), Data.DensityFactor);
	}

	private float DensityFactorInResolution() {
		return RageToolsExtensions.GetGameViewSize().y / StartResolutionHeight;
	}

	private float DensityFactorInZOffset() {
		if (Camera.mainCamera.isOrthoGraphic) return 1;
		return Mathf.Pow(StartCameraDistance / OffsetFromCamera(), Data.DensityFactor);
	}

	private float DensityFactorInOrthoSize( ) {
		if (! Camera.mainCamera.isOrthoGraphic) return 1;
		return StartOrthoSize / Camera.mainCamera.orthographicSize;
	}
   #endregion
	
	/// <summary> Calculates the distance from camera 
	/// depending on the X rotation (pitch) angle. </summary>
	private float OffsetFromCamera() {
		return (Vector3.Distance(gameObject.transform.position, Camera.mainCamera.transform.position));
	}

	private void CheckRageGroupAndList() {
		if(Group != null && Group.List != null && Group.List.Count != 0) return;
		Group = GetComponent<RageGroup>();
		Group.UpdatePathList();
	}

	private void TestSnapShot() {
		if (!_initializeNow) return;
		Initialize();
		_initializeNow = false;
	}

	public void GuessMaxDensity() {
		Data.MaxDensity = Group.GetMaxDensity() * 2;
	}
}
