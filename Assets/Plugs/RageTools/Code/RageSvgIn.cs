using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using Com.Freakow.RageTools.Commons;
using Com.Freakow.RageTools.Extensions;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections.Generic;
using System.Globalization;
using Object = UnityEngine.Object;

[AddComponentMenu("RageTools/Rage SVG In")]
public class RageSvgIn : MonoBehaviour {

	public char Separator = '/';
	public string Directory = "RageTools/Demo/SvgFiles";
	public Object SvgFile;

	public int MinVertexDensity = 3;
	public int MaxVertexDensity = 5;
	public float AntialiasWidth = 1f;
	public float ZsortOffset = -0.1f;
	public float MergeRadius = 0.01f;
	public bool ImportOnStart;
	public bool DebugMeshCreation;
	public bool DebugStyleCreation;
	public bool MidlineControls = true;
	public bool OutlineBehindFill;
	public bool AutoLayering;

	private int _currentGroupId;
	private int _currentPathId;
	private bool _adaptiveDensity;
	private RageSvgObject _current;        // Current RageObject - class with the gameobject, ragespline and current point index
	private RageSvgStyle _currentStyle;
	private RageSvgGradient _currentGradient;
	private string _styleString = "";
	private string _transformString = "";
	private bool _isLinear;
	private enum Poly { Line = 0, Gon };

	private Dictionary<string, RageSvgGradient> _gradients;
	private RageGroup _pathList;
	private Camera _sceneCamera;
	private TextMesh _debugTextMesh;
	public bool AutoLayeringGroup;
	public bool AutoLayeringMaterials;

	public void Start() {
		if(!ImportOnStart) return;
		ImportSvg();
	}

	/// <summary> Change the return line below if you need a different path for your project's SVG files.
	/// 'ds' is the directory separator, it's set to forward slash since we'll use a platform-independent www call
	/// </summary>
	public string AbsoluteSvgFilePath {
		get{
			if(SvgFile == null) return "";
			return AbsoluteDirectory + SvgFile.name + ".svg";
		}
	}

	public string AbsoluteDirectory {
		get {
			if(Directory == null) return "";
			return Application.dataPath + Separator + Directory + Separator;
		}
	}

	public string SvgFilePath {
		get {
			if(SvgFile == null) return "";
			return Directory + Separator + SvgFile.name + ".svg";
		}
	}

	public void FixPath() {
		if (SvgFile == null) return;
		Separator = Path.AltDirectorySeparatorChar;
#if UNITY_EDITOR
		var dirName = Path.GetDirectoryName (AssetDatabase.GetAssetPath (SvgFile));
#else
		string dirName = null;
#endif
		if (String.IsNullOrEmpty(dirName)) return;
		var delimiters = new[] { Separator };
		// Removes the initial 'Assets/' from the directory, if not at root level
		var folderParts = dirName.Split (delimiters, 2);
		if (folderParts.Length > 1)
			dirName = folderParts[1];
		else
			dirName = "";
		Directory = dirName;

		//Directory = Directory.Replace ("\\".ToCharArray (0, 1)[0], Separator);
		//Directory = Directory.Replace ("/".ToCharArray (0, 1)[0], Separator);
		if (!File.Exists(AbsoluteSvgFilePath)) return;
			
		return;
	}

	public void ImportSvg() {
		if (!Application.isEditor) return;

		if (SvgFile == null)
		{
			Debug.Log("RageTools: SVG File not set, canceling import.");
			return;
		}

		_current = RageSvgObject.NewInstance();
		_currentStyle = RageSvgStyle.NewInstance();
		_gradients = new Dictionary<string, RageSvgGradient>();
		_currentPathId = 0;

		_adaptiveDensity = (MinVertexDensity != MaxVertexDensity);
		if (_adaptiveDensity) _sceneCamera = Camera.main;

		//the starting parent is the current one, ie. the root
		_current.Parent = gameObject.transform;

		SvgLoad();

		// If it has a RageGroup attached, auto-update it after import
		_pathList = GetComponent<RageGroup>();
		if (_pathList != null) _pathList.UpdatePathList();
	}

	/// <summary> Loads the SVG file using a www call (to make it platform-agnostic) </summary>
	public void SvgLoad(){
		string path = "file://" + AbsoluteSvgFilePath;
		Debug.Log("RageSvgIn importing: " + path);

		var xml = new WWW(path);
		while(!xml.isDone)
			print(" Loading file...");

		XmlNode rootNode = RageXmlParser.XmlToDOM(xml.text);
		FindSvgRoot(rootNode);
		if (AutoLayering && AutoLayeringMaterials) {
			foreach (RageLayer layer in GetComponentsInChildren<RageLayer>())
				layer.SetMaterialRenderQueue();
			Debug.LogWarning ("RageTools: Please disregard error messages above.");
		}
		Debug.Log("RageSvgIn file imported!");
	}

	private void FindSvgRoot(XmlNode node) {
		foreach(XmlNode n in node.ChildNodes) {
			if(n.Name.Trim() == "svg") {
				ParseSvg(n, null, false);
				break;
			}
			foreach(XmlNode childNode in n.ChildNodes)
				FindSvgRoot(childNode);
		}
	}

	/// <summary> SVG XML node parser. svgEntry can be: 'path', 'rect', 'line', etc </summary>
	/// <param name="svgNode">XML node to start parsing from</param>
	/// <param name="groupStyle">If null, starts a fresh new style</param>
	/// <param name="subPath"></param>
	private void ParseSvg(XmlNode svgNode, RageSvgStyle groupStyle, bool subPath) {
		if(svgNode == null) {
			Debug.Log("Error: Invalid SVG Node");
			return;
		}
		if(DebugMeshCreation)
			Debug.Log(svgNode.Name.Trim() + " parse start: \n=================");

		if(groupStyle == null)
			_currentStyle = RageSvgStyle.NewInstance();

		switch(svgNode.Name.Trim()) {                       // Starts parsing the valid commands' attributes
			case ("g"):
				ParseGroup(svgNode, subPath);
				break;
			//case ("defs"):
			//    ParseChildNodes(svgNode);
			//    break;
			case ("linearGradient"):
				ParseGradient(svgNode, RageSvgGradient.GradientType.Linear, subPath);
				break;
			case ("radialGradient"):
				ParseGradient(svgNode, RageSvgGradient.GradientType.Radial, subPath);
				break;
			case ("stop"):
				ParseGradientStop(svgNode);
				break;
			case ("rect"):
				ParseRect(svgNode, subPath);
				break;
			case ("circle"):
				ParseCircle(svgNode, subPath);
				break;
			case ("ellipse"):
				ParseEllipse(svgNode, subPath);
				break;
			case ("line"):
				ParseLine(svgNode, subPath);
				break;
			case ("polyline"):
				ParsePolyLine(svgNode, Poly.Line, subPath);
				break;
			case ("polygon"):
				ParsePolyLine(svgNode, Poly.Gon, subPath);
				break;
			case ("path"):
				ParsePath(svgNode, subPath, false);
				break;
		}

		// if some style was found in the svgEntry declaration, apply it
		if(!String.IsNullOrEmpty(_styleString)) {
			if(DebugStyleCreation)
				Debug.Log("svgStyle String: " + _styleString);
			if(_current.Spline != null)
				ApplyRageStyle();
		}

		// Prevents double-parsing if it's a group or gradient node
		if(svgNode.Name.Trim() == "g" ||
			svgNode.Name.Trim() == "linearGradient" ||
			svgNode.Name.Trim() == "radialGradient" ||
			svgNode.Name.Trim() == "pattern" )
			return;

		// Recurses through child nodes
		if(svgNode.ChildNodes.Count > 0) {
			foreach(XmlNode svgEntry in svgNode.ChildNodes)
				ParseSvg(svgEntry, groupStyle, subPath);
		}
	}

	private void ParseGroup(XmlNode svgEntry, bool subPath) {
		var origParent = _current.Parent;                       // Accounts for Nested nodes
		var hideGroup = false;
		var groupTransformString = "";

		_current.Parent = CreateGameObject(_current.Parent, true, false, false);   // All contained nodes will be parented to this guy
		var currentGroup = _current.Parent.gameObject;          // Stores the current Style to allow proper style nesting

		var groupStyle = RageSvgStyle.NewInstance();
		_styleString = "";

		XmlAttributeCollection attributes = svgEntry.Attributes;
		if(attributes != null)
			foreach(XmlAttribute svgAttribute in attributes) {
				string svgCommand = svgAttribute.Name;
				string svgValue = svgAttribute.Value;
				if(ParseSvgStyle(svgCommand, svgValue))
					continue;

				switch(svgCommand) {
					case ("id"):
						_current.Object.name = svgValue;
						break;
					case ("display"):
						if(svgValue == "none")
							hideGroup = true;
						break;
					case ("transform"):
						groupTransformString = svgValue;
						break;
				}
			}

		_currentStyle.IsFilled = false;
		ApplyRageStyle();
		groupStyle.CopyDataFrom(_currentStyle);

		if(DebugMeshCreation)
			Debug.Log("######### Group children Count: " + svgEntry.ChildNodes.Count);

		if(svgEntry.ChildNodes.Count != 0) {
			foreach(XmlNode childNode in svgEntry.ChildNodes) {
				ParseSvg(childNode, groupStyle, subPath);
				_currentStyle.CopyDataFrom(groupStyle);             // Restores the group style (safe check)
			}
		}
		if(DebugMeshCreation)                              // Only Apply transformations after creating child objects
			Debug.Log("\tGroup transform: " + groupTransformString + " :: Group = " + currentGroup.name);
		ApplyTransform(currentGroup, groupTransformString);
		_transformString = "";

		if(hideGroup) {                                    // If needed, hide the Group and its descendants
			var transforms = currentGroup.GetComponentsInChildren<Transform>();
			foreach(Transform thisTransform in transforms)
				thisTransform.gameObject.active = false;
			currentGroup.active = false;
		}
		_current.Parent = origParent;                       // Restores the default parent to the one prior to this entry
	}

	private void ParseGradient(XmlNode svgEntry, RageSvgGradient.GradientType gradientType, bool subPath) {
		// Initializes a new Gradient
		_currentGradient = RageSvgGradient.NewInstance();
		_currentGradient.Type = gradientType;
		float radius = 0f;
		var gradientTransformString = "";

		XmlAttributeCollection attributes = svgEntry.Attributes;
		if(attributes != null)
			foreach(XmlAttribute svgAttribute in attributes) {
				string svgCommand = svgAttribute.Name;
				string svgValue = svgAttribute.Value;
				// Parse valid Svg style commands, if any found, iterate
				// if (ParseSvgStyle(svgCommand, svgValue)) continue;

				switch(svgCommand) {
					//<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="170.0791" y1="85.0396" x2="170.0791" y2="28.3472">
					case ("id"):
						_currentGradient.Id = svgValue;
						break;
					case ("xlink:href"):
						ParseGradientLink(ref _currentGradient, svgValue.Substring(1));
						break;
					case ("x1"):
						_currentGradient.X1 = svgValue.SvgToFloat();
						break;
					case ("y1"):
						_currentGradient.Y1 = svgValue.SvgToFloat();
						break;
					case ("x2"):
						_currentGradient.X2 = svgValue.SvgToFloat();
						break;
					case ("y2"):
						_currentGradient.Y2 = svgValue.SvgToFloat();
						break;
					case ("cx"):
						_currentGradient.X2 = svgValue.SvgToFloat();
						break;
					case ("cy"):
						_currentGradient.Y2 = svgValue.SvgToFloat();
						break;
					case ("fx"):
						_currentGradient.X1 = svgValue.SvgToFloat();
						break;
					case ("fy"):
						_currentGradient.Y1 = svgValue.SvgToFloat();
						break;
					case ("r"):
						radius = svgValue.SvgToFloat();
						break;
					case ("gradientTransform"):
						gradientTransformString = svgValue;
						break;
				}
			}

		if(!(Mathfx.Approximately(radius,0f))) {
			_currentGradient.X2 = _currentGradient.X1 + radius;
			_currentGradient.Y2 = _currentGradient.Y2 + radius;
		}

		if(DebugStyleCreation)
			Debug.Log("######### Gradient children Count: " + svgEntry.ChildNodes.Count);

		if(svgEntry.ChildNodes.Count != 0) {
			foreach(XmlNode childNode in svgEntry.ChildNodes)
				ParseSvg(childNode, _currentStyle, subPath);
		}

		ApplyGradientTransform(gradientTransformString);

		if(_gradients.ContainsKey(_currentGradient.Id))
			return;

		// After all "stop" child nodes are parsed, actually add the new dictionary item
		if(DebugStyleCreation)
			Debug.Log("Gradient entry added: " + _currentGradient.Id);

		_gradients.Add(_currentGradient.Id, _currentGradient);
	}

	private void ParseGradientLink(ref RageSvgGradient currentGradient, string gradientKey) {
		RageSvgGradient refGradient;
		if(_gradients.TryGetValue(gradientKey, out refGradient)) {
			currentGradient.CopyDataFrom(refGradient);
		}
	}

	private void ParseGradientStop(XmlNode svgEntry) {
		var offset = "";
		var stopColor = "";
		var stopOpacity = "";

		XmlAttributeCollection attributes = svgEntry.Attributes;
		if(attributes != null)
			foreach(XmlAttribute svgAttribute in attributes) {
				string svgCommand = svgAttribute.Name;
				string svgValue = svgAttribute.Value;

				if(DebugStyleCreation)
					Debug.Log("Gradient Stop: " + svgCommand + " " + svgValue);

				switch(svgCommand) {
					//<stop  offset="0" style="stop-color:#FFFFFF;stop-opacity:1"/>
					case ("offset"):
						offset = svgValue;
						break;
					case ("stop-color"):
						stopColor = svgValue.Substring(1);
						break;
					case ("stop-opacity"):
						stopOpacity = svgValue;
						break;
					case ("style"):
						var gradientStyleString = svgValue;

						foreach(var style in GetSvgStyleCommands(gradientStyleString)) {
							switch(style.Command) {
								case ("stop-color"):
									stopColor = style.Value.Substring(1);
									break;
								case ("stop-opacity"):
									stopOpacity = style.Value;
									break;
							}
						}
						break;
				}
			}
		var thisColor = stopColor.ToColor();
		if(stopOpacity != "")
			thisColor.a = float.Parse(stopOpacity);

		//SVG gradient stops are defined in reverse order.
		// TODO: Reduce the gradient radius according to the valid offsets 'delta'
		if(float.Parse(offset) < 0.5f)
			_currentGradient.EndColor = thisColor;
		else
			_currentGradient.StartColor = thisColor;
	}

	private void ParseLine(XmlNode svgEntry, bool subPath) {
		// Initializes a new Style string
		_styleString = "";
		//_currentStyle.Outline = RageSpline.Outline.Free;
		_isLinear = true;

		float x2, y1, y2;
		float x1 = x2 = y1 = y2 = 0f;
		string toPath;

		if(svgEntry.Attributes != null)
			foreach(XmlAttribute svgAttribute in svgEntry.Attributes) {
				string svgCommand = svgAttribute.Name;
				string svgValue = svgAttribute.Value;

				if(ParseSvgStyle(svgCommand, svgValue))
					continue;

				switch(svgCommand) {
					// <line x1="100" y1="300" x2="300" y2="100" stroke-width="5"/>
					case ("x1"):
						x1 = svgValue.SvgToFloat();
						break;
					case ("y1"):
						y1 = svgValue.SvgToFloat();
						break;
					case ("x2"):
						x2 = svgValue.SvgToFloat();
						break;
					case ("y2"):
						y2 = svgValue.SvgToFloat();
						break;
					case ("transform"):
						_transformString = svgValue;
						break;
					case ("id"):
						_current.name = svgValue;
						break;
				}
			}

		if(_styleString == "")
			toPath = "<path d=\"";
		else
			toPath = "<path style=\"" + _styleString + "\" d=\"";

		toPath += "M " + x1.ToString() + "," + y1.ToString() + " ";
		toPath += "L " + x2.ToString() + "," + y2.ToString() + "\"/>";

		if(DebugMeshCreation)
			Debug.Log("svgLineToPath: " + toPath);
		XmlNode node = RageXmlParser.XmlToDOM(toPath);
		_currentStyle.CornersType = Spline.CornerType.Beak;
		ParsePath(node.ChildNodes[0], subPath, true);
	}

	private void ParsePolyLine(XmlNode svgEntry, Poly polyType, bool subPath) {
		// Initializes a new Style string
		_styleString = "";

		_currentStyle.CornersType = Spline.CornerType.Beak;
		_isLinear = true;

		string points = "";
		string toPath;

		if(svgEntry.Attributes != null)
			foreach(XmlAttribute svgAttribute in svgEntry.Attributes) {
				string svgCommand = svgAttribute.Name;
				string svgValue = svgAttribute.Value;

				if(ParseSvgStyle(svgCommand, svgValue))
					continue;
				// <polygon fill="lime" points="850,75 958,137.5 958,262.5 850,325" />
				switch(svgCommand) {
					case ("points"):
						points = svgValue;
						break;
					case ("transform"):
						_transformString = svgValue;
						break;
					case ("id"):
						_current.name = svgValue;
						break;
				}
			}

		if(_styleString == "")
			toPath = "<path d=\"";
		else
			toPath = "<path style=\"" + _styleString + "\" d=\"";

		toPath += "M " + points;
		if(polyType == Poly.Line)
			toPath += "\"/>";
		else
			toPath += " z\"/>";

		if(DebugMeshCreation)
			Debug.Log("svgPolygon: " + toPath);

		XmlNode node = RageXmlParser.XmlToDOM(toPath);
		_currentStyle.CornersType = Spline.CornerType.Beak;
		ParsePath(node.ChildNodes[0], subPath, true);
	}

	private void ParseRect(XmlNode svgEntry, bool subPath) {
		// Initializes a new Style string
		_styleString = "";
		//_currentStyle.Outline = RageSpline.Outline.None;
		_currentStyle.CornersType = Spline.CornerType.Beak;
		_isLinear = true;

		//var thisParent = parent;
		float y, width, height;
		float x = y = width = height = 0f;
		string svgRectToPath;

		if(svgEntry.Attributes != null)
			foreach(XmlAttribute svgAttribute in svgEntry.Attributes) {
				string svgCommand = svgAttribute.Name;
				string svgValue = svgAttribute.Value;

				if(ParseSvgStyle(svgCommand, svgValue))
					continue;

				switch(svgCommand) {
					// x="21.778" y="22.11" fill="none" stroke="#000000"  width="100" height="100"
					case ("x"):
						x = svgValue.SvgToFloat();
						break;
					case ("y"):
						y = svgValue.SvgToFloat();
						break;
					case ("width"):
						width = svgValue.SvgToFloat();
						break;
					case ("height"):
						height = svgValue.SvgToFloat();
						break;
					case ("transform"):
						_transformString = svgValue;
						break;
					case ("id"):
						_current.name = svgValue;
						break;
				}
			}

		if(Mathf.Approximately(width, 0f) || Mathf.Approximately(height, 0f))
			return;
		//  Eg.:     d="m 10,12.362183 c 0,0 11,18 10,10 -2,-2 10,10 10,10 l -20,0 z"
		if(_styleString == "")
			svgRectToPath = "<path d=\"";
		else
			svgRectToPath = "<path style=\"" + _styleString + "\" d=\"";
		//* perform an absolute moveto operation to location (x+rx,y), where x is the value of the �rect� element's �x� attribute converted to user space, rx is the effective value of the �rx� attribute converted to user space and y is the value of the �y� attribute converted to user space
		svgRectToPath += "M " + x.ToString() + "," + y.ToString() + " ";

		//* perform an absolute horizontal lineto operation to location (x+width-rx,y), where width is the �rect� element's �width� attribute converted to user space
		svgRectToPath += "L " + (x + width).ToString() + "," + y.ToString() + " ";

		//* perform an absolute elliptical arc operation to coordinate (x+width,y+ry), where the effective values for the �rx� and �ry� attributes on the �rect� element converted to user space are used as the rx and ry attributes on the elliptical arc command, respectively, the x-axis-rotation is set to zero, the large-arc-flag is set to zero, and the sweep-flag is set to one
		//* perform a absolute vertical lineto to location (x+width,y+height-ry), where height is the �rect� element's �height� attribute converted to user space
		svgRectToPath += "L " + (x + width).ToString() + "," + (y + height).ToString() + " ";

		//* perform an absolute elliptical arc operation to coordinate (x+width-rx,y+height)
		//* perform an absolute horizontal lineto to location (x+rx,y+height)
		svgRectToPath += "L " + x.ToString() + "," + (y + height).ToString() + " ";

		//* perform an absolute elliptical arc operation to coordinate (x,y+height-ry)
		//* perform an absolute absolute vertical lineto to location (x,y+ry)
		svgRectToPath += "L " + x.ToString() + "," + y.ToString() + " ";

		//* perform an absolute elliptical arc operation to coordinate (x+rx,y)
		svgRectToPath += " z\" />";

		if(DebugMeshCreation)
			Debug.Log("svgRectToPath: " + svgRectToPath);

		XmlNode node = RageXmlParser.XmlToDOM(svgRectToPath);
		_currentStyle.CornersType = Spline.CornerType.Beak;
		ParsePath(node.ChildNodes[0], subPath, true);

	}

	private void ParseCircle(XmlNode svgEntry, bool subPath) {
		_styleString = "";
		_currentStyle.CornersType = Spline.CornerType.Default;

		float cy, r;
		float cx = cy = r = 0f;
		string svgCircleToPath;

		if(svgEntry.Attributes != null)
			foreach(XmlAttribute svgAttribute in svgEntry.Attributes) {
				string svgCommand = svgAttribute.Name;
				string svgValue = svgAttribute.Value;

				if(ParseSvgStyle(svgCommand, svgValue))
					continue;

				switch(svgCommand) {
					// <circle fill="none" stroke="#000000" stroke-miterlimit="10" cx="100" cy="100" r="50"/>
					case ("cx"):
						cx = svgValue.SvgToFloat();
						break;
					case ("cy"):
						cy = svgValue.SvgToFloat();
						break;
					case ("r"):
						r = svgValue.SvgToFloat();
						break;
					case ("transform"):
						_transformString = svgValue;
						break;
					case ("id"):
						_current.name = svgValue;
						break;
				} // switch (svgCommand)
			}

		if(Mathf.Approximately(r, 0f))
			return;

		if(_styleString == "")
			svgCircleToPath = "<path d=\"";
		else
			svgCircleToPath = "<path style=\"" + _styleString + "\" d=\"";
		//tangent length: http://en.wikipedia.org/wiki/File:Circle_and_cubic_bezier.svg
		var tanlen = (float)(0.5522847498307934 * r);

		//* top point = (cx, cy+r) 	//eg(100, 50) 
		//        svgCircleToPath += "m " + cx.ToString() + "," + (cy+r).ToString();

		//* <bottom> point = (cx-tanlen, cy+r ,cx, cy+r) 	//eg(100, 50) 
		svgCircleToPath += " S " + (cx - tanlen).ToString() + "," + (cy + r).ToString() + " " + cx.ToString() + "," + (cy + r).ToString();

		//smooth curve syntax: Sx1,y1 x,y
		//right point = (cx+r, cy+tanlen, cx+r, cy) 	//eg(150, 100)
		svgCircleToPath += "S " + (cx + r).ToString() + "," + (cy + tanlen).ToString() + " " + (cx + r).ToString() + "," + cy.ToString();

		// <top> (reflected) point = (cx+tanlen, cy-r, cx, cy-r)	//eg(100, 150)
		svgCircleToPath += " S " + (cx + tanlen).ToString() + "," + (cy - r).ToString() + " " + cx.ToString() + "," + (cy - r).ToString();

		// left point = (cx-r, cy-tanlen, cx-r, cy) 	//eg(50, 100)
		svgCircleToPath += " S " + (cx - r).ToString() + "," + (cy - tanlen).ToString() + " " + (cx - r).ToString() + "," + cy.ToString();

		//* closing <bottom> point = (cx-tanlen, cy+r ,cx, cy+r) 	//eg(100, 50) 
		svgCircleToPath += " S " + (cx - tanlen).ToString() + "," + (cy + r).ToString() + " " + cx.ToString() + "," + (cy + r).ToString();

		svgCircleToPath += " z\" />";

		if(DebugMeshCreation)
			Debug.Log("svgCircleToPath: " + svgCircleToPath);

		XmlNode node = RageXmlParser.XmlToDOM(svgCircleToPath);
		_currentStyle.CornersType = Spline.CornerType.Beak;
		
		ParsePath(node.ChildNodes[0], subPath, true);
	}

	private void ParseEllipse(XmlNode svgEntry, bool subPath) {
		// Initializes a new Style string
		_styleString = "";
		_currentStyle.CornersType = Spline.CornerType.Default;

		float cy, rx, ry;
		float cx = cy = rx = ry = 0f;
		string svgEllipseToPath;

		if(svgEntry.Attributes != null)
			foreach(XmlAttribute svgAttribute in svgEntry.Attributes) {
				string svgCommand = svgAttribute.Name;
				string svgValue = svgAttribute.Value;

				if(ParseSvgStyle(svgCommand, svgValue))
					continue;

				switch(svgCommand) {
					//   eg.: <ellipse cx="40" cy="40" rx="30" ry="15" style="stroke:#006600; fill:#00cc00"/>
					case ("cx"):
						cx = svgValue.SvgToFloat();
						break;
					case ("cy"):
						cy = svgValue.SvgToFloat();
						break;
					case ("rx"):
						rx = svgValue.SvgToFloat();
						break;
					case ("ry"):
						ry = svgValue.SvgToFloat();
						break;
					case ("transform"):
						_transformString = svgValue;
						break;
					case ("id"):
						_current.name = svgValue;
						break;
				}
			}

		// Any zero-radius disables the rendering of the element        );
		if(Mathf.Approximately(rx, 0f) || Mathf.Approximately(ry, 0f))
			return;

		if(_styleString == "") svgEllipseToPath = "<path d=\"";
		else svgEllipseToPath = "<path style=\"" + _styleString + "\" d=\"";
		//tangent length: http://en.wikipedia.org/wiki/File:Circle_and_cubic_bezier.svg
		var tanLenX = (float)(0.5522847498307934 * rx);
		var tanLenY = (float)(0.5522847498307934 * ry);

		//* top point = (cx, cy+r) 	//eg(100, 50) 
		//        svgCircleToPath += "m " + cx.ToString() + "," + (cy+ry).ToString();

		//* <bottom> point = (cx-tanLenX, cy+ry ,cx, cy+ry) 	//eg(100, 50) 
		svgEllipseToPath += " S " + (cx - tanLenX).ToString() + "," + (cy + ry).ToString() + " " +
							cx.ToString() + "," + (cy + ry).ToString();
		//right point = (cx+rx, cy+tanLenY, cx+rx, cy) 	//eg(150, 100)
		svgEllipseToPath += "S " + (cx + rx).ToString() + "," + (cy + tanLenY).ToString() + " " +
							(cx + rx).ToString() + "," + cy.ToString();
		// <top> (reflected) point = (cx+tanLenX, cy-ry, cx, cy-ry)	//eg(100, 150)
		svgEllipseToPath += " S " + (cx + tanLenX).ToString() + "," + (cy - ry).ToString() + " " +
							cx.ToString() + "," + (cy - ry).ToString();
		// left point = (cx-rx, cy-tanLenY, cx-rx, cy) 	//eg(50, 100)
		svgEllipseToPath += " S " + (cx - rx).ToString() + "," + (cy - tanLenY).ToString() + " " +
							(cx - rx).ToString() + "," + cy.ToString();
		//* closing <bottom> point = (cx-tanlen, cy+ry ,cx, cy+ry) 	//eg(100, 50) 
		svgEllipseToPath += " S " + (cx - tanLenX).ToString() + "," + (cy + ry).ToString() + " " +
							cx.ToString() + "," + (cy + ry).ToString();

		svgEllipseToPath += " z\" />";

		if(DebugMeshCreation)
			Debug.Log("svgCircleToPath: " + svgEllipseToPath);

		XmlNode node = RageXmlParser.XmlToDOM(svgEllipseToPath);
		_currentStyle.CornersType = Spline.CornerType.Beak;
		ParsePath(node.ChildNodes[0], subPath, true);
	}

	/// <summary> SVG Path parsing main function. All RageSplines are created from Path instructions. </summary>
	/// <param name="svgEntry">SVG Text string</param>
	/// <param name="subPath"> </param>
	/// <param name="derivedPath"> </param>
	private void ParsePath(XmlNode svgEntry, bool subPath, bool derivedPath) {
		CreateGameObject(_current.Parent, false, subPath, derivedPath);
		if (!derivedPath) _transformString = "";

		if(svgEntry.Attributes != null)
			foreach(XmlAttribute svgAttribute in svgEntry.Attributes) {
				string svgCommand = svgAttribute.Name;
				string svgValue = svgAttribute.Value;

				if(ParseSvgStyle(svgCommand, svgValue))
					continue;

				switch(svgCommand) {
					case ("id"):
						_current.Object.name = svgValue;
						break;
					case ("transform"):
						_transformString = svgValue;
						break;
					// if it's a Draw entry, split and treat the commandsets
					case ("d"): {
							bool finalized = false;	// Tells if the path command was truncated or not
							bool firstPathCommand = true; 
							Transform compoundPathRoot = _current.Object.transform;
							foreach(var commandSet in SplitCommands(svgAttribute.Value.TrimEnd(null))) {
								if(DebugMeshCreation)
									Debug.Log("commandset: " + commandSet);
								var coords = new List<float> (ParseCoordinates(commandSet.Trim()));
								var command = commandSet[0];
								var isRelative = char.IsLower(command);

								switch(command) {
									//      moveto (relative)
									case 'm':
									case 'M':
										if (firstPathCommand) {
											MoveTo(coords, isRelative, false, false);
											firstPathCommand = false;
											break;
										}
										ApplyRageStyle();
										if(DebugMeshCreation)
											Debug.Log("\t ### Starting Sub-Path");
										_currentStyle.IsFilled = false;
										if (!finalized) FinalizePath();
										finalized = false;
										CreateGameObject(compoundPathRoot, false, true, false);
										MoveTo(coords, isRelative, false, false);
										break;
									//      lineto (relative)
									case 'l':
									case 'L':
										MoveTo(coords, isRelative, false, false);
										break;
									case 'h':
									case 'H':
										MoveTo(coords, isRelative, false, true);
										break;
									case 'v':
									case 'V':
										MoveTo(coords, isRelative, true, false);
										break;
									case 'c':
									case 'C':
										CurveTo(coords, isRelative);
										break;
									case 's':
									case 'S':
										//      rageSpline.AddPoint(index, vPos, vOutCtrl)
										SmoothCurveTo(coords, isRelative);
										break;
									//      Close Path, generally on end (if followed by M, next subpath starts there)
									case 'z':
									case 'Z':
										_currentStyle.IsFilled = true;
										_currentStyle.IsClosed = true;
										FinalizePath();
										finalized = true;
										//subPath = true; //		Sets to start new subPath in case a 'move' command follows up 
										break;
								}
							}
							if (!finalized) {
								FinalizePath();
							}
					}
					break; //>> endof case (d)
				}
			}   //>> endof foreach (var svgAttribute in svgEntry.Attributes)
	}

	private void FinalizePath() {
		if (_current.Spline.PointsCount == 0) {
			if (DebugMeshCreation)
				Debug.Log ("Parsing Error: Path has no valid points");
			return;
		}
		// Render outlines behind (not-compatible with emboss) or not
		_current.Spline.OutlineBehindFill = OutlineBehindFill;
		ApplyRageStyle();
		ApplyTransform(_current.Object, _transformString);
		_transformString = "";
		_current.Spline.Rs.MergeStartEndPoints(DebugMeshCreation);
		// Merge nearby points according to the user-defined radius
		if (MergeRadius > 0f && _current.Spline.Rs.GetPointCount() > 3) {
			bool hasMerged = false;
			do {
				int pointCount = _current.Spline.Rs.GetPointCount();
				int i = 0; // Skips the start point (processed by MergeStartEndPoints)
				while (i < pointCount) {
					hasMerged = _current.Spline.Rs.RemoveOverlap (i - 1, i, MergeRadius, DebugMeshCreation);
					i++;
				}
			} while (hasMerged && _current.Spline.Rs.GetPointCount() > 3);
		}
		_current.Spline.Rs.RefreshMeshInEditor(true, true, true);
	}

	private bool ParseSvgStyle(string svgCommand, string svgValue) {
		switch(svgCommand) {
			// TODO: Add style id to the class and store it
			//case ("id"):
			//    _current.Object.name = svgValue;
			//    return true;
			case ("opacity"):
			case ("stroke-miterlimit"):
			case ("stroke-linejoin"):
			case ("stroke"):
			case ("stroke-opacity"):
			case ("stroke-width"):
			case ("stroke-linecap"):
			case ("fill"):
			case ("fill-opacity"):
				_styleString += svgCommand + ":" + svgValue + ";";
				return true;
			// style="fill:none;stroke:#FF0000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
			case ("style"):
				if(_styleString != "")
					if(_styleString[_styleString.Length - 1] != ';')
						_styleString += ";";
				_styleString += svgValue;
				return true;
		}
		return false;
	}

	/// <summary> Responsible for actually creating the RageSpline-containing Game Objects </summary>
	/// <param name="parentTransform">This will be the parent of the new Game Object</param>
	/// <param name="isGroup">Is this new Game Object a Group?</param>
	/// <param name="subPath"> </param>
	/// <param name="derivedPath"> </param>
	/// <returns>The Transform of new Game Object</returns>
	private Transform CreateGameObject(Transform parentTransform, bool isGroup, bool subPath, bool derivedPath) {
		_current.Object = new GameObject ();

		string newName = "<Path" + _currentPathId + ">";
		if (derivedPath)
			if (_current.name != "") {
				_current.Object.name = _current.name;
				_current.name = "";	 
			} else
				_current.Object.name = newName;
		else
			_current.Object.name = newName;

		// Actually parent the object
		_current.Object.transform.parent = parentTransform;

		if(isGroup) {
			_current.Spline = null;
			if (AutoLayeringGroup) _currentGroupId++;
			return _current.Object.transform;
		}

		SetZordering(_current.Object);

		//Initializes a new RageSpline Object
		_current.Spline = Spline.Add(_current.Object);
		if(_current.Spline == null)
			return _current.Object.transform;

		_current.Spline.RemoveAllPoints();
		_current.Spline.Outline.AntialiasingWidth = AntialiasWidth;

		_current.PointIdx = 0;
		// According to the SVG spec, the first move command is always absolute
		if(!subPath)
			_current.CursorPos = new Vector3(0f, 0f);

		return _current.Object.transform;
	}

	private void SetZordering(GameObject gO) {
		if (!AutoLayering) { 
			gO.transform.position = new Vector3 (_current.Object.transform.position.x,
		                                                  _current.Object.transform.position.y,
		                                                  NewZOffset());
			return;
		}
		var rageLayer = gO.AddComponent<RageLayer>();
		if (rageLayer == null) return;
		if (AutoLayeringGroup) 
			rageLayer.Zorder = _currentGroupId * Mathf.CeilToInt (ZsortOffset * -1);
		else
			rageLayer.Zorder = (int)NewZOffset();
		rageLayer.ForceRefresh = AutoLayeringMaterials;
	}

	/// <summary>Iterates a new Z Offset and updates the variable </summary>
	/// <returns>The new z offset value</returns>
	private float NewZOffset() {
		var oldPathId = _currentPathId;
		_currentPathId++;
		if (!AutoLayering) return (oldPathId * ZsortOffset);
		return (oldPathId * Mathf.CeilToInt(ZsortOffset * -1));
	}

	/// <summary> Draws a circle to represent a single point, according to https://bugzilla.mozilla.org/show_bug.cgi?id=322976 </summary>
	private void CreateDotCircle() {
		// <circle fill="none" stroke="#000000" stroke-miterlimit="10" cx="100" cy="100" r="50"/>
		var svgCircle = "<circle ";
		//Stores the current Outline color to later apply it as a fill
		var originalOutlineColor = _currentStyle.OutlineColor1;
		var originalIsClosed = _currentStyle.IsClosed;
		var objectName = _current.Object.name;

		svgCircle += "stroke=\"none\" ";
		Vector3 position = _current.Spline.GetPointAt(0).Position;
		svgCircle += "cx=\"" + position.x + "\" ";
		svgCircle += "cy=\"" + (-1 * position.y) + "\" ";
		svgCircle += "r=\"" + _current.Spline.Outline.GetWidth(0) + "\"/>";

		if(DebugMeshCreation)
			Debug.Log("svgDotCircle: " + _current.Object.name + " " + svgCircle);
		// Destroy the initially created path container, gonna start fresh
		_current.Object.SmartDestroy();

		XmlNode node = RageXmlParser.XmlToDOM(svgCircle);
		_currentStyle.CornersType = Spline.CornerType.Beak;
		ParseCircle(node.ChildNodes[0],false);

		// Only fills the shape if it's closed (found "z")
		// TODO: single "l" command to the same position of a move also makes it a dot shape
		if(originalIsClosed) {
			_current.Spline.FillType = Spline.FillType.Solid;
			_current.Spline.FillColor = originalOutlineColor;
		}
		else
			_current.Spline.FillType = Spline.FillType.None;

		_current.Object.name = objectName;
	}

	/// <summary> Parses the style string to the '_currentStyle' object that SVG-In uses </summary>
	private void ParseRageStyle() {
		if(string.IsNullOrEmpty(_styleString))
			return;

		foreach(var style in GetSvgStyleCommands(_styleString)) {
			switch(style.Command) {
				case ("stroke-miterlimit"):
					if(style.Value != "0")
						_currentStyle.CornersType = Spline.CornerType.Beak;
					break;
				case ("stroke-linejoin"):
					_currentStyle.CornersType = style.Value == "miter" ? Spline.CornerType.Beak : Spline.CornerType.Default;
					break;
				case ("stroke-linecap"):
					// 					if (style.Value == "round")
					// 						_currentStyle.Corners = RageSpline.Corner.Default;
					break;
				case ("opacity"):
					_currentStyle.FillColor1Alpha = style.Value.SvgToFloat();
					_currentStyle.OutlineAlpha = style.Value.SvgToFloat();
					break;
				case "fill":
					if(style.Value == "none" || style.Value == "transparent") {
						_currentStyle.FillType = Spline.FillType.None;
						break;
					}
					_currentStyle.FillType = Spline.FillType.Solid;
					if(style.Value[0] == '#') {
						_currentStyle.FillColor1 = style.Value.Substring(1).ToColor();
						break;
					}
					if(style.Value.StartsWith("url")) {
						//eg.: url(#SVGID_1_)
						var length = style.Value.Length - 6; //-5 + (-1)
						var gradientId = style.Value.Substring(5, length);
						//Debug.Log("gradient Id to Search: " + gradientId);
						RageSvgGradient gradient;
						//Note: For dictionaries, TryGetValue is much faster than GetKey
						if(_gradients.TryGetValue(gradientId, out gradient)) {
							_currentStyle.RageSvgGradient = gradient;
							_currentStyle.FillType = Spline.FillType.Gradient;
						}
						break;
					}
					// Tries parsing a named color
					_currentStyle.FillColor1 = style.Value.KeywordToHex().ToColor();
					break;
				case "stroke":
					if(style.Value == "none" || style.Value == "transparent") {
						_currentStyle.OutlineType = Spline.OutlineType.None;
						break;
					}
					_currentStyle.OutlineColor1 = style.Value[0] == '#' ?
														style.Value.Substring(1).ToColor()
													  : style.Value.KeywordToHex().ToColor();
					// Temporarily sets outline to loop just as a flag
					_currentStyle.OutlineType = Spline.OutlineType.Loop;
					break;
				case "stroke-width":
					// if there's a measure unit, remove the trailing alphabetic characters (eg.: px, en)
					var width = style.Value.TrimTrailingAlphas();
					_currentStyle.OutlineWidth = width.SvgToFloat();
					if(Mathfx.Approximately(_currentStyle.OutlineWidth,0f))
						_currentStyle.OutlineType = Spline.OutlineType.None;

					if(DebugMeshCreation)
						Debug.Log("stroke width ===> " + width);
					break;
				case "stroke-opacity":
					_currentStyle.OutlineAlpha = style.Value.SvgToFloat();
					break;
				case "fill-opacity":
					_currentStyle.FillColor1Alpha = style.Value.SvgToFloat();
					break;
			}
		}
	}

	/// <summary> Parses the RageStyle and applies the Vertex Density during import  </summary>
	private void ApplyRageStyle() {
		// Parse object example:
		// "fill:none;stroke:#FF0000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
		if(DebugStyleCreation) {
			Debug.Log("\n>>> Preparing to set RageStyle of object " + _current.Object.name
					  + ". Previous Fill & outline: " + _currentStyle.FillType + " :: " + _currentStyle.OutlineType);
			Debug.Log("\tPrevious Fill Color: " + _currentStyle.FillColor1);
		}
		ParseRageStyle();

		if(DebugStyleCreation) {
			Debug.Log("\n>>> Current Fill & outline: " + _currentStyle.FillType + " :: " + _currentStyle.OutlineType);
			Debug.Log("\tCurrent Fill Color: " + _currentStyle.FillColor1);
		}
		if(DebugStyleCreation)
			Debug.Log("\t\t Closed shape? " + _currentStyle.IsFilled + "; Outline: " + _currentStyle.OutlineType);

		if(_currentStyle.OutlineType != Spline.OutlineType.None) {
			_currentStyle.OutlineType = _currentStyle.IsFilled ? Spline.OutlineType.Loop : Spline.OutlineType.Free;
		}
		// If it's a group (has no spline), stops here.
		if(_current.Spline == null) {
			_styleString = "";
			return;
		}
		ApplyStyleToSpline();
		// Parse single point, according to: https://bugzilla.mozilla.org/show_bug.cgi?id=322976
		if(_current.Spline.PointsCount == 1) {
			// Has to create a circle even without a "z" command to prevent RageSpline errors
			//if(_current.Spline.Outline.CornerType == Spline.CornerType.Default)
				CreateDotCircle();
			//else return;
		}
		ApplyVertexDensity();
		_current.Spline.Rs.RefreshMeshInEditor(true, true, true);
	}

	private void ApplyVertexDensity() {
		if(_current.Spline.PointsCount < 2) return;

		if(_isLinear) {							// If it's a line or polygon
			OptimizeLinearShape();
			return;
		}
		int finalVertexDensity = MaxVertexDensity;

		if(_adaptiveDensity) {
			// If AdaptiveDensity is on, compares the shape size with the viewport size
			var area = _current.Spline.Bounds.width * _current.Spline.Bounds.height;

			float ratio = Mathf.Clamp(((area / _sceneCamera.GetCameraArea()) * 10), 0f, 1f);

			// upper limit is user-set vertex density
			finalVertexDensity = Mathf.RoundToInt(Mathf.Lerp(MinVertexDensity, MaxVertexDensity, ratio));

			if(DebugStyleCreation) {
				Debug.Log("Area = " + area + "\nRatio = " + ratio);
				Debug.Log("Final Density for " + _current.Object.name + " = " + finalVertexDensity);
			}
		}
		// Applies the Vertex Density
		_current.Spline.VertexDensity = (finalVertexDensity);
		//^-RagetoolsCommon extension method, equals to: rageSpline.SetVertexCount(rageSpline.GetPointCount() * finalVertexDensity);
		_styleString = "";
	}

	private void ApplyGradientTransform(string gradientTransformString) {
		if(gradientTransformString == "") {
			_currentGradient.Y1 *= -1;
			_currentGradient.Y2 *= -1;
			return;
		}
		/*Debug.Log(_currentGradient.Id + "\nG1 orig pos: " + _currentGradient.X1 + " " + _currentGradient.Y1);*/

		var gradientStop = new Vector2(_currentGradient.X1, _currentGradient.Y1);
		gradientStop = ApplyTransform(gradientStop, gradientTransformString);
		_currentGradient.SetStartPos(gradientStop);
		/*Debug.Log("G1 final pos: " + _currentGradient.X1 + " " + _currentGradient.Y1);*/

		gradientStop = new Vector2(_currentGradient.X2, _currentGradient.Y2);
		gradientStop = ApplyTransform(gradientStop, gradientTransformString);
		_currentGradient.SetEndPos(gradientStop);
	}

	/// <summary> Apply the Transform parameters and process Transform Matrices </summary>
	/// <param name="targetGO"></param>
	/// <param name="newTransformString"></param>
	private void ApplyTransform(GameObject targetGO, string newTransformString) {
		if(string.IsNullOrEmpty(newTransformString))
			return;

		if(DebugMeshCreation)
			Debug.Log("Transformation String: " + newTransformString);
		//<g transform="translate(-10,-20) scale(2) rotate(45) translate(5,10)">
		Regex r = new Regex(@",\s*", RegexOptions.IgnoreCase);
		newTransformString = r.Replace(newTransformString, " ");
		var transformCommand = newTransformString.Split(new[] { ' ', ',', '(', ')', '\r', '\n' }); 
		transformCommand.RemoveEmptyEntries();

		var posOffset = Vector3.zero;
		var rotOffset = Vector3.zero;
		float scaleFactor = 1f;
		for(var i = 0; i < transformCommand.Length; i++) {
			if(transformCommand[i] == "matrix") {
				ApplyTransformMatrix(transformCommand, targetGO);
				break;
			}
			if(transformCommand[i] == "translate") {
				if(DebugMeshCreation)
					Debug.Log("\tTransform translate: " + transformCommand[i + 1].SvgToFloat() +
							  "," + transformCommand[i + 2].SvgToFloat());
				posOffset += new Vector3(transformCommand[i + 1].SvgToFloat(),
										 transformCommand[i + 2].SvgToFloat(),
										 0);
				i = i + 2;
			}
			if(transformCommand[i] == "rotate") {
				if(DebugMeshCreation)
					Debug.Log("\tTransform Rotate: " + transformCommand[i + 1].SvgToFloat());
				rotOffset += new Vector3(0f, 0f, transformCommand[i + 1].SvgToFloat());
				i = i + 1;
			}
			if(transformCommand[i] == "scale") {
				if(DebugMeshCreation)
					Debug.Log("\tTransform scale: " + transformCommand[i + 1].SvgToFloat());
				scaleFactor *= transformCommand[i + 1].SvgToFloat();
				i = i + 1;
			}
		}

		targetGO.transform.eulerAngles -= rotOffset;
		targetGO.transform.localScale *= scaleFactor;
		var pos = targetGO.transform.position + posOffset;

		targetGO.transform.position = new Vector3(pos.x, -1*pos.y, pos.z); // Mirrors on Y
	}

	private void ApplyTransformMatrix(string[] transformCommand, GameObject targetGO) {
		string temp = "";
		if (DebugMeshCreation) {
			foreach (string str in transformCommand)
				temp += str + "|| ";
			Debug.Log(temp);
		}
		// Eg.: <g transform="matrix(0.240147,0.000000,0.000000,0.240147,650.7029,5.991577)">
		// transformCommand :: [1 2 3 4 5 6]
		//                     | a  b  tx |       | a c e |
		//          Inkscape = | c  d  ty | SVG = | b d f |
		//                     | 0  0  1  |       | 0 0 1 |
		var a = transformCommand[1].SvgToFloat(); // x Scale
		var b = transformCommand[2].SvgToFloat(); // x Skew
		var c = transformCommand[3].SvgToFloat(); // y Skew
		var d = transformCommand[4].SvgToFloat(); // y Scale
		var tx = transformCommand[5].SvgToFloat();
		var ty = transformCommand[6].SvgToFloat();

		var pos = targetGO.transform.position;
		var finalPos = new Vector2(	(pos.x * a) + (pos.y * c) + tx,
									(pos.x * b) + (pos.y * d) + ty);
		float xSign = Mathf.Sign(a);
		float ySign = Mathf.Sign(d);
		// from matrix(a,-b,b,a,c,d), where a and b are not both zero => rotation angle is atan2(b,a).
		var r = Mathf.Atan2 (c,a) * Mathf.Rad2Deg;
		targetGO.transform.eulerAngles = new Vector3(0f, 0f, xSign * ySign* r);

		// a,b,c,d,tx,ty => the scale is sx=sqrt(a^2+b^2) and sy=sqrt(c^2+d^2)
		// In SVG: a,c,b,d,e,f => the scale is sx=sqrt(a^2+c^2) and sy=sqrt(b^2+d^2)
		var sx = Mathf.Sqrt ((Mathf.Pow (a, 2)) + (Mathf.Pow (c, 2)));
		var sy = Mathf.Sqrt ((Mathf.Pow (b, 2)) + (Mathf.Pow (d, 2)));

		targetGO.transform.position = new Vector3(finalPos.x, finalPos.y, targetGO.transform.position.z);
		targetGO.transform.localScale = new Vector3((xSign*sx), (ySign*sy), 1);
	}

	/// <summary> ApplyTransform variation used for Gradient Transforms</summary>
	private Vector2 ApplyTransform(Vector2 target, string newTransformString) {
		if(string.IsNullOrEmpty(newTransformString)) return target;

		if(DebugMeshCreation)
			Debug.Log("Transformation String: " + newTransformString);
		var transformCommand = newTransformString.Split(new[] { ' ', ',', '(', ')', '\r', '\n' });
		transformCommand.RemoveEmptyEntries();

		for(var i = 0; i < transformCommand.Length; i++) {

			if(transformCommand[i] == "matrix") {
				var a = transformCommand[1].SvgToFloat(); // x Scale
				var b = transformCommand[2].SvgToFloat(); // x Skew
				var c = transformCommand[3].SvgToFloat(); // y Skew
				var d = transformCommand[4].SvgToFloat(); // y Scale
				var tx = transformCommand[5].SvgToFloat();
				var ty = transformCommand[6].SvgToFloat();
				target = new Vector2 (	(target.x * a) + (target.y * c) + tx, 
										(target.x * b) + (target.y * d) + ty);
			}
			if(transformCommand[i] == "translate") {
				if(DebugMeshCreation)
					Debug.Log("\tTransform translate: " + transformCommand[i + 1].SvgToFloat() +
							  "," + transformCommand[i + 2].SvgToFloat());
				var offset = new Vector2(transformCommand[i + 1].SvgToFloat(),
										 transformCommand[i + 2].SvgToFloat());

				target = new Vector2(target.x + offset.x,
									 target.y + offset.y);					// reflects on Y
				i = i + 2;
			}
		}
		return target;
	}

	/// <summary> Actually applies the current style to the spline  </summary>
	private void ApplyStyleToSpline() {
		_current.Spline.FillType = _currentStyle.FillType;
		_current.Spline.Outline.Type = _currentStyle.OutlineType;
		_current.Spline.Outline.Width = _currentStyle.OutlineWidth;
		_current.Spline.Outline.CornerType =_currentStyle.CornersType;

// 		if(_currentStyle.OutlineType == Spline.OutlineType.Free)
// 			CreateFreeOutlineCaps(_current.Spline);

		Color cFillColor1 = _currentStyle.FillColor1;
		cFillColor1.a = _currentStyle.FillColor1Alpha;

		if(_currentStyle.FillType == Spline.FillType.Gradient)
			ApplyFillGradient();
		else
			_current.Spline.FillColor = cFillColor1;

		Color cStrokeColor = _currentStyle.OutlineColor1;
		cStrokeColor.a = _currentStyle.OutlineAlpha;
		_current.Spline.Outline.StartColor = cStrokeColor;
	}

	/// <summary> Sets the Gradient Fill parameters of the RageSpline </summary>
	private void ApplyFillGradient() {
		var gradient = _currentStyle.RageSvgGradient;
		var start = new Vector2(gradient.X1, gradient.Y1);
		var end = new Vector2(gradient.X2, gradient.Y2);
		if(DebugStyleCreation)
			Debug.Log("\tGradient Start Color:" + gradient.StartColor);
		_current.Spline.FillGradient.StartColor = gradient.StartColor;
		_current.Spline.FillGradient.EndColor = gradient.EndColor;
		_current.Spline.FillGradient.StyleLocalPositioning = true;
		_current.Spline.FillGradient.Offset = (start + end) / 2;
		_current.Spline.FillGradient.Scale = 1 / ((end - start) / 2).magnitude;
		//Debug.Log("current: "+_current.Spline.name+" | start-end x/y : "+ start.x +" "+ start.y+" "+end.x+" "+end.y);
		var gradientAngle = ColorExtension.CalcTheta(new Vector2(gradient.X1, gradient.Y1),
											new Vector2(gradient.X2, gradient.Y2));

		if(DebugStyleCreation)
			Debug.Log("\tGradient Angle: " + gradientAngle);
		_current.Spline.FillGradient.Angle = gradientAngle;
	}

	private void MoveTo(List<float> coords, bool isRelative, bool keepX, bool keepY) {
		int k = 0;

		while(k < coords.Count) {
			Vector3 newPos = Vector3.zero;

			// if it shouldn't keep the current X or Y (ie. H or V commands), parse the new coord
			if(keepX) {
				newPos.x = _current.CursorPos.x;
				newPos.y = -coords[k]; // reflects on Y
				if(isRelative)
					newPos.y += _current.CursorPos.y;
			} else {
				if(keepY) {
					newPos.x = coords[k];
					newPos.y = _current.CursorPos.y;
					if(isRelative)
						newPos.x += _current.CursorPos.x;
				} else {
					newPos.x = coords[k];
					newPos.y = -coords[k + 1]; // reflects on Y
					if(isRelative) {
						newPos.x += _current.CursorPos.x;
						newPos.y += _current.CursorPos.y;
					}
				}
			}

			if(DebugMeshCreation)
				Debug.Log("\tcurrent x/y: " + _current.CursorPos.x + " " + _current.CursorPos.y);

			_current.Spline.Rs.AddPoint(_current.PointIdx, newPos, Vector3.zero, Vector3.zero, 1.0f, false);

			if(DebugMeshCreation)
				Debug.Log("Move To (" + (_current.PointIdx) + ") X/Y: " + newPos.x + "," + newPos.y + 
						  " InCtrl: " + _current.Spline.Rs.GetInControlPositionWorldSpace(_current.PointIdx));
// 			_current.Spline.Rs.SetPoint(_current.PointIdx, _current.Spline.Rs.GetPosition(_current.PointIdx),
// 										_current.Spline.Rs.GetInControlPosition(_current.PointIdx),
// 										_current.Spline.Rs.GetOutControlPosition(_current.PointIdx), false);
			//_current.Spline.GetPointAt(_current.PointIdx).Smooth = false;
			// Sets the in control to be 1/2 of the vector to the previous point
			if(MidlineControls)
				CreateHalfwayTangents();

			_current.CursorPos = newPos;
			_current.PointIdx++;

			// will parse the next coordinate if it's an H or V command, otherwise skips two coords
			k = (keepX || keepY) ? k + 1 : k + 2;
		}

	}


	private void CurveTo(List<float> coords, bool isRelative) {
		int k = 0;
		// if (!_isPathStarted) _isPathStarted = true;

		while(k < coords.Count) {
			// Relative curves are somewhat tricky. Example:
			//c 0,0 11,18 10,10
			// The first Pair is the in-tangent of the previous point, absolute and reflected on Y.
			//
			// The Position (third Pair) is added to the previous point coords, also reflected on Y. 
			// Like so: (20,30) => (20+10, (30+10) = (30, 40)
			//
			// (Second Pair - Third Pair), reflected on Y, gives the out tangent point. 
			// Like so: (11-10, 18-10) = (1, -8)

			// Sets the previous point Out Tangent

			var point = _current.Spline.GetPointAt(_current.PointIdx - 1);
			if(isRelative) point.OutTangentLocal = new Vector3(coords[k], -coords[k + 1]);
			else point.OutTangent =  new Vector3(coords[k], -coords[k + 1]);

			// Calculate new position coordinates
			var newPos = new Vector3(coords[k + 4], -coords[k + 5]);

			// Calculate in-control vector coordinates
			var inCtrl = new Vector3(coords[k + 2], -coords[k + 3]);
			inCtrl -= newPos;

			if(isRelative) newPos += _current.CursorPos;

			ISplinePoint newPoint = _current.Spline.AddPointLocal(_current.PointIdx, newPos, Vector3.zero, Vector3.zero, 1.0f, false);
			newPoint.InTangentLocal = inCtrl;

			if(DebugMeshCreation)
				Debug.Log("(c)index: " + (_current.PointIdx - 1) + " Position: "
					+ newPos.x + "," + newPos.y + " Pos: " + newPos.x + "," + newPos.y +
					" InCtrl: " + newPoint.InTangentLocal.x + "," + newPoint.InTangentLocal.y);

			_current.PointIdx++;
			_current.CursorPos = newPos;
			k += 6;
		}
	}

	private void SmoothCurveTo(List<float> coords, bool isRelative) {
		int k = 0;
		bool startingPoint = false;

		if(_current.PointIdx > 0)
			_current.Spline.GetPointAt(_current.PointIdx - 1).Smooth = true;
		else
			startingPoint = true;

		while(k < coords.Count) {
			//      Coords: (x2 y2 x y); (x2,y2) = out control point position, (x,y) point position

			// Calculate new position coordinates
			var newPos = new Vector3(coords[k + 2], -coords[k + 3]);

			if(isRelative)
				newPos += _current.CursorPos;

			// Calculate in-control point coordinates
			var inCtrl = new Vector3(coords[k], -coords[k + 1]);
			var inCtrl2 = new Vector3(coords[k + 2], -coords[k + 3]);
			inCtrl -= inCtrl2;

			// We add the point, then set its in-control position
			ISplinePoint p = _current.Spline.AddPointLocal(_current.PointIdx, newPos, new Vector3(0, 0), new Vector3(0, 0), 1.0f, false);		
			p.InTangentLocal = inCtrl;

			_current.PointIdx++;

			if(DebugMeshCreation)
				Debug.Log("(s)index: " + _current.PointIdx +
					" Pos: " + newPos.x + "," + newPos.y +
					" InCtrl: " + inCtrl.x + "," + inCtrl.y
					);

			_current.CursorPos = newPos;

			k += 4;
		}

		// This is just a safe check. A well-formed SVG file won't ever use this.
		if(startingPoint)
			_current.Spline.GetPointAt(_current.PointIdx).Smooth = true;
	}

	private void OptimizeLinearShape() {

		if(_current.Spline.PointsCount == 2) {
			_current.Spline.VertexDensity = 24;
			_current.Spline.Outline.CornerType = Spline.CornerType.Default;
		}

		if(_current.Spline.PointsCount == 4 && _current.Spline.Outline.Type == Spline.OutlineType.Loop) 
			_current.Spline.VertexDensity = 1;

		_styleString = "";
		_isLinear = false;
	}

	private void CreateHalfwayTangents() {
		if(_current.PointIdx <= 0) return;

		var p0 = _current.Spline.GetPointAt(_current.PointIdx - 1);
		var p1 = _current.Spline.GetPointAt(_current.PointIdx);

		p0.OutTangent = new Vector3((p1.Position.x + p0.Position.x) / 2,
									(p1.Position.y + p0.Position.y) / 2,
									(p1.Position.z + p0.Position.z) / 2);
	}


	// style="fill:none;stroke:#FF0000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	private static IEnumerable<RageSvgStyleEntry> GetSvgStyleCommands(string styleString) {
		if(styleString.Length == 0)
			yield break;

		var styleStringLength = styleString.Length;

		for(int i = 0; i < styleStringLength; i++) {
			var style = RageSvgStyleEntry.NewInstance();

			while(styleString[i] != ':') {
				style.Command += styleString[i];
				i++;
				if(i == styleStringLength)         //safe-check for malformed SVGs
					yield break;
			}
			i++; //skip the colon

			while(styleString[i] != ';') {
				style.Value += styleString[i];
				i++;
				if(i == styleStringLength) {
					yield return style;
					yield break;
				}
			}
			// Remove empty spaces from commands and values
			style.Command = style.Command.Trim();
			style.Value = style.Value.Trim();

			// new style entry found, iterate
			yield return style;
		}
	}

	private static IEnumerable<string> SplitCommands(string path) {

		var commandStart = 0;

		for(var i = 0; i < path.Length; i++) {
			string command;
			if (path[i].IsPathCommand()) {
				command = path.Substring(commandStart, i - commandStart).Trim();
				commandStart = i;

				if(!string.IsNullOrEmpty(command))
					yield return command;

				if(path.Length == i + 1)
					yield return path[i].ToString();
			} else if(path.Length == i + 1) {
				command = path.Substring(commandStart, i - commandStart + 1).Trim();

				if(!string.IsNullOrEmpty(command))
					yield return command;
			}
		}
	}

	private static IEnumerable<float> ParseCoordinates(string coords) {
		coords = coords.Remove(0, 1);
		// Replaces "-" by " -", to help parsing, unless it's an exponential index
		//([^e\s]|[\d])-(\d)
		Regex rx = new Regex(@"([\d])-(\d)", RegexOptions.IgnoreCase);
		coords = rx.Replace(coords, "$1 -$2");
		coords = rx.Replace(coords, "$1 -$2");

		var parts = coords.Split(new[] { ',', ' ', '\t', '\r', '\n' }).RemoveEmptyEntries();

		foreach(string t in parts) {
			yield return float.Parse(t.Trim(), NumberStyles.Float, CultureInfo.InvariantCulture);
		}
	}
};
